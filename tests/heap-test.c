/*
 * heap-test.c -- add a comment about this file
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2009 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdlib.h>
#include <ccl/ccl-heap.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-init.h>
#include <ccl/ccl-log.h>

static int
s_check_heap (int array_size);

static void
s_listener (ccl_log_type type, const char *msg, void *data);

			/* --------------- */

int main (int argc, char **argv)
{
  int i;
  int result = EXIT_SUCCESS;
  const int seed = 123456789;

  srand (seed);

  ccl_init ();
  ccl_log_add_listener(s_listener, NULL);

  for (i = 0; i < 100 && result == EXIT_SUCCESS; i++)
    {
      if (! s_check_heap (i))
	result = EXIT_FAILURE;
    }
  ccl_terminate ();

  return result;
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else
    fprintf(stderr,"%s",msg);
}

			/* --------------- */

static int
s_cmp_int(const void *p1, const void *p2)
{
  return *((int *) p1) - *((int *) p2);
}

			/* --------------- */

static void
s_log_int (ccl_log_type log, void *obj, void *data)
{
  ccl_log (log, "%d", (int) (intptr_t) obj);
}

			/* --------------- */

static int
s_check_heap (int array_size)
{
  int result = 0;
  int i;
  int *qsorted_array = ccl_new_array (int, array_size);
  int *hsorted_array = ccl_new_array (int, array_size);
  ccl_heap *H = ccl_heap_create (1, NULL);

  for (i = 0; i < array_size; i++)
    {
      intptr_t val = random ();
      qsorted_array[i] = val;
      ccl_heap_add (H, (void *) val);
    }

  if (ccl_heap_get_size (H) != array_size)
    {
      fprintf (stderr, "failure on insertion: "
	       "expected size = %d actual size = %d\n",
	       array_size, ccl_heap_get_size (H));
      goto end;
    }

  i = 0;
  while (! ccl_heap_is_empty (H))
    {
      intptr_t val = (intptr_t) ccl_heap_take_first (H);
      hsorted_array[i++] = val;
    }

  if (i != array_size)
    {
      fprintf (stderr, "failure on removal: "
	       "expected size = %d actual size = %d\n",
	       array_size, i);
      goto end;
    }
  
  qsort (qsorted_array, array_size, sizeof (int), s_cmp_int);
  result = ccl_memcmp (qsorted_array, hsorted_array, 
		       sizeof (int) * array_size) == 0;
  if (!result)
    {
      fprintf (stderr, "qsorted=");
      for (i = 0; i < array_size; i++)
	fprintf (stderr, "%d ", qsorted_array[i]);
      fprintf (stderr, "\nhsorted=");
      for (i = 0; i < array_size; i++)
	fprintf (stderr, "%d ", hsorted_array[i]);
      fprintf (stderr, "\n");
    }

 end:
  ccl_heap_delete (H);
  ccl_delete (qsorted_array);
  ccl_delete (hsorted_array);

  return result;
}
