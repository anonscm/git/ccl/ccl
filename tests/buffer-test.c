/*
 * buffer-test.c -- add a comment about this file
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdlib.h>
#include <ccl/ccl-buffer.h>
#include <ccl/ccl-init.h>
#include <ccl/ccl-log.h>

static int
s_check_buffer (int size);

static void
s_listener (ccl_log_type type, const char *msg, void *data);

			/* --------------- */

int main (int argc, char **argv)
{
  int i;
  int result = EXIT_SUCCESS;
  const int seed = 123456789;

  srand (seed);

  ccl_init ();
  ccl_log_add_listener(s_listener, NULL);

  if (! s_check_buffer (0))
    result = EXIT_FAILURE;
  for (i = 1; i < 21358 && result == EXIT_SUCCESS; i += 64)
    {
      if (! s_check_buffer (i))
	result = EXIT_FAILURE;
    }
  ccl_terminate ();

  return result;
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else
    fprintf (stderr,"%s",msg);
}

			/* --------------- */

static int
s_check_buffer (int size)
{
  int result = 0;
  int i;
  ccl_buffer_content orig;
  ccl_buffer_content res;
  ccl_buffer *buf = ccl_buffer_create ();

  ccl_array_init (res);
  ccl_array_init_with_size (orig, size);

  for (i = 0; i < size; i++) orig.data[i] = random ();

  ccl_buffer_append (buf, orig.data, orig.size);
  ccl_buffer_get_content (buf, &res);

  if (res.size != orig.size)
    ccl_error ("error: expected size = %d actual size = %d (param=%d)\n",
	       orig.size, res.size, size);
  else if (ccl_memcmp (res.data, orig.data, orig.size) != 0)
    ccl_error ("error: contents differs\n");
  else
    result = 1;

  ccl_buffer_del_reference (buf);
  ccl_array_delete (orig);
  ccl_array_delete (res);

  return result;
}
