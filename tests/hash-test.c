/*
 * heap-test.c -- add a comment about this file
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2009 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdlib.h>
#include <ccl/ccl-hash.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-init.h>
#include <ccl/ccl-log.h>

static int
s_check_hash (int size);

static void
s_listener (ccl_log_type type, const char *msg, void *data);

			/* --------------- */

int main (int argc, char **argv)
{
  int result = EXIT_SUCCESS;
  const int seed = 123456789;

  srand (seed);

  ccl_init ();
  ccl_log_add_listener(s_listener, NULL);

  s_check_hash (1000);

  ccl_terminate ();

  return result;
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else
    fprintf(stderr,"%s",msg);
}

			/* --------------- */

static int
s_check_hash (const int size)
{
  int i, j, k;
  ccl_hash *H = ccl_hash_create (NULL, NULL, NULL, NULL);

  for (i = 0; i < size*size; i++)
    {
      void *p = (void *) (intptr_t) i;
      if (!ccl_hash_find (H, p))
	ccl_hash_insert (H, p);
    }

  for (k = 0; k < size; k++)
    {
      if (k%5==0) printf("%5d/%5d;\n",k,size);
      else printf("%5d/%5d;",k,size-1);

      for (i = 0; i < size; i++)
	{      
	  for (j = 0; j < size; j++)
	    {
	      void *p = (void *) (intptr_t) (size * i + j);
	      if (! ccl_hash_find (H, p) && ! (ccl_hash_get (H) == p))
		return 0;
	    }
	}
    }

  ccl_hash_delete (H);

  return 1;
}
