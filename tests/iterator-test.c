/*
 * iterator-01-test.c -- add a comment about this file
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <stdlib.h>
#include <ccl/ccl-iterator.h>
#include <ccl/ccl-assert.h>
#include <ccl/ccl-init.h>
#include <ccl/ccl-memory.h>
#include <ccl/ccl-log.h>


typedef struct interval_iterator_st interval_iterator;
struct interval_iterator_st {
  ccl_int_iterator super;
  int current;
  int end;
};


static int
s_interval_has_more_elements (const ccl_int_iterator *p)
{
  const interval_iterator *ii = (const interval_iterator *) p;
  return ii->current <= ii->end;
}

static int
s_interval_next_element (ccl_int_iterator *p)
{
  interval_iterator *ii = (interval_iterator *) p;
  return ii->current++;
}

static void
s_interval_delete_iterator (ccl_int_iterator *p)
{
  /*  interval_iterator *ii = (interval_iterator *) p;*/
}

static ccl_int_iterator *
s_crt_interval_iterator (int start, int end)
{
  interval_iterator *ii = ccl_new (interval_iterator);
  ii->super.has_more_elements = s_interval_has_more_elements;
  ii->super.next_element = s_interval_next_element;
  ii->super.delete_iterator = s_interval_delete_iterator;

  ii->current = start;
  ii->end = end;
  
  return (ccl_int_iterator *) ii;
}

static void
s_listener (ccl_log_type type, const char *msg, void *data);

static void
s_test_1 (void);

static void
s_test_2 (void);

			/* --------------- */

int main (int argc, char **argv)
{
  int result = EXIT_SUCCESS;
  const int seed = 123456789;

  srand (seed);

  ccl_init ();
  ccl_log_add_listener(s_listener, NULL);
  ccl_debug_max_level (-1);
  ccl_debug_is_on = 1;
  s_test_1 ();
  s_test_2 ();
  
  ccl_terminate ();

  return result;
}

			/* --------------- */

static void
s_listener (ccl_log_type type, const char *msg, void *data)
{
  if (type == CCL_LOG_DISPLAY)
    fprintf (stdout, "%s", msg);
  else
    fprintf (stderr,"%s",msg);
}

			/* --------------- */

static void
s_test_1 (void)
{
  int N = 100;

  while (N--)
    {
      int i;
      int min = rand () % 1000;
      int max = min + rand () % 1000;      
      ccl_int_iterator *ii = s_crt_interval_iterator (min, max);

      ccl_log (CCL_LOG_DEBUG, "min=%d max=%d\n", min, max);
      while (ccl_iterator_has_more_elements (ii))
	{	  
	  i = (intptr_t) ccl_iterator_next_element (ii);
	  ccl_log (CCL_LOG_DEBUG, "%d ", i);
	  ccl_assert (i == min);
	  min++;
	}
      ccl_log (CCL_LOG_DEBUG, "\n");
      ccl_assert (i == max);
      ccl_iterator_delete (ii);
    }
}

static void
s_test_2 (void)
{
  int N = 100;

  while (N--)
    {
      int k, i, min, max;
      int M = 10;
      int *T = ccl_new_array (int, M);
      T[0] = rand () % 1000;
      for (i = 1; i < M; i++)	
	T[i] = T[i-1] + (rand() % 50);

      ccl_int_iterator *ii
	= s_crt_interval_iterator (T[0], T[1]);
      for (i = 2; i < M; i++)
	{
	  ccl_int_iterator *ii2 =
	    s_crt_interval_iterator (T[i-1], T[i]);
	  ii = (ccl_int_iterator *)
	    ccl_iterator_crt_concat ((ccl_pointer_iterator *)ii,
				     (ccl_pointer_iterator *)ii2);
	}

      for (i = 0; i < M; i++)
	ccl_log (CCL_LOG_DEBUG, "%d ", T[i]);
      ccl_log (CCL_LOG_DEBUG, "\n");

      k = 0;
      i = ccl_iterator_next_element (ii);
      ccl_assert (T[k] <= i && i <= T[k + 1]);
      
      while (ccl_iterator_has_more_elements (ii))
	{
	  i = (intptr_t) ccl_iterator_next_element (ii);
	  ccl_assert (T[k] <= i && i <= T[k + 1]);
	  if (i == T[k+1])
	    k++;
	}
      ccl_log (CCL_LOG_DEBUG, "\n");
      ccl_assert (i == T[M-1]);
      ccl_iterator_delete (ii);
    }
}
