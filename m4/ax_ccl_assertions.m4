dnl
dnl Enable or not ccl-assertion checking.
dnl
AC_DEFINE([CCL_ENABLE_ASSERTIONS],[0],[Description])
AC_DEFUN([AX_CCL_ENABLE_ASSERTIONS],
[
AC_ARG_ENABLE(ccl_assertions,
 AS_HELP_STRING([--enable-ccl-assertions],[enable CCL assertions (no)]),
 [enable_ccl_assertions=yes],)

if test "x$enable_ccl_assertions" = xyes; then
  AC_DEFINE([CCL_ENABLE_ASSERTIONS],[1])
fi
])

