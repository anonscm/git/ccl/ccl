/*
 * ccl-init.h -- CCL initializer and finalizer
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_INIT_H__
# define __CCL_INIT_H__

# include <ccl/ccl-common.h>

BEGIN_C_DECLS

/*!
 * \brief Initialization function to use prior to any CCL function call.
 * A counter if used to handle multiple calls.
 */
extern void
ccl_init (void);

/*!
 * \brief Function used to release any resources allocated by the CCL library.
 */
extern void
ccl_terminate (void);

END_C_DECLS

#endif /* ! __CCL_INIT_H__ */
