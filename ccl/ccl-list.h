/*
 * ccl-list.h -- A generic list encoded with a simple chaining.
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-list.h
 * \brief Generic simple linked list. 
 */
#ifndef __CCL_LIST_H__
# define __CCL_LIST_H__

# include <ccl/ccl-assert.h>
# include <ccl/ccl-protos.h>
# include <ccl/ccl-iterator.h>

BEGIN_C_DECLS

/*!
 * \brief Type of a generic list
 */
typedef struct ccl_list_st ccl_list;

/*! 
 * \brief Type of a cell of a \a ccl_list
 *
 * A pair is just a couple <em>(car, cdr)</em> where \e car is the address of
 * an object and \e cdr the address of the next \e pair (or cell) in the linked
 * list.
 */
typedef struct ccl_pair_st ccl_pair;

# ifndef IN_DOXYGEN_DOC
struct ccl_list_st {
  int size;
  ccl_pair *first;
  ccl_pair **last;
};

struct ccl_pair_st {
  void *car;
  ccl_pair *cdr;
};
# endif 

/*!
 * \brief Returns first \ref ccl_pair "pair" of the list \a l
 */
# define FIRST(l) ((l)->first)

/*!
 * \brief Returns the \a car part of the pair \a p
 */
# define CAR(p)   ((p)->car)

/*!
 * \brief Returns the \a cdr part of the pair \a p
 */
# define CDR(p)   ((p)->cdr)

/*!
 * \brief Shortcut for CAR (CDR (p))
 */
# define CADR(p)  (CAR (CDR (p)))

/*!
 * \brief Shortcut for CDR (CDR (p))
 */
# define CDDR(p)  (CDR (CDR (p)))

/*!
 * \brief Shortcut for CAR (CDDR (p))
 */
# define CADDR(p) (CAR (CDDR (p)))

/*!
 * \brief Shortcut for CDR (CDDR (p))
 */
# define CDDDR(p) (CDR (CDDR (p)))

/*!
 * \brief Shortcut for CAR (CDDDR (p))
 */
# define CADDDR(p) (CAR (CDDDR (p)))

/*!
 * \brief Shortcut for CDR (CDDDR (p))
 */
# define CDDDDR(p) (CDR (CDDDR (p)))

/*!
 * \brief Shortcut for CAR (CDDDDR (p))
 */
# define CADDDDR(p) (CAR (CDDDDR (p)))

/*!
 * \brief Shortcut for CDR (CDDDDR (p))
 */
# define CDDDDDR(p) (CDR (CDDDDR (p)))

/*!
 * \brief Creates a new list
 * \return a new empty list
 */
extern ccl_list *
ccl_list_create (void);

/*!
 * \brief Deletes the list \a l
 *
 * The function deletes the resources allocated for the storage of \a l. Objects
 * stored into pairs are not deleted. In order to free objects the 
 * \ref ccl_list_clear_and_delete must be used.
 *
 * \param l the list
 * \pre l != NULL
 */
extern void
ccl_list_delete (ccl_list *l);

/*!
 * \brief Makes empty the list \a l.
 *
 * The function deletes all pairs in \a l and apply the deletion function \a del
 * on each stored object. \a del can be NULL. The list is made empty but not
 * deleted.
 *
 * \param l the list
 * \param del a pointer to a deletion function
 * \pre l != NULL
 */
extern void
ccl_list_clear (ccl_list *l, ccl_delete_proc *del);

/*!
 * \brief Deletes the list \a l and stored objects.
 *
 * The function removes all resources allocated to \a l including the objects
 * stored in each pair. The function applies the deletion function \a del to
 * each object.
 *
 * \param l the list
 * \param del a pointer to a deletion function
 * \pre l != NULL
 */
extern void
ccl_list_clear_and_delete (ccl_list *l, ccl_delete_proc *del);

/*!
 * \brief Returns the number of elements in the list. 
 * \param l the list
 * \pre l != NULL
 * \return the number of elements in the list. 
 */
# define ccl_list_get_size(l)   (ccl_pre ((l) != NULL), (l)->size)

/*!
 * \brief Returns the number of elements in the list. 
 * \param l the list
 * \pre l != NULL
 * \return the number of elements in the list. 
 */
# define ccl_list_get_length(l) ccl_list_get_size(l)

/*!
 * \brief Checks if the list \a l contains no element.
 * \param l the list
 * \pre l != NULL
 * \return a non null value if \a l is empty
 */
# define ccl_list_is_empty(l) (ccl_list_get_size (l) ==0)

/*!
 * \brief Adds an object \a obj at the end of the list \a l
 * 
 * A new pair containing \a obj is appended to \a l. The operation is done in 
 * constant time.
 * 
 * \param l the list
 * \param obj the obejct put at the end of \a l
 * \pre l != NULL
 */
extern void
ccl_list_add (ccl_list *l, void *obj);

/*!
 * \brief Checks if the object \a obj belongs to \a l.
 *
 * \param l the list
 * \param obj the object whose existence is checked
 * \pre l != NULL
 * \return a non-null value if \a object belongs to \a l.
 */
extern int
ccl_list_has (const ccl_list *l, void *obj);

/*!
 * \brief Adds an object \a obj at the beginning of the list \a l
 * 
 * A new pair containing \a obj is added before the head of \a l.
 * 
 * \param l the list
 * \param obj the object added in the list \a l
 * \pre  != NULL
 */
extern void
ccl_list_put_first (ccl_list *l, void *obj);

/*!
 * \brief Removes the first object of the list \a l
 * 
 * The function removes the first pair of \a l and returns the object that 
 * was stored in it.
 * 
 * \param l the list
 * \pre ! ccl_list_is_empty (l)
 * \return the content of the removed pair
 */
extern void *
ccl_list_take_first (ccl_list *l);

/*!
 * \brief (Quick-)Sort the list \a l using the comparison function \a cmp. 
 * If \a cmp == \ref CCL_DEFAULT_COMPARE_FUNC, addresses of objects are 
 * compared.
 * \param l the list 
 * \param cmp the comparison function for objects stored in the list \a l
 * \pre l != NULL
 */
extern void
ccl_list_sort (ccl_list *l, ccl_compare_func *cmp);

typedef int ccl_compare_with_data_func (const void *o1, const void *o2, 
					const void *data);

#define CCL_DEFAULT_COMPARE_WITH_DATA_FUNC NULL

extern void
ccl_list_sort_with_data (ccl_list *l, ccl_compare_with_data_func *cmp, 
			 const void *data);

 /*!
 * \brief Inserts the object \a obj into the ordered list \a l
 *
 * The function inserts the object \a obj before the first pair \e p such 
 * \a obj < CAR(p) (according to \a cmp). \a obj is inserted even if it is 
 * already present in \a l.
 *
 * If \a cmp == \ref CCL_DEFAULT_COMPARE_FUNC, addresses of objects are 
 * compared.
 *
 * \param l the list 
 * \param obj the object to insert into \a l
 * \param cmp the comparison function for objects stored in the list \a l
 * \pre l != NULL
 * \return the position (starting from 0) where \a obj is inserted. 
 */
extern int
ccl_list_insert (ccl_list *l, void *obj, ccl_compare_func *cmp);

extern void
ccl_list_insert_before (ccl_list *l, void *obj, void *inserted);

extern void
ccl_list_insert_after (ccl_list *l, void *obj, void *inserted);

/*!
 * \brief Checks if \a l1 and \a l2 are equals
 *
 * The function checks if the two lists \a l1 and \a l2 are equal i.e. they
 * represent the same sequence of pointed objects.
 *
 * \param l1 the first list 
 * \param l2 the second list 
 * \pre l1 != NULL
 * \pre l2 != NULL
 * \return a non null value if \a l1 and \a l2 are equal.
 */
extern int
ccl_list_equals (const ccl_list *l1, const ccl_list *l2);

/*!
 * \brief Duplicates the list \a l
 *
 * The function creates a new list containing the same pointed objects than
 * \a l. The objects are not duplicated.
 *
 * \param l the list to duplicate
 * \pre l != NULL
 * \return a copy of \a l
 */
extern ccl_list *
ccl_list_dup (const ccl_list *l);

/*!
 * \brief Duplicates the list \a l and stored objects.
 *
 * The function creates a new list containing the same objects than
 * \a l. The objects are duplicated using the \a dup function. 
 * If \a dup == \ref CCL_DEFAULT_DUPLICATE_FUNC then the funtion behaves like
 * \ref ccl_list_dup.
 *
 * \param l the list to duplicate
 * \param dup the function used to duplciate objects
 * \pre l != NULL 
 * \return a copy of \a l
 */
extern ccl_list *
ccl_list_deep_dup (const ccl_list *l, ccl_duplicate_func *dup);

/*!
 * \brief Returns the object at the postion \a index
 * \param l the list
 * \param index the index of the object
 * \pre 0 <= index && index < ccl_list_get_size(l)
 * \return the object of \a l stored at the position \a index
 */
extern void *
ccl_list_get_at (ccl_list *l, int index);

/*!
 * \brief Removes the object \a p from the list \a l
 * 
 * If the object pointed by \a p is found in \a l then the first pair 
 * containing it is removed. The object pointed by \a p is not deleted.
 *
 * \param l the list
 * \param p the object to remove.
 * \pre l != NULL
 */
extern void 
ccl_list_remove (ccl_list *l, void *p);

/*!
 * \brief Removes the objects of \a l2 from \a l1
 * \param l1 the first list 
 * \param l2 the second list 
 * \pre l1 != NULL
 * \pre l2 != NULL
 */
extern void
ccl_list_sub (ccl_list *l1, const ccl_list *l2);

/*!
 * \brief append \a l2 to \a l1
 * \param l1 the first list 
 * \param l2 the second list 
 * \pre l1 != NULL
 * \pre l2 != NULL
 */
extern void
ccl_list_append (ccl_list *l1, const ccl_list *l2);

extern void
ccl_list_deep_append (ccl_list *l1, const ccl_list *l2, 
		      ccl_duplicate_func *dup);

/*!
 * \brief Returns the position of \a ptr in the list \a l.
 *
 * The function looks for the object \a ptr in the list \a l. The search is
 * made according to the comparison function \a cmp.
 * If \a cmp == \ref CCL_DEFAULT_COMPARE_FUNC, the objects are compared using 
 * their addresses.
 * 
 * The function returns the index of the first instance of \a ptr found in \a l
 * or -1 if the object is not found.
 *
 * \param l the list
 * \param ptr the object to find in the list
 * \param cmp the comparison function
 * \pre l != NULL
 * \return the position of \a ptr in \a l or -1 if it is not found.
 */
extern int
ccl_list_get_index (const ccl_list *l, void *ptr, ccl_compare_func *cmp);

/*!
 * \brief Compares the two lists \a l1 and \a l2 with the lexicographical order.
 *
 * The function orders \a l1 and \a l2 with the lexicographic order induced by
 * \a cmp.
 * \param l1 the first list
 * \param l2 the second list
 * \param cmp the comparison function for stored objects
 * \pre l1 != NULL
 * \pre l2 != NULL
 * \retval < 0 if \a l1 < \a l2
 * \retval = 0 if \a l1 == \a l2
 * \retval > 0 if \a l1 > \a l2
 */
extern int
ccl_list_compare (const ccl_list *l1, const ccl_list *l2, 
		  ccl_compare_func *cmp);

/*!
 * \brief Computes a hashed value for the list \a l.
 *
 * The address of stored objects is used to compute the hashed value of \a l.
 *
 * \param l the list
 * \pre l != NULL
 * \return a hashed value for \a l.
 */
extern unsigned int
ccl_list_hash (const ccl_list *l);

/*!
 * \brief Computes a hashed value for the list \a l.
 *
 * The hashed value computed by the function uses the function \a h to compute
 * an hashed value for each object stored in the list. If \a h is 
 * \ref CCL_DEFAULT_HASH_FUNC the function behaves like \ref ccl_list_hash.
 *
 * \param l the list
 * \param h the hashing function for objects.
 * \pre l != NULL
 * \return a hashed value for \a l.
 */
extern unsigned int
ccl_list_hash_all (const ccl_list *l, ccl_hash_func *h);

/*!
 * \brief Return a newly allocated array containing all elements of \a l
 * 
 * Even if the list is empty the return array is not NULL and must be deleted. 
 *
 * \param l the list
 * \param p_size  a pointer to an integer that should receive the size of the
 *        array (i.e. the size of the list).
 * \pre l != NULL
 * \post result != NULL
 * \return a newly allocated array containing all elements of \a l
 */
extern void **
ccl_list_to_array (const ccl_list *l, int *p_size);


extern ccl_pointer_iterator *
ccl_list_get_iterator (ccl_list *l, ccl_duplicate_func *dup, 
		       ccl_delete_proc *del);

extern ccl_list *
ccl_list_create_from_iterator (ccl_pointer_iterator *i);

END_C_DECLS

#endif /* ! __CCL_LIST_H__ */
