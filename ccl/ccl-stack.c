/*
 * ccl-stack.c -- A generic stack
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-stack.h"

typedef struct stack_cell_st {
  void *obj;
  struct stack_cell_st *next;
} stack_cell;

struct ccl_stack_st {
  stack_cell *top;
  int size;
};

			/* --------------- */

ccl_stack *
ccl_stack_create (void)
{
  ccl_stack *result = ccl_new (ccl_stack);

  result->size = 0;
  result->top = NULL;

  return result;
}

			/* --------------- */

void
ccl_stack_delete (ccl_stack *s)
{
  ccl_pre (s != NULL);

  while (! ccl_stack_is_empty (s))
    ccl_stack_pop (s);
  ccl_delete (s);
}

			/* --------------- */

int
ccl_stack_get_size (const ccl_stack *s)
{
  ccl_pre (s != NULL);

  return s->size;
}

			/* --------------- */

void *
ccl_stack_get_top (ccl_stack *s)
{
  ccl_pre (! ccl_stack_is_empty (s));
  
  return s->top->obj;
}

			/* --------------- */

void
ccl_stack_push (ccl_stack *s, void *obj)
{
  stack_cell *c;

  ccl_pre (s != NULL);
  
  c = ccl_new (stack_cell);
  c->obj = obj;
  c->next = s->top;
  s->top = c;
  s->size++;
}

			/* --------------- */

void *
ccl_stack_pop (ccl_stack *s)
{
  void *result;
  stack_cell *c;

  ccl_pre (! ccl_stack_is_empty (s));

  c = s->top;
  result = c->obj;
  s->top = c->next;
  ccl_delete (c);
  s->size--;

  return result;
}

