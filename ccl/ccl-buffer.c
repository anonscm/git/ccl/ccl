/*
 * ccl-buffer.c -- an auto-extensible buffer
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-buffer.h"

#define BUF_MINSIZE 64

typedef struct data_area_st data_area;
struct data_area_st
{
  data_area *next;
  char buf[BUF_MINSIZE];
};


struct ccl_buffer_st
{
  int refcount;
  size_t content_size;
  size_t area_size;
  size_t area_pos;
  data_area first_area;
  data_area *last_area;
};

			/* --------------- */

static data_area *
s_allocate_area (size_t size);

static void
s_new_area (ccl_buffer *buf);

			/* --------------- */

ccl_buffer *
ccl_buffer_create (void)
{
  ccl_buffer *result = ccl_new (ccl_buffer);

  result->refcount = 1;
  result->content_size = 0;
  result->area_size = BUF_MINSIZE;
  result->area_pos = 0;
  result->first_area.next = NULL;
  result->last_area = &result->first_area;

  return result;
}

			/* --------------- */

ccl_buffer *
ccl_buffer_add_reference (ccl_buffer *buf)
{
  ccl_pre (buf != NULL);

  buf->refcount++;
  return buf;
}

			/* --------------- */

void
ccl_buffer_del_reference (ccl_buffer *buf)
{
  data_area *a;
  data_area *next;

  ccl_pre (buf != NULL);
  ccl_pre (buf->refcount > 0);

  buf->refcount--;
  if (buf->refcount > 0)
    return;

  for (a = buf->first_area.next; a; a = next)
    {
      next = a->next;
      ccl_delete (a);
    }
  
  ccl_delete (buf);
}

			/* --------------- */

size_t 
ccl_buffer_get_content_size (ccl_buffer *buf)
{
  ccl_pre (buf != NULL);

  return buf->content_size;
}

			/* --------------- */

void
ccl_buffer_append (ccl_buffer *buf, void *data, size_t datasize)
{
  ccl_pre (buf != NULL);

  buf->content_size += datasize;

  while (datasize > 0)
    {
      size_t to_write = datasize;
      size_t available = buf->area_size - buf->area_pos;
      if (available == 0)
	{
	  s_new_area (buf);
	  available = buf->area_size - buf->area_pos;
	}
	
      if (to_write > available)
	to_write = available;
      ccl_memcpy (buf->last_area->buf + buf->area_pos, data, 
		  sizeof (char) * to_write);
      buf->area_pos += to_write;
      data += to_write;
      datasize -= to_write;
    }
}

			/* --------------- */

void
ccl_buffer_reset (ccl_buffer *buf)
{
  ccl_pre (buf != NULL);
  
  buf->content_size = 0;
  buf->area_size = BUF_MINSIZE;
  buf->area_pos = 0;
  buf->last_area = &buf->first_area;
}

			/* --------------- */

void
ccl_buffer_get_content (ccl_buffer *buf, ccl_buffer_content *pcontent)
{
  data_area *a;
  void *dst;
  size_t a_size;

  ccl_pre (buf != NULL);

  a = &buf->first_area;
  a_size = BUF_MINSIZE;

  ccl_array_ensure_size (*pcontent, buf->content_size);
  dst = pcontent->data;

  while (a != buf->last_area)
    {
      ccl_memcpy (dst, a->buf, sizeof (char) * a_size);
      dst += a_size;
      a = a->next;
      a_size *= 2;
    }
  ccl_memcpy (dst, a->buf, sizeof (char) * buf->area_pos);
  ccl_array_trim_to_size (*pcontent);
}

			/* --------------- */

static data_area *
s_allocate_area (size_t size)
{
  data_area *a = ccl_malloc (sizeof (data_area) + size * sizeof (char));

  a->next = NULL;
  ccl_memzero (a->buf, size * sizeof (char));

  return a;
}

			/* --------------- */

static void
s_new_area (ccl_buffer *buf)
{
  ccl_pre (buf->last_area != NULL);
  
  if (buf->last_area->next == NULL)
    buf->last_area->next = s_allocate_area (2 * buf->area_size);

  buf->last_area = buf->last_area->next;
  buf->area_size *=  2;
  buf->area_pos = 0;
}
