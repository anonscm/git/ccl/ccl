/*
 * ccl-time.c -- Retrieve current time 
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "ccl-time.h"

			/* --------------- */

uint32_t 
ccl_time_get_in_milliseconds (void)
{
  struct rusage r;
  double res;

  if (getrusage (RUSAGE_SELF, &r) == 0)
    {
      res = 1000.0 * r.ru_utime.tv_sec + r.ru_utime.tv_usec / 1000.0;
    }
  else
    {
      res = 1000.0 * (double) clock () / (double) CLOCKS_PER_SEC;
    }
  return (uint32_t) res;
}

			/* --------------- */

void
ccl_time_get_hms (uint32_t t, uint32_t *h, uint32_t *m, uint32_t *s,
		  uint32_t *ms)
{
  *s = t / 1000;
  *ms = t % 1000;
	      
  *m = *s / 60;
  *s = *s % 60;
	      
  *h = *m / 60;
  *m = *m % 60;
}

			/* --------------- */

void
ccl_time_log (ccl_log_type log, uint32_t t)
{
  const char *units[] = { "h", "min", "s", "ms" };
  uint32_t values[4] = { 0, 0, 0, 0 };
  const int nb_values = sizeof (values) / sizeof (values[0]);
  int i = 0;

  ccl_time_get_hms (t, &values[0], &values[1], &values[2], &values[3]);

  for (i = 0; i < nb_values - 1 && values[i] == 0; i++)
    CCL_NOP ();

  ccl_log (log, "%d %s", values[i], units[i]);
  for (i++; i < nb_values; i++)
    ccl_log (log, " %d %s", values[i], units[i]);
}
