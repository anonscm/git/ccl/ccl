/*
 * ccl-common.h -- Common macros and types definitions
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-common.h
 * \brief Some useful and common macros
 * 
 */
#ifndef __CCL_COMMON_H__
# define __CCL_COMMON_H__

# include <ccl/ccl-config.h>

# if CCL_HAVE_STDIO_H
#  include <stdio.h>
# endif /* CCL_HAVE_STDIO_H */

# if CCL_HAVE_STDLIB_H
#  include <stdlib.h>
# endif /* CCL_HAVE_STDLIB_H */

#ifdef CCL_HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif

#ifdef CCL_HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif

#ifdef CCL_STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef CCL_HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef CCL_HAVE_STRING_H
# if !defined CCL_STDC_HEADERS && defined CCL_HAVE_MEMORY_H
#  include <memory.h>
# endif
# include <string.h>
#endif

#ifdef CCL_HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef CCL_HAVE_INTTYPES_H
# include <inttypes.h>
#endif

#ifdef CCL_HAVE_STDINT_H
# include <stdint.h>
#endif

#ifdef CCL_HAVE_UNISTD_H
# include <unistd.h>
#endif 

# if CCL_HAVE_STDARG_H
#  include <stdarg.h>
# endif /* CCL_HAVE_STDARG_H */

# ifdef _ccl_size_t
#  define size_t _ccl_size_t
# endif /* _ccl_size_t */

# ifdef _ccl_uint_32_t
#  define uint32_t _ccl_uint32_t
# endif

# ifdef _ccl_uintptr_t
#  define uintptr_t _ccl_uintptr_t
# endif

# ifndef BEGIN_C_DECLS
#  ifdef __cplusplus 
#   define BEGIN_C_DECLS extern "C" {
#   define END_C_DECLS }
#  else /* ! __cpluplus */
#   define BEGIN_C_DECLS 
#   define END_C_DECLS 
#  endif /* ! __cpluplus */
# endif /* ! BEGIN_C_DECLS */

# ifndef NULL
#  define NULL ((void *) ((uintptr_t) 0))
# endif

/*!
 * \brief "Do-Nothing"  statement
 */
# define CCL_NOP() ((void) 0)

/*!
 * \brief Set the least-significant bit of the address \a _p and casts the 
 * result to the type \a _type.
 */
# define CCL_BITPTR(_type,_p)     ((_type)(((uintptr_t)(_p))|((uintptr_t)1)))

/*!
 * \brief Checks if the least-significant bit of the address \a _p is set or
 * not.
 */
# define CCL_PTRHASBIT(_p)        (((uintptr_t)(_p))&((uintptr_t)1))

/*!
 * \brief Unset the least-significant bit of the address \a _p and casts the
 * result to the type \a _type.
 */
# define CCL_BITPTR2PTR(_type,_p) ((_type)(((uintptr_t)(_p))&(~(uintptr_t)1)))

/*!
 * \brief A simple macro use to annotate a part of the code that has some
 * problem and that must be fixed.
 */
#define CCL_FIXME(_comment_) ((void)0)

#endif /* ! __CCL_COMMON_H__ */
