/*
 * ccl-log.h -- Message displayer 
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-log.h
 * \brief Message displayer 
 */
#ifndef __CCL_LOG_H__
# define __CCL_LOG_H__

# include <stdio.h>
# include <ccl/ccl-common.h>

BEGIN_C_DECLS

/*! 
 * \brief Enum that indicates which kind of message has to be displayed. Each
 * kind of message can be displayed on a separate stream.
 */
enum ccl_log_type_enum {
  CCL_LOG_DISPLAY = 0, /**< \brief Standard output */
  CCL_LOG_WARNING,     /**< \brief Warning information */
  CCL_LOG_ERROR,       /**< \brief Errors stream */
  CCL_LOG_DEBUG,       /**< \brief Debugging informations */
  CCL_LOG_PANIC,       /**< \brief Vital informations displayed before an 
			* urgent exit.
			*/
  CCL_LOG_USR1,        /**< \brief Log type freely available for user */
  CCL_LOG_USR2,        /**< \brief Log type freely available for user */
  CCL_LOG_NB_TYPES
};

/*! 
 * \brief Enum that indicates which kind of message has to be displayed. Each
 * kind of message can be displayed on a separate stream.
 */
typedef enum ccl_log_type_enum ccl_log_type;

/*!
 * \brief Prototype of callback functions receiving messages.
 */
typedef void ccl_log_proc (ccl_log_type type, const char  *msg, void *data);

/*!
 * \brief Boolean variable indicating if debugging traces are actived or not.
 */
extern int ccl_debug_is_on;

/*!
 * \brief Adds a message listener.
 * 
 * The function registers the function \a proc as a message listener. Thus, 
 * each time a new message \e M as to be send, the following statement is
 * executed <em>proc (type, M, data)</em> where type is the \ref ccl_log_type
 * on which the message \e M is sent and \a data is the pointer given when 
 * registering the listener.
 * 
 * \param proc the listener function
 * \param data a pointer to some data passed to \a proc each time it is called.
 */
extern void
ccl_log_add_listener (ccl_log_proc *proc, void *data);

/*!
 * \brief Removes the listener \a proc.
 * \param \proc the listerner function
 * \pre proc must be a registered function.
 */
extern void
ccl_log_remove_listener (ccl_log_proc *proc);

/*!
 * \brief Checks if a listener of messages is yet positioned.
 * \return a non-null value if there exists at list one message listener.
 */
extern int
ccl_log_has_listener (void);

/*!
 * \brief Redirects the data sent to the stream \a type to the listener \a proc.
 *
 * Each time a message is sent on the stream \a type, instead of being treated
 * by registered listeners, it is sent to \a proc.
 *
 * \param type the stream to redirect
 * \param proc the listener function
 * \param data a pointer to some data passed to \a proc each time it is called.
 */
extern void
ccl_log_push_redirection (ccl_log_type type, ccl_log_proc *proc, void *data);

/*!
 * \brief Removes redirection for the stream \a type.
 * \param type the redirected stream
 * \pre The stream \a type must have been redirected.
 */
extern void
ccl_log_pop_redirection (ccl_log_type type);

/*!
 * \brief Sends the message formatted using the specification \a fmt on the
 * stream indicated by \a type.
 * The formating string is written in the same way than the one used by \ref 
 * ccl_string_format.
 * \param type the steam where message is sent
 * \param fmt the formatting string
 */
extern void
ccl_log (ccl_log_type type, const char *fmt, ...);

/*!
 * \brief Behaves like \a ccl_log but the variable arguments are replaced
 * by a \a va_list.
 * \param type the steam where message is sent
 * \param fmt the formatting string
 * \param pa the list of arguments.
 */
extern void
ccl_log_va (ccl_log_type type, const char *fmt, va_list pa);

/*!
 * \brief Behaves like \ref ccl_log but the message is send on \ref 
 * CCL_LOG_DISPLAY
 * \param fmt the formatting string
 */
extern void
ccl_display (const char *fmt, ...);

/*!
 * \brief Behaves like \ref ccl_log but the message is send on \ref 
 * CCL_LOG_WARNING
 * \param fmt the formatting string
 */
extern void
ccl_warning (const char *fmt, ...);
 
/*!
 * \brief Behaves like \ref ccl_log but the message is send on \ref 
 * CCL_LOG_PANIC
 * \param fmt the formatting string
 */
extern void
ccl_panic (const char *fmt, ...);

/*!
 * \brief Behaves like \ref ccl_log but the message is send on \ref 
 * CCL_LOG_ERROR
 * \param fmt the formatting string
 */
extern void
ccl_error (const char *fmt, ...);

/*!
 * \brief Behaves like \ref ccl_log but the message is send on \ref 
 * CCL_LOG_DEBUG
 * 
 * This function maintains a counter that represents a level of information.
 * The user can request the CCL_LOG_DEBUG stream to be restricted to message
 * occuring only when the counter is less than a maximal value.
 * \see ccl_debug_max_level, ccl_debug_push_tab_level and 
 * ccl_debug_pop_tab_level.
 * \param fmt the formatting string
 */
extern void
ccl_debug (const char *fmt, ...);

/*!
 * \brief Indicates maximal level after which nothing else is displayed.
 * If the current level is >= \a maxlevel then messages sent to CCL_LOG_DEBUG
 * are not displayed.
 * If \a maxlevel < 0 then all messages are displayed.
 * If \a maxlevel == 0 then no messages are displayed.
 * \param maxlevel the maximal level of information
 */
extern void
ccl_debug_max_level (int maxlevel);

/*!
 * \brief Increments the level of the CCL_LOG_DEBUG stream.
 */
extern void
ccl_debug_push_tab_level (void);

/*!
 * \brief Decrements the level of the CCL_LOG_DEBUG stream
 */
extern void
ccl_debug_pop_tab_level (void);

/*!
 * \brief Starts a timed block of statements
 */
extern void
ccl_debug_start_timed_block (const char *fmt, ...);

/*!
 * \brief Ends a timed block of statements
 */
extern void
ccl_debug_end_timed_block (void);

# define CCL_DEBUG_START_TIMED_BLOCK(_args) \
  do { if (ccl_debug_is_on) ccl_debug_start_timed_block _args ; } while (0)

# define CCL_DEBUG_END_TIMED_BLOCK() \
  do { if (ccl_debug_is_on) ccl_debug_end_timed_block () ; } while (0)


/*!
 * \brief Redirects messages sent to 'type' stream into the string 'dst'
 *
 * \param type the redirected stream
 * \param dst a pointer to the destination string
 */
extern void
ccl_log_redirect_to_string (ccl_log_type type, char **dst);

extern void
ccl_log_redirect_to_FILE (ccl_log_type type, FILE *output);

END_C_DECLS

#endif /* ! __CCL_LOG_H__ */
