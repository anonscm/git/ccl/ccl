/*
 * ccl-memory.c -- Memory allocators
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include <stdio.h>
#include "ccl-memory.h"

struct exhaustion_handler
{
  struct exhaustion_handler *next;
  ccl_memory_exhaustion_handler *proc;
  void *data;
};

			/* --------------- */

#define TRY_ALLOCATION(_alloc)					\
  do								\
    {								\
      void *result = _alloc;					\
								\
      if (result == NULL)					\
	{							\
	  if (HANDLERS != NULL)					\
	    {							\
	      s_call_exhaustion_handlers ();			\
	      result = _alloc;					\
	      if (result != NULL)				\
		goto end;					\
	    }							\
	  ccl_throw_no_msg (memory_exhausted_exception);	\
	}							\
    end:							\
      return result;						\
    }								\
  while (0)

CCL_DEFINE_EXCEPTION (memory_exhausted_exception, runtime_exception);

			/* --------------- */

static struct exhaustion_handler *HANDLERS = NULL;

			/* --------------- */


static void
s_call_exhaustion_handlers (void)
{
  struct exhaustion_handler *H;

  for (H = HANDLERS; H; H = H->next)
    H->proc (H->data);
}

			/* --------------- */

void
ccl_memory_add_exhaustion_handler (ccl_memory_exhaustion_handler *hdl, 
				   void *data)
{
  struct exhaustion_handler *HDL = ccl_new (struct exhaustion_handler);

  HDL->next = HANDLERS;
  HDL->proc = hdl;
  HDL->data = data;
  HANDLERS = HDL;
}

			/* --------------- */

void
ccl_memory_del_exhaustion_handler (ccl_memory_exhaustion_handler *hdl, 
				   void *data)
{
  struct exhaustion_handler **pH;

  for (pH = &HANDLERS; *pH; pH = &((*pH)->next))
    {
      if ((*pH)->proc == hdl && (*pH)->data == data)
	{
	  struct exhaustion_handler *H = *pH;
	  *pH = H->next;
	  ccl_delete (H);
	  break;
	}
    }
}

			/* --------------- */

void *
ccl_malloc (size_t size)
{
  TRY_ALLOCATION (malloc (size));
}

			/* --------------- */

void
ccl_free (void *ptr)
{
  ccl_pre (ptr != NULL);

  free (ptr);
}

			/* --------------- */

void *
ccl_realloc (void *ptr, size_t size)
{
  TRY_ALLOCATION (realloc (ptr, size));
}

			/* --------------- */

void *
ccl_calloc (size_t nb_el, size_t el_size)
{
  TRY_ALLOCATION (calloc (nb_el, el_size));
}
