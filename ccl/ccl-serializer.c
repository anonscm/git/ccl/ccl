/*
 * ccl-serializer.c -- Serialization function for basic C types
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-serializer.c
 * \brief implementation of serialization function for basic types
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-serializer.h"

			/* --------------- */

static void
s_write_buffer (const uint8_t *buf, int size, FILE *out, 
		ccl_serializer_status *p_err)
{
  int count;

  ccl_pre (buf != NULL);
  ccl_pre (out != NULL);
  ccl_pre (p_err != NULL);
  ccl_pre (*p_err == CCL_SERIALIZER_OK);

  count = fwrite (buf, sizeof (uint8_t), size, out);
  if (count != size)
    *p_err = CCL_SERIALIZER_IO_ERROR;
}

			/* --------------- */

void
ccl_serializer_write_uint8 (uint8_t n, FILE *out, ccl_serializer_status *p_err)
{
  ccl_serializer_write_uint32 (n, out, p_err);
}

			/* --------------- */

void
ccl_serializer_write_int8 (int8_t n, FILE *out, ccl_serializer_status *p_err)
{
  ccl_serializer_write_int32 (n, out, p_err);
}

			/* --------------- */

void
ccl_serializer_write_uint16 (uint16_t n, FILE *out, 
			     ccl_serializer_status *p_err)
{
  ccl_serializer_write_uint32 (n, out, p_err);
}

			/* --------------- */

void
ccl_serializer_write_int16 (int16_t n, FILE *out, ccl_serializer_status *p_err)
{
  ccl_serializer_write_int32 (n, out, p_err);
}

			/* --------------- */

void
ccl_serializer_write_uint32 (uint32_t n, FILE *out, 
			     ccl_serializer_status *p_err)
{
  int len;
  uint8_t buf[5];

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  if (n < 192) 
    {
      len = 1;
      buf[0] = (uint8_t) (0xFF & n);
    } 
  else if (192 <= n && n < 8384) 
    {
      n -= 192;
      len = 2;
      buf[0] = 192 + (uint8_t) (0xFF & (n>>8));
      buf[1] = (uint8_t) (0xFF & n);
    } 
  else 
    {
      len = 5;
      buf[0] = 0xFF;
      buf[1] = (uint8_t) (0xFF & (n>>24));
      buf[2] = (uint8_t) (0xFF & (n>>16));
      buf[3] = (uint8_t) (0xFF & (n>>8));
      buf[4] = (uint8_t) (0xFF & n);
    }
  s_write_buffer (buf, len, out, p_err);
}

			/* --------------- */

void
ccl_serializer_write_int32 (int32_t n, FILE *out, ccl_serializer_status *p_err)
{
  ccl_serializer_write_uint32 (n, out, p_err);
}

			/* --------------- */

void
ccl_serializer_write_string (const char *s, FILE *out,			     
			     ccl_serializer_status *p_err)
{
  uint32_t len = strlen (s);

  ccl_serializer_write_uint32 (len, out,  p_err);
  if (*p_err == CCL_SERIALIZER_OK)
    s_write_buffer ((const uint8_t *) s, len, out, p_err);
}

			/* --------------- */

void
ccl_serializer_read_uint8 (uint8_t *p_n, FILE *in, 
			   ccl_serializer_status *p_err)
{
  uint32_t n;

  ccl_pre (p_n != NULL);

  ccl_serializer_read_uint32 (&n, in, p_err);
  *p_n = (uint8_t) n;
}

			/* --------------- */

void
ccl_serializer_read_int8 (int8_t *p_n, FILE *in, 
			  ccl_serializer_status *p_err)
{
  ccl_serializer_read_int8 (p_n, in, p_err);
}

			/* --------------- */

void
ccl_serializer_read_uint16 (uint16_t *p_n, FILE *in, 
			    ccl_serializer_status *p_err)
{
  uint32_t n;

  ccl_pre (p_n != NULL);

  ccl_serializer_read_uint32 (&n, in, p_err);
  *p_n = (uint16_t) n;
}

			/* --------------- */

void
ccl_serializer_read_int16 (int16_t *p_n, FILE *in, 
			   ccl_serializer_status *p_err)
{
  ccl_serializer_read_uint16 ((uint16_t *) p_n, in, p_err);
}

			/* --------------- */

static void
s_read_buffer (uint8_t *buf, int size, FILE *in, ccl_serializer_status *p_err)
{
  int count;

  ccl_pre (buf != NULL);
  ccl_pre (in != NULL);
  ccl_pre (p_err != NULL);

  if (*p_err != CCL_SERIALIZER_OK)
    return;

  count = fread (buf, sizeof (uint8_t), size, in);
  if (count != size)
    {
      if (feof (in))
	*p_err = CCL_SERIALIZER_EOF;
      else
	*p_err = CCL_SERIALIZER_IO_ERROR;
    }
}

			/* --------------- */

void
ccl_serializer_read_uint32 (uint32_t *p_n, FILE *in, 
			    ccl_serializer_status *p_err)
{
  uint32_t n = 0;
  uint8_t b[4];

  ccl_pre (p_n != NULL);  
  s_read_buffer (&b[0], 1, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    return;

  if (b[0] < 192)
    n = (uint32_t) b[0];
  else if (b[0] == 0xFF)
    {
      s_read_buffer (b, 4, in, p_err);
      n = ((uint32_t) b[0]) << 24;
      n |= ((uint32_t) b[1]) << 16;
      n |= ((uint32_t) b[2]) << 8;
      n |= ((uint32_t) b[3]);
    }
  else 
    {
      s_read_buffer (&b[1], 1, in, p_err);
      n = ((((uint32_t) b[0]) - 192) << 8) + ((uint32_t) b[1]) + 192;
    }

  *p_n = n;
}

			/* --------------- */

void
ccl_serializer_read_int32 (int32_t *p_n, FILE *in, 
			   ccl_serializer_status *p_err)
{
  ccl_serializer_read_uint32 ((uint32_t *) p_n, in, p_err);
}

			/* --------------- */

void
ccl_serializer_read_string (char **p_s, FILE *in, 
			    ccl_serializer_status *p_err)
{
  char *s = NULL;
  uint32_t len = 0;

  ccl_pre (p_s != NULL);
  ccl_serializer_read_uint32 (&len, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    return;
  s = ccl_new_array (char, len + 1);
  s_read_buffer ((uint8_t *) s, len, in, p_err);
  if (*p_err != CCL_SERIALIZER_OK)
    {
      ccl_delete (s);
      s = NULL;
    }
  *p_s = s;
}
