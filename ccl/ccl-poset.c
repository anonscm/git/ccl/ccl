/*
 * ccl-poset.c -- A generic Partially Ordered Set
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-memory.h"
#include "ccl-graph.h"
#include "ccl-assert.h"
#include "ccl-poset.h"

struct ccl_poset_st
{
  int refcount;
  int accept_redundant_edges;
  ccl_duplicate_func *dup;
  ccl_vertex_data_methods vertex_methods;
  ccl_graph *dag;
};

			/* --------------- */

static ccl_edge_data_methods EDGE_METHODS = { NULL, NULL, NULL };

ccl_poset *
ccl_poset_create (int accept_redundant_edges, ccl_duplicate_func *dup, 
		  ccl_compare_func *cmp, ccl_delete_proc *del)
{
  ccl_poset *result = ccl_new (ccl_poset);

  result->accept_redundant_edges = accept_redundant_edges;
  result->dup = dup;
  result->vertex_methods.hash = NULL;
  result->vertex_methods.compare = cmp;
  result->vertex_methods.del = del;
  result->dag = ccl_graph_create (&result->vertex_methods, &EDGE_METHODS);

  return result;
}

			/* --------------- */

ccl_poset *
ccl_poset_add_reference (ccl_poset *poset)
{
  ccl_pre (poset != NULL);

  poset->refcount++;

  return poset;
}

			/* --------------- */

void
ccl_poset_del_reference (ccl_poset *poset)
{
  ccl_pre (poset != NULL);

  poset->refcount--;
  if (poset->refcount == 0)
    ccl_graph_del_reference (poset->dag);
}

			/* --------------- */

int
ccl_poset_get_set_size (ccl_poset *poset)
{
  ccl_pre (poset != NULL);

  return ccl_graph_get_number_of_edges (poset->dag);
}

			/* --------------- */

static ccl_vertex *
s_find_or_add_element (ccl_poset *poset, void *obj)
{
  ccl_pre (poset != NULL);

  obj = poset->dup ? poset->dup (obj) : obj;
  return ccl_graph_find_or_add_vertex (poset->dag, obj);
}

			/* --------------- */

void
ccl_poset_add (ccl_poset *poset, void *obj)
{
  (void) s_find_or_add_element (poset, obj);
}

			/* --------------- */

int
ccl_poset_has (ccl_poset *poset, void *obj)
{
  ccl_pre (poset != NULL);

  return ccl_graph_get_vertex (poset->dag, obj) != NULL;
}

			/* --------------- */

struct search_data
{
  ccl_vertex *v;
  int found;
};

			/* --------------- */

static int
s_search_vertex (ccl_vertex *v, void *data)
{
  struct search_data *sd = data;

  if (sd->found)
    return 1;

  sd->found = (v == sd->v);

  return sd->found;
}

			/* --------------- */

int
ccl_poset_has_path (ccl_poset *poset, void *obj1, void *obj2)
{
  int result;
  ccl_vertex *v[2];

  ccl_pre (poset != NULL);

  v[0] = ccl_graph_get_vertex (poset->dag, obj1);
  v[1] = ccl_graph_get_vertex (poset->dag, obj2);

  if (v[0] == NULL || v[1] == NULL || v[0] == v[1])
    result = 0;
  else
    {
      struct search_data data;

      data.v = v[1];
      data.found = 0;
      ccl_graph_dfs (poset->dag, v[0], 0, s_search_vertex, &data);
      result = data.found;
    }

  return result;
}

			/* --------------- */

void *
ccl_poset_get_object (ccl_poset *poset, void *obj)
{
  void *result;
  ccl_vertex *v;

  ccl_pre (poset != NULL);

  v = ccl_graph_get_vertex (poset->dag, obj);
  if (v == NULL)
    result = NULL;
  else 
    {
      result = ccl_vertex_get_data (v);
      if (poset->dup)
	poset = poset->dup (result);
    }

  return result;  
}

			/* --------------- */

int
ccl_poset_add_pair (ccl_poset *poset,  void *greater, void *least)
{
  ccl_vertex *v[2];

  ccl_pre (poset != NULL);
  
  v[0] = s_find_or_add_element (poset, greater);
  v[1] = s_find_or_add_element (poset, least);
  
  if (ccl_poset_has_path (poset, v[1], v[0]))
    return 0;
  
  if (poset->accept_redundant_edges || !ccl_poset_has_path (poset, v[0], v[1]))
    ccl_graph_add_edge (poset->dag, v[0], v[1], NULL);

  return 1;
}

			/* --------------- */

int
ccl_poset_add_poset (ccl_poset *poset, ccl_poset *other)
{
  int loop = 0;
  ccl_vertex_iterator *vi;

  ccl_pre (poset != NULL); 
  ccl_pre (other != NULL); 

  vi = ccl_graph_get_vertices (other->dag);
  while (ccl_iterator_has_more_elements (vi) && !loop)
    {
      ccl_vertex *v = ccl_iterator_next_element (vi);
      ccl_edge_iterator *ei = ccl_vertex_get_out_edges (v);
      void *data = ccl_vertex_get_data (v);

      ccl_poset_add (poset, data);
      while (ccl_iterator_has_more_elements (ei) && !loop)
	{
	  ccl_edge *e = ccl_iterator_next_element (ei);
	  ccl_vertex *tgt = ccl_edge_get_tgt (e);
	  void *tgt_data = ccl_vertex_get_data (tgt);

	  loop = !ccl_poset_add_pair (poset, data, tgt_data);
	    
	}
      ccl_iterator_delete (ei);
    }
  ccl_iterator_delete (vi);

  return !loop;
}

			/* --------------- */

int
ccl_poset_add_lt (ccl_poset *poset, ccl_poset *poset1, ccl_poset *poset2)
{
  int loop;
  ccl_list *p1_roots_list;
  ccl_vertex_iterator *p1_roots;
  ccl_vertex_iterator *p2_leaves;

  if (!ccl_poset_add_poset (poset, poset1))
    return 0;

  if (!ccl_poset_add_poset (poset, poset2))
    return 0;
  
  p2_leaves = ccl_graph_get_roots (poset2->dag);
  p1_roots = ccl_graph_get_roots (poset1->dag);
  p1_roots_list = 
    ccl_list_create_from_iterator ((ccl_pointer_iterator *) p1_roots);
  ccl_iterator_delete (p1_roots);
  loop = 0;
  while (ccl_iterator_has_more_elements (p2_leaves) && !loop)
    {
      ccl_pair *p;
      ccl_vertex *bot = ccl_iterator_next_element (p2_leaves);
      void *bot_data = ccl_vertex_get_data (bot);

      for (p = FIRST (p1_roots_list); p && !loop; p = CDR (p))
	{
	  ccl_vertex *top = (ccl_vertex *) CAR (p);
	  void *top_data = ccl_vertex_get_data (top);

	  loop = !ccl_poset_add_pair (poset, top_data, bot_data);
	}
    }
  ccl_iterator_delete (p2_leaves);
  ccl_list_delete (p1_roots_list);

  return !loop;
}

			/* --------------- */

void
ccl_poset_reverse (ccl_poset *poset)
{
  ccl_pre (poset != NULL);

  ccl_graph_reverse_edges (poset->dag);
}

			/* --------------- */

struct collect_data
{
  ccl_list *result;
  ccl_duplicate_func *dup;
};

static int
s_add_to_list (ccl_vertex *v, void *data)
{
  struct collect_data *cd = data;
  void *vdata = ccl_vertex_get_data (v);
  
  if (cd->dup != NULL)
    vdata = cd->dup (vdata);

  ccl_list_add (cd->result, vdata);

  return 0;
}

			/* --------------- */

static ccl_list *
s_collect_vertices (ccl_poset *poset, ccl_vertex *root, int backward)
{
  struct collect_data data;

  ccl_pre (poset != NULL);

  data.result = ccl_list_create ();
  data.dup = poset->dup;

  ccl_graph_bfs (poset->dag, root, backward, s_add_to_list, &data);

  return data.result;
}

			/* --------------- */

ccl_list *
ccl_poset_get_greater_objects (ccl_poset *poset, void *ev)
{
  ccl_vertex *v;
  
  ccl_pre (ccl_poset_has (poset, ev));

  v = ccl_graph_get_vertex (poset->dag, ev);
  return s_collect_vertices (poset, v, 1);
}

			/* --------------- */

struct objects_iterator 
{
  ccl_pointer_iterator super;
  ccl_vertex_iterator *vi;
  ccl_duplicate_func *dup;
};

			/* --------------- */

static int
s_object_iterator_has_more_elements (const ccl_pointer_iterator *i)
{
  const struct objects_iterator *oi = (const struct objects_iterator *) i;

  return ccl_iterator_has_more_elements (oi->vi);
}

			/* --------------- */

static void *
s_object_iterator_next_element (ccl_pointer_iterator *i)
{
  struct objects_iterator *oi = (struct objects_iterator *) i;
  ccl_vertex *v = ccl_iterator_next_element (oi->vi);
  void *result = ccl_vertex_get_data (v);

  if (oi->dup != NULL)
    result = oi->dup (result);

  return result;
}

			/* --------------- */
static void
s_object_iterator_delete_iterator (ccl_pointer_iterator *i)
{
  struct objects_iterator *oi = (struct objects_iterator *) i;

  ccl_iterator_delete (oi->vi);
  ccl_delete (oi);
}

			/* --------------- */

ccl_pointer_iterator *
ccl_poset_get_objects (ccl_poset *poset)
{
  struct objects_iterator *result = ccl_new (struct objects_iterator);

  ccl_pre (poset != NULL);


  result->super.has_more_elements = s_object_iterator_has_more_elements;
  result->super.next_element = s_object_iterator_next_element;
  result->super.delete_iterator = s_object_iterator_delete_iterator;
  result->vi = ccl_graph_get_vertices (poset->dag);
  result->dup = poset->dup;

  return (ccl_pointer_iterator *) result;
}

			/* --------------- */

ccl_list *
ccl_poset_get_top_to_down_objects (ccl_poset *poset)
{
  return s_collect_vertices (poset, NULL, 0);
}

			/* --------------- */

ccl_list *
ccl_poset_get_down_to_top_objects (ccl_poset *poset)
{
  return s_collect_vertices (poset, NULL, 1);
}

			/* --------------- */

int
ccl_poset_is_empty (ccl_poset *poset)
{
  return ccl_graph_get_number_of_edges (poset->dag) == 0;
}

			/* --------------- */

static void
s_log_pointer (ccl_log_type log, void *ptr, void *dummy)
{
  ccl_log (log, "%p", ptr);
}

			/* --------------- */

void
ccl_poset_log (ccl_log_type log, ccl_poset *poset, 
	       void (*logproc)(ccl_log_type log, void *obj, void *data),
	       void *data)
{
  ccl_pointer_iterator *i = ccl_poset_get_objects (poset);

  if (! ccl_iterator_has_more_elements (i))
    ccl_log (log, "empty\n");
  else 
    {
      if (logproc == NULL)
	logproc = &s_log_pointer;

      while (ccl_iterator_has_more_elements (i))
	{
	  ccl_pair *p;
	  void *obj = ccl_iterator_next_element (i);
	  ccl_list *gt = ccl_poset_get_greater_objects (poset, obj);
      
	  logproc (log, obj, data);
	  if (ccl_list_get_size (gt) > 0)
	    {
	      if (ccl_list_get_size (gt) > 1) 
		ccl_log (log, " < { ");
	      else 
		ccl_log (log, " < ");

	      for (p = FIRST (gt); p; p = CDR (p))
		{
		  logproc (log, CAR (p), data);
		  if (CDR (p) != NULL)
		    ccl_log (log, ", ");

		  if (poset->vertex_methods.del != NULL)
		    poset->vertex_methods.del (CAR(p));
		}

	      if (ccl_list_get_size (gt) > 1) 
		ccl_log (log, " }");
	    }
	  ccl_log (log, "\n");

	  if (poset->vertex_methods.del != NULL)
	    poset->vertex_methods.del (obj);

	  ccl_list_delete (gt);
	}
    }
  ccl_iterator_delete (i);
}

			/* --------------- */

struct log_vertex_cb_data 
{
  void (*logproc)(ccl_log_type log, void *obj, void *data);
  void *data;
};

static void 
s_log_vertex (ccl_log_type log, ccl_vertex *v, void *data)
{
  struct log_vertex_cb_data *cd = data;
  cd->logproc (log, ccl_vertex_get_data (v), cd->data);
}

void
ccl_poset_log_as_dot (ccl_log_type log, ccl_poset *poset, 
		      void (*logproc)(ccl_log_type log, void *obj, void *data),
		      void *data)
{
  struct log_vertex_cb_data cbdata;

  ccl_pre (poset != NULL);

  cbdata.logproc = logproc;
  cbdata.data = data;
  ccl_graph_log_as_dot (log, poset->dag, NULL, NULL,
			s_log_vertex, &cbdata, NULL, NULL);
}

			/* --------------- */

ccl_poset *
ccl_poset_dup (ccl_poset *poset)
{
  ccl_poset *result = ccl_poset_create (poset->accept_redundant_edges,
					poset->dup,
					poset->vertex_methods.compare,
					poset->vertex_methods.del);

  ccl_poset_add_poset (result, poset);

  return result;
}
