/*
 * ccl-time.h -- Retrieve current time
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_TIME_H__
# define __CCL_TIME_H__

# include <ccl/ccl-log.h>

BEGIN_C_DECLS

extern uint32_t 
ccl_time_get_in_milliseconds (void);

extern void
ccl_time_get_hms (uint32_t t, uint32_t *h, uint32_t *m, uint32_t *s,
		  uint32_t *ms);

extern void
ccl_time_log (ccl_log_type log, uint32_t t);

END_C_DECLS

#endif /* ! __CCL_TIME_H__ */
