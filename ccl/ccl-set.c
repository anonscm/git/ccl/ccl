/*
 * ccl-set.c -- add a comment about this file
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2014 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */
#include "ccl-assert.h"
#include "ccl-set.h"

ccl_set *
ccl_set_create (void)
{
  return ccl_set_create_for_objects (NULL, NULL, NULL);
}

ccl_set *
ccl_set_create_for_objects (ccl_hash_func *hash,
			    ccl_compare_func *cmp,
			    ccl_delete_proc *del)
{
  return ccl_hash_create (hash, cmp, del, NULL);
}

void
ccl_set_delete (ccl_set *set)
{
  ccl_pre (set != NULL);
  
  ccl_hash_delete (set);
}

int
ccl_set_has (const ccl_set *set, void *obj)
{
  ccl_pre (set != NULL);
  
  return ccl_hash_has (set, obj);
}
	     
void
ccl_set_add (ccl_set *set, void *obj)
{
  ccl_pre (set != NULL);
  
  if (! ccl_hash_find (set, obj))
    ccl_hash_insert (set, obj);
}

void
ccl_set_remove (ccl_set *set, void *obj)
{
  ccl_pre (set != NULL);
  
  if (ccl_hash_find (set, obj))
    ccl_hash_remove(set);
}
	     

ccl_pointer_iterator *
ccl_set_get_elements (const ccl_set *set)
{
  ccl_pre (set != NULL);
  
  return ccl_hash_get_elements (set);
}

int
ccl_set_is_empty (const ccl_set *set)
{
  ccl_pre (set != NULL);

  return (ccl_set_get_size (set) == 0);
}

int
ccl_set_get_size (const ccl_set *set)
{
  ccl_pre (set != NULL);

  return ccl_hash_get_size (set);
}

ccl_set *
ccl_set_dup (const ccl_set *set)
{
  const ccl_hash_methods *m = ccl_hash_get_methods (set);
  ccl_set *result = ccl_set_create_for_objects (m->hash, m->compare, m->del);
  ccl_pointer_iterator *pi = ccl_set_get_elements (set);
  while (ccl_iterator_has_more_elements (pi))		       
    ccl_set_add (result, ccl_iterator_next_element (pi));
  ccl_iterator_delete (pi);

  return result;
}

void
ccl_set_merge (ccl_set *set, const ccl_set *other)
{ 
  ccl_pointer_iterator *pi = ccl_set_get_elements (other); 
  while (ccl_iterator_has_more_elements (pi)) 
    ccl_set_add (set, ccl_iterator_next_element (pi)); 
  ccl_iterator_delete (pi); 
}

ccl_set * 
ccl_set_union (const ccl_set *s1, const ccl_set *s2)
{
  ccl_set *result = ccl_set_dup (s1);
  ccl_set_merge (result, s2);
  return result;
}

ccl_set * 
ccl_set_intersect (const ccl_set *s1, const ccl_set *s2)
{
  ccl_set *result = ccl_set_dup (s1);
  ccl_pointer_iterator *pi = ccl_set_get_elements (s2); 
  while (ccl_iterator_has_more_elements (pi)) 
    ccl_set_remove (result, ccl_iterator_next_element (pi)); 
  ccl_iterator_delete (pi); 

  return result;
}
