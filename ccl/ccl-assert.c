/*
 * ccl-assert.c -- Macros allowing to set assertions into the code
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"

CCL_DEFINE_EXCEPTION (assertion, error);
CCL_DEFINE_EXCEPTION (unreachable_code_error, error);

			/* --------------- */

void
ccl_check_condition (int cond, const char *file, int line, const char *msg)
{
  if (!cond)
    {
      ccl_throw__ (&(CCL_EXCEPTION_NAME (assertion)), msg, file, line);
    }
}

