/*
 * ccl-hash.h -- generic hash table
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-hash.h
 * \brief A dynamic association table implemented by hashing.
 * 
 * \a ccl_hash tables associates keys and objects. Keys are hashed into a 
 * hashing-table; objects are stored into entries pointed by keys. 
 * The size of the underlying hash table evolves according to its fill degree
 * (i.e. the mean length of collision chains). That means that if the fill 
 * degree exceeds a pre-defined value then the size of the table is increased;
 * and conversely, if the fill degree falls under the pre-defined level the
 * size of the table is reduced.
 *
 * In order to optimize \e find-add operations, the implementation uses a \e 
 * cursor that is moved by \e find operations. The insertion operation is then
 * made at the position pointed by the cursor. Example:
 * \code
 * if (ccl_hash_find (ht, k)
 *   ccl_hash_insert (ht, object);
 * else
 *   {
 *      ... the key k is already present in the table
 *   }
 * \endcode
 *
 * On should note the key \e k is not an argument of \ref ccl_hash_insert 
 * "insert" operation; indeed, \e k has been attached to the cursor by the
 * \ref ccl_hash_find "find" call.
 */
#ifndef __CCL_HASH_H__
# define __CCL_HASH_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-protos.h>
# include <ccl/ccl-iterator.h>

BEGIN_C_DECLS

/*!
 * \brief abstract type of an hash table.
 */
typedef struct ccl_hash_st ccl_hash;

typedef struct ccl_hash_methods_st {
  ccl_hash_func *hash;
  ccl_compare_func *compare;
  ccl_delete_proc *del;
} ccl_hash_methods;

/*!
 * \brief Entries of the table that are returned by \e ad-hoc iterator.
 */
typedef struct ccl_hash_entry_st ccl_hash_entry;
struct ccl_hash_entry_st {
  void *key;
  void *object;
};

/*!
 * \brief Abstract type of iterators that visit entries of an hash table. The 
 * iterator returns objects of type \ref ccl_hash_entry
 */
CCL_ITERATOR_TYPEDEF (ccl_hash_entry_iterator, ccl_hash_entry);

/*!
 * \brief Creates a new hash table.
 * \param key_hash pointer to a function that computes a hashing value for keys
 * \param key_compare pointer to a comparison function for keys
 * \param key_delete pointer to a deletion function for keys
 * \param object_delete pointer to a deletion function for objects
 * \return the new hash table
 */
extern ccl_hash *
ccl_hash_create (ccl_hash_func *key_hash, ccl_compare_func *key_compare,
		 ccl_delete_proc *key_delete, ccl_delete_proc *object_delete);

extern ccl_hash *
ccl_hash_create_methods_struct (const ccl_hash_methods *methods, 
				ccl_delete_proc *object_delete);

extern const ccl_hash_methods *
ccl_hash_get_methods (const ccl_hash *ht);

/*!
 * \brief Deletes the table \a ht
 *
 * The function releases resources allocated for the table \a ht. 
 * The \e key_delete and \e object_delete function used to create the
 * table are applied to keys and objects.
 *
 * \param ht the table to delete
 * \pre ht != NULL 
 */
extern void
ccl_hash_delete (ccl_hash *ht);

/*! 
 * \brief Looks for \a key in the table \a ht.
 * 
 * The function looks for entry containing the key \a key. During the search
 * the cursor of the table is moved to position where \a key should be placed.
 *
 * \param ht the hash table
 * \param key the key that is looked for.
 * \pre ht != NULL
 * \return a non-null if an entry with the key \a key has been found.
 */
extern int
ccl_hash_find (ccl_hash *ht, void *key);

extern int
ccl_hash_has (const ccl_hash *ht, const void *key);

/*!
 * \brief Returns the object of the entry pointed by the cursor.
 *
 * The function should be call after a positive result of the function
 * \ref ccl_hash_find; in that case, the cursor is placed in a valid position.
 * \param ht the hash table
 * \pre ht != NULL
 * \return the object at the cursor position
 */
extern void *
ccl_hash_get (ccl_hash *ht);

extern void * 
ccl_hash_get_with_key (const ccl_hash * ht, const void * key);

/*!
 * \brief Inserts at the cursor position the \a object.
 * 
 * The function should be call after a use of the function \ref ccl_hash_find.
 * If the cursor indicates an non-empty entry (i.e. the call \ref ccl_hash_find
 * gives a positive answer), then the object currently stored in the table is 
 * removed (and deleted if a function has been furnished to \ref 
 * ccl_hash_create) and replaced by \a object.
 *
 * \param ht the hash table
 * \param object the object to insert in the table
 * \pre ht != NULL
 */
extern void
ccl_hash_insert (ccl_hash *ht, void *object);

/*!
 * \brief Removes the object at the cursor position.
 * 
 * The function should be call after a positive result of the function \ref 
 * ccl_hash_find; in that case, the cursor is placed in a valid position.
 * The function removes (and deletes if a function has been furnished 
 * to \ref ccl_hash_create) the object placed under the cursor. 
 * The key associated with the object is also deleted.
 *
 * \param ht the hash table
 * \pre ht != NULL
 */
extern void
ccl_hash_remove (ccl_hash *ht);

/*!
 * \brief Returns the number of elements in the table.
 * \param ht the hash table
 * \pre ht != NULL
 * \return the number of elements in the table.
 */
extern int
ccl_hash_get_size (const ccl_hash *ht);

/*!
 * \brief Builds an iterator over keys of \a ht.
 * \attention If an object is added or removed from \a ht the behavior of the
 * iterator is undefined.
 * \param ht the hash table
 * \pre ht != NULL
 * \return an iterator over keys of \a ht.
 */
extern ccl_pointer_iterator *
ccl_hash_get_keys (const ccl_hash *ht);

/*!
 * \brief Builds an iterator over objects stored in \a ht.
 * \attention If an object is added or removed from \a ht the behavior of the
 * iterator is undefined.
 * \param ht the hash table
 * \pre ht != NULL
 * \return an iterator over elements of \a ht.
 */
extern ccl_pointer_iterator *
ccl_hash_get_elements (const ccl_hash *ht);

/*!
 * \brief Builds an iterator over entries of \a ht.
 * \attention If an object is added or removed from \a ht the behavior of the
 * iterator is undefined.
 * \param ht the hash table
 * \pre ht != NULL
 * \return an iterator over entries of \a ht.
 */
extern ccl_hash_entry_iterator *
ccl_hash_get_entries (const ccl_hash *ht);

END_C_DECLS

#endif /* ! __CCL_HASH_H__ */
