/*
 * ccl-iterator.h -- A generic iterator interface
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-iterator.h
 * \brief A generic iterator interface
 */
#ifndef __CCL_ITERATOR_H__
# define __CCL_ITERATOR_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-protos.h>

/*!
 * \brief Macro-function used to define and declare a new iterator type.
 *
 * An iterator is a structure containing 3 pointers of functions:
 * \li <tt>int (*has_more_elements) (const _typename_ *i)</tt> that has to 
 * return a non-null value if there remains elements to visit.
 * \li <tt>_eltype_ (*next_element) (_typename_ *i)</tt> that returns the next 
 * element to visite.
 * \li <tt>void (*delete_iterator) (_typename_ *i)</tt> that clean-up the 
 * resources allocated to the iterator.
 *
 * The three methods have to be implemented.
 *
 * \param _typename_ the name of the new type.
 * \param _eltype_ the type of object handled by the iterator.
 */
# define CCL_ITERATOR_TYPEDEF(_typename_,_eltype_) \
typedef struct ccl_iterator_ ## _typename_ ## _st _typename_; \
struct ccl_iterator_ ## _typename_ ## _st { \
  int (*has_more_elements) (const _typename_ *i); \
  _eltype_ (*next_element) (_typename_ *i); \
  void  (*delete_iterator) (_typename_ *i); \
}

/*!
 * \brief The type of iterators returning a \e void pointer.
 */
CCL_ITERATOR_TYPEDEF (ccl_pointer_iterator, void *);

/*!
  * \brief The type of iterators returning an integer.
 */
CCL_ITERATOR_TYPEDEF (ccl_int_iterator, int);

/*!
 * \brief Checks if it remains elements to visit.
 * \param i the iterator
 * \return a non-null value if the underlying container has not been completely
 * visited by the iterator \a i
 */
# define ccl_iterator_has_more_elements(i) ((i)->has_more_elements (i))

/*!
 * \brief Returns the next element
 * \param i the iterator
 * \return the next element accessible by the iterator. The iterator can assume
 * that ccl_iterator_has_more_elements(i) has returned a non null value.
 */
# define ccl_iterator_next_element(i) ((i)->next_element (i))

/*!
 * \brief Releases the resources allocated to \a i.
 * \param i the iterator
 */
# define ccl_iterator_delete(i) ((i)->delete_iterator (i))

BEGIN_C_DECLS

extern ccl_pointer_iterator *
ccl_iterator_crt_filter (ccl_pointer_iterator *base, 
			 int (*accept) (void *obj, void *data), 
			 void *data,
			 ccl_delete_proc *del);

extern ccl_pointer_iterator *
ccl_iterator_crt_concat (ccl_pointer_iterator *i1, ccl_pointer_iterator *i2);

extern ccl_pointer_iterator *
ccl_iterator_crt_sequence (int nb_iterators, ccl_pointer_iterator **iterators);
								    
END_C_DECLS

#endif /* ! __CCL_ITERATOR_H__ */
