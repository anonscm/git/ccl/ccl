/*
 * ccl-tree.c -- Generic non-ordered tree structure
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-memory.h"
#include "ccl-assert.h"
#include "ccl-tree.h"

			/* --------------- */

ccl_tree *
ccl_tree_create (void *object, ccl_delete_proc *del)
{
  ccl_tree *result = ccl_new (ccl_tree);

  result->refcount = 1;
  result->del = del;
  result->object = object;
  result->next = NULL;
  result->childs = NULL;
  result->parent = NULL;
  
  return result;
}

			/* --------------- */

ccl_tree *
ccl_tree_add_reference (ccl_tree *t)
{
  ccl_pre (t != NULL);

  t->refcount++;
  
  return t;
}

			/* --------------- */
void
ccl_tree_del_reference (ccl_tree *t)
{
  ccl_pre (t != NULL);
  ccl_pre (t->refcount > 0);

  t->refcount--;
  if (t->refcount == 0)
    {
      ccl_tree *c;
      ccl_tree *next;

      for (c = t->childs; c; c = next)
	{
	  next = c->next;
	  c->parent = NULL;
	  ccl_tree_del_reference (c);
	}
      if (t->del != NULL)
	ccl_zdelete (t->del, t->object);
      ccl_delete (t);
    }
}

			/* --------------- */

int
ccl_tree_get_depth (const ccl_tree *t)
{
  ccl_tree *c; 
  int result = 0;

  ccl_pre (t != NULL);
  for (c = t->childs; c; c = c->next)
    {
      int d = ccl_tree_get_depth (c);
      if (d > result)
	result = d;
    }

  result++;

  return result;
}

			/* --------------- */

int
ccl_tree_is_leaf (const ccl_tree *t)
{
  ccl_pre (t != NULL);

  return (t->childs == NULL);
}

			/* --------------- */

int
ccl_tree_get_nb_siblings (const ccl_tree *t)
{
  int result;

  ccl_pre (t != NULL);

  for (result = 0; t; t = t->next, result++)
    /* do nothing */;

  return result;    
}

			/* --------------- */

int
ccl_tree_get_nb_childs (const ccl_tree *t)
{
  ccl_pre (t != NULL);

  if (t->childs == NULL)
    return 0;

  return ccl_tree_get_nb_siblings (t->childs);
}

			/* --------------- */

ccl_tree *
ccl_tree_get_child (ccl_tree *t, int index)
{
  int i;
  ccl_tree *c;

  ccl_pre (0 <= index && index < ccl_tree_get_nb_childs (t));

  for (i = 0, c = t->childs; i < index; i++, c = c->next)
    /* do nothing */;

  return ccl_tree_add_reference (c);
}

			/* --------------- */

void
ccl_tree_add_child (ccl_tree *t, ccl_tree *child)
{
  ccl_tree **pc;

  ccl_pre (t != NULL);
  ccl_pre (child != NULL);

  for (pc = &t->childs; *pc; pc = &((*pc)->next))
    /* do nothing */;

  *pc = ccl_tree_add_reference (child);
  ccl_assert (child->parent == NULL);
  child->parent = t;
}

			/* --------------- */

void
ccl_tree_set_label (ccl_tree *t, void *obj, ccl_delete_proc *del)
{
  ccl_pre (t != NULL);

  if (t->del != NULL)
    ccl_zdelete(t->del, t->object);
  t->object = obj;
  t->del = del;
}

			/* --------------- */

static int
s_compute_path (ccl_tree *from, ccl_tree *to, ccl_list *result)
{
  int found;
  ccl_tree *c;

  if (from == to)
    found = 1;
  else
    {
      found = 0;
      for (c = from->childs; c != NULL && !found; c = c->next)
	found = s_compute_path (c, to, result);
    }

  if (found)
    ccl_list_put_first (result, from);

  return found;
}

			/* --------------- */

ccl_list *
ccl_tree_get_path (ccl_tree *from, ccl_tree *to)
{
  ccl_list *result = ccl_list_create ();
  s_compute_path (from, to, result);

  return result;
}

			/* --------------- */

ccl_tree *
ccl_tree_least_common_ancestor (ccl_tree *root, ccl_tree *v1, ccl_tree *v2)
{
  ccl_tree *result = NULL;
  ccl_list *p1 = ccl_tree_get_path (root, v1);
  ccl_list *p2 = ccl_tree_get_path (root, v2);

  ccl_assert (v1 != v2);

  while (! ccl_list_is_empty (p1) && ! ccl_list_is_empty (p2) &&
	 CAR (FIRST (p1)) == CAR (FIRST (p2)))
    {
      result = ccl_list_take_first (p1);
      
      ccl_list_take_first (p2);
    }
  ccl_list_delete (p1);
  ccl_list_delete (p2);
  
  return result;
}
