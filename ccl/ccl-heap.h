/*
 * ccl-heap.h -- A generic binary heap
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_HEAP_H__
# define __CCL_HEAP_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-protos.h>

BEGIN_C_DECLS

typedef struct ccl_heap_st ccl_heap;

extern ccl_heap *
ccl_heap_create (int size, ccl_compare_func *cmp);

extern void
ccl_heap_delete (ccl_heap *h);

extern void
ccl_heap_clear (ccl_heap *h, ccl_delete_proc *del);

extern void
ccl_heap_clear_and_delete (ccl_heap *help, ccl_delete_proc *del);

# define ccl_heap_is_empty(h) (ccl_heap_get_size (h) == 0)

extern int
ccl_heap_get_size (const ccl_heap *h);

extern void *
ccl_heap_get_first (const ccl_heap *h);

extern void *
ccl_heap_take_first (ccl_heap *h);

extern void
ccl_heap_add (ccl_heap *h, void *obj);

extern int
ccl_heap_has (const ccl_heap *h, void *obj);

extern void
ccl_heap_log (ccl_log_type log, ccl_heap *h, 
	      void (*logproc) (ccl_log_type log, void *obj, void *data), 
	      void *data);

END_C_DECLS

#endif /* ! __CCL_HEAP_H__ */
