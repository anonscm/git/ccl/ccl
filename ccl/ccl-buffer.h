/*
 * ccl-buffer.h -- an auto-extensible buffer
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_BUFFER_H__
# define __CCL_BUFFER_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-array.h>

BEGIN_C_DECLS

typedef struct ccl_buffer_st ccl_buffer;
typedef CCL_ARRAY (int8_t) ccl_buffer_content;

extern ccl_buffer *
ccl_buffer_create (void);

extern ccl_buffer *
ccl_buffer_add_reference (ccl_buffer *buf);

extern void
ccl_buffer_del_reference (ccl_buffer *buf);

extern size_t 
ccl_buffer_get_content_size (ccl_buffer *buf);

extern void
ccl_buffer_append (ccl_buffer *buf, void *data, size_t datasize);

extern void
ccl_buffer_reset (ccl_buffer *buf);

extern void
ccl_buffer_get_content (ccl_buffer *buf, ccl_buffer_content *pcontent);

END_C_DECLS

#endif /* ! __CCL_BUFFER_H__ */
