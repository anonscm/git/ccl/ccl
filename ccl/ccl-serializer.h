/*
 * ccl-serializer.h -- Serialization function for basic C types
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-serializer.h
 * \brief Serialization function for basic C types
 * 
 * The detailed description of functions is not given since they all operate 
 * in the same way. The name of each function follows the following convention: 
 *
 * <code>ccl_serializer_<em>mode</em>_<em>datatype</em></code>
 *
 * where <em>mode</em> is either <c>write</c> or <c>read</c> and 
 * <em>datatype</em> is one of the following data type: [u]int{8,16,32} or 
 * string.
 * 
 * The usage of functions is quite intuitive. They takes three arguments:
 * - The first one is the data to serialize for the <c>write</c> mode or a 
 *   pointer to a data with the appropriate type for the <c>read</c> mode.
 * - The second argument is the (input or output) stream on which operates the 
 *   function.
 * - The last argument is a pointer to a variable that should receive the error
 *   code returned by the function (see \ref ccl_serializer_status_enum).
 *
 * Before any other operations, functions always that the current value of the 
 * error code is CCL_SERIALIZER_OK. If this is not the case the function does
 * nothing else and immediately returns.
 * 
 * All pointers used by the serialization function (input streams, pointers to 
 * data and pointers to error codes) are assumed to not be NULL.
 */
#ifndef __CCL_SERIALIZER_H__
# define __CCL_SERIALIZER_H__

# include <ccl/ccl-common.h>

BEGIN_C_DECLS

/*!
 * \brief Error code returned by serialization functions.
 */
typedef enum ccl_serializer_status_enum {
  CCL_SERIALIZER_OK = 0,      /*!< No error occurs. */
  CCL_SERIALIZER_EOF,         /*!< The end of the input stream has been 
				reached. */
  CCL_SERIALIZER_IO_ERROR,    /*!< The write or read operation produces an 
				error raised by the system. */
  CCL_SERIALIZER_DATA_ERROR   /*!< The input stream is malformed. */
} ccl_serializer_status;



/*!
 * \pre out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_uint8 (uint8_t n, FILE *out, ccl_serializer_status *p_err);

/*!
 * \pre out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_int8 (int8_t n, FILE *out, ccl_serializer_status *p_err);

/*!
 * \pre out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_uint16 (uint16_t n, FILE *out, 
			     ccl_serializer_status *p_err);

/*!
 * \pre out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_int16 (int16_t n, FILE *out, ccl_serializer_status *p_err);

/*!
 * \pre out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_uint32 (uint32_t n, FILE *out, 
			     ccl_serializer_status *p_err);

/*!
 * \pre out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_int32 (int32_t n, FILE *out, ccl_serializer_status *p_err);

/*!
 * \pre s != NULL && out != NULL && p_err != NULL
 */
extern void
ccl_serializer_write_string (const char *s, FILE *out, 
			     ccl_serializer_status *p_err);

/*!
 * \pre p_n != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_uint8 (uint8_t *p_n, FILE *in, 
			   ccl_serializer_status *p_err);

/*!
 * \pre p_n != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_int8 (int8_t *p_n, FILE *in, 
			  ccl_serializer_status *p_err);

/*!
 * \pre p_n != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_uint16 (uint16_t *p_n, FILE *in, 
			    ccl_serializer_status *p_err);

/*!
 * \pre p_n != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_int16 (int16_t *p_n, FILE *in, 
			   ccl_serializer_status *p_err);

/*!
 * \pre p_n != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_uint32 (uint32_t *p_n, FILE *in, 
			    ccl_serializer_status *p_err);

/*!
 * \pre p_n != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_int32 (int32_t *p_n, FILE *in, 
			   ccl_serializer_status *p_err);

/*!
 * \pre p_s != NULL && in != NULL && p_err != NULL
 */
extern void
ccl_serializer_read_string (char **p_s, FILE *in, ccl_serializer_status *p_err);

END_C_DECLS

#endif /* ! __CCL_SERIALIZER_H__ */
