/*
 * ccl-bittable.h -- Implementation of vectors of bits
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-bittable.h
 * \brief Implementation of vectors of bits
 * 
 */
#ifndef __CCL_BITTABLE_H__
# define __CCL_BITTABLE_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>
# include <ccl/ccl-iterator.h>

BEGIN_C_DECLS

/*!
 * \brief Abstract type of a vector of bytes.
 */
typedef struct ccl_bittable_st ccl_bittable;

/*!
 * \brief Creates a new table of bits large enough for \a size bits
 * \param size the minimal number of bits of the table
 * \pre size >= 0
 * \return a new table of bits large enough for \a size bits
 */
extern ccl_bittable *
ccl_bittable_create (int size);

/*!
 * \brief Releases resources allocated to \a bt
 * \param bt the table of bits
 * \pre bt != NULL
 */
extern void
ccl_bittable_delete (ccl_bittable *bt);

/*!
 * \brief Returns the number of bits of the table 
 * \param bt the table of bits
 * \pre bt != NULL
 */
extern int
ccl_bittable_get_size (const ccl_bittable *bt);

/*!
 * \brief Checks if the bit at position \a index is set or not.
 * \param bt the table of bits
 * \param index the index of the bit
 * \pre bt != NULL
 * \pre index >= 0
 * \return a non-null value if the bit is set.
 */
extern int
ccl_bittable_has (const ccl_bittable *bt, int index);

/*!
 * \brief Set the bit at position \a index
 * 
 * If \a index is out of the current bounds of \a bt then the table
 * is resized to contain \a index.
 *
 * \param bt the table of bits
 * \param index the index of the bit
 * \pre bt != NULL
 * \pre index >= 0
 */
extern void
ccl_bittable_set (ccl_bittable *bt, int index);

/*!
 * \brief Unset the bit at position \a index
 * 
 * If \a index is out of the current bounds of \a bt then the table
 * is resized to contain \a index.
 *
 * \param bt the table of bits
 * \param index the index of the bit
 * \pre bt != NULL
 * \pre index >= 0
 */
extern void
ccl_bittable_unset (ccl_bittable *bt, int index);

/*!
 * \brief Returns the index of the first non-null bit.
 *
 * \param bt the table of bits
 * \pre bt != NULL
 * \return the index of the first bit different of 0 or a negative value 
 *         all bits are null.
 */
extern int
ccl_bittable_get_first (ccl_bittable *bt);

/*!
 * \brief Returns the index of the first non-null bit encountered after the 
 * position \a prev.
 *
 * \param bt the table of bits
 * \param prev a postion in the table
 * \pre 0 <= prev && prec < ccl_bittable_get_size (bt)

 * \return the index of the first non-null bit encountered after the 
 *         position \a prev.
 *
 */
extern int
ccl_bittable_get_next (ccl_bittable *bt, int prev);

/*!
 * \brief Sets to zero all bits of \a bt
 *
 * \param bt the table of bits
 * \pre bt != NULL
 */
extern void
ccl_bittable_clear (ccl_bittable *bt);

/*! 
 * \brief Computes an hashed value of the content of \a bt
 * \param bt the table of bits
 * \pre bt != NULL
 * \return an hashed value of the content of \a bt
 */
extern unsigned int
ccl_bittable_hash (const ccl_bittable *bt);

/*!
 * \brief Checks if \a bt1 and \a bt2 are equal.
 *
 * To be equal the two tables must be defined on the same number of bits.
 *
 * \param bt1 the first table
 * \param bt2 the second table
 * \pre bt1 != NULL
 * \pre bt2 != NULL
 * \return a non-null value if \a bt1 and \a bt2 are equal. 
 */
extern int
ccl_bittable_equals (const ccl_bittable *bt1, const ccl_bittable *bt2);

/*!
 * \brief Duplicates the table \a bt.
 * \param bt the table of bits
 * \pre bt != NULL
 * \return a copy of \a bt
 */
extern ccl_bittable *
ccl_bittable_dup (const ccl_bittable *bt);

/*!
 * \brief Returns the number of bits set to 1 in \a bt/
 * \param bt the table of bits
 * \pre bt != NULL
 * \return the number of bits set to 1 in \a bt/
 */
extern int
ccl_bittable_get_nb_one (ccl_bittable *bt);

/*!
 * \brief Returns an iterator that gives positions of bit equal to 1.
 *
 * The behavior of the iterator is undefined if the table is modified between
 * two iterations.
 *
 * \param bt the table of bits
 * \pre bt != NULL
 * \return on iterator over bits equal to 1
 */
extern ccl_int_iterator *
ccl_bittable_get_ones (ccl_bittable *bt);

/*!
 * \brief Resize the table of bits \a bt for \a newsize bits.
 * 
 * The function creates a new table for \a newsize bits. The content of \a bt 
 * is copied into the new table between positions \e 0 and <em>newsize-1</em>.
 *
 * \param bt the table of bits
 * \param newsize the new number of bits
 * \pre bt != NULL
 * \return a new table with \a newsize bits; common bits with \a bt are 
 *         kept.
 */
extern ccl_bittable *
ccl_bittable_resize (ccl_bittable *bt, int newsize);

/*!
 * \brief Computes the bitwise disjunction of tables \a bt1 and \a bt2.
 * \param bt1 the first table
 * \param bt2 the second table
 * \pre ccl_bittable_get_size (bt1) == ccl_bittable_get_size (bt2)
 * \return the union of sets of integers represented by \a bt1 and \a bt2
 */
extern ccl_bittable *
ccl_bittable_union (const ccl_bittable *bt1, const ccl_bittable *bt2);

/*!
 * \brief Computes the bitwise disjunction of tables \a bt[0], ..., \a 
 * bt[\a nb_bts-1]
 * \param bts an array of bit tables
 * \param nb_bts number of tables to consider
 * \pre ccl_bittable_get_size (bt[0]) == ccl_bittable_get_size (bt[i]) for
 *  all \e i from 1 to <em>nb_bts-1</em>
 * \return the union of sets of integers represented by \a bt1 and \a bt2
 */
extern ccl_bittable *
ccl_bittable_nary_union (ccl_bittable **bts, int nb_bts);

/*!
 * \brief Computes the bitwise conjunction of tables \a bt1 and \a bt2.
 * \param bt1 the first table
 * \param bt2 the second table
 * \pre ccl_bittable_get_size (bt1) == ccl_bittable_get_size (bt2)
 * \return the intersection of sets of integers represented by \a bt1 and \a bt2
 */
extern ccl_bittable *
ccl_bittable_intersection (const ccl_bittable *bt1, const ccl_bittable *bt2);

/*!
 * \brief Computes the bitwise difference of tables \a bt1 and \a bt2.
 * \param bt1 the first table
 * \param bt2 the second table
 * \pre ccl_bittable_get_size (bt1) == ccl_bittable_get_size (bt2)
 * \return the difference of sets of integers represented by \a bt1 and \a bt2
 */
extern ccl_bittable *
ccl_bittable_sub (const ccl_bittable *bt1, const ccl_bittable *bt2);

/*!
 * \brief Negate the bits of \a bt
 * \param bt the table to complement
 * \pre bt != NULL
 * \return the complement of \a bt
 */
extern ccl_bittable *
ccl_bittable_complement (const ccl_bittable *bt);

/*!
 * \brief Makes a bitwise disjunction between \a bts_dst and \a bt_src but 
 *        on a restricted part of indices.
 *
 * The function computes a bitwise disjunction of two windows in \a bt_dst and 
 * \a bt_src. Both windows contain \a w bits and start respectively at index
 * \a dst and \a src. The result of the disjunction is stored in \a bt_dst 
 * at position \a dst.
 *
 * \param bt_dst the table receiving the partial union
 * \param dst the start index the window in \a bt_dst
 * \param bt_src the second table 
 * \param src the start index the window in \a bt_src
 * \param w the width of the window
 * \pre bt_dst != bt_src || dst + w - 1 < src || src + w - 1 < dst
 * \pre src + w - 1 < ccl_bittable_get_size(bt_src)
 * \pre dst + w - 1 < ccl_bittable_get_size(bt_dst)
 */
extern void
ccl_bittable_window_union (ccl_bittable *bt_dst, int dst, 
			   const ccl_bittable *bt_src, int src, int w);

/*!
 * \brief Does the same work than \ref ccl_bittable_window_union but
 * the window of \a bt_src is rolled for 1 bit on the right (i.e. circular shift
 * on the right) prior the disjunction.
 *
 * \param bt_dst the table receiving the partial union
 * \param dst the start index the window in \a bt_dst
 * \param bt_src the second table 
 * \param src the start index the window in \a bt_src
 * \param w the width of the window
 * \pre bt_dst != bt_src || dst + w - 1 < src || src + w - 1 < dst
 * \pre src + w - 1 < ccl_bittable_get_size(bt_src)
 * \pre dst + w - 1 < ccl_bittable_get_size(bt_dst)
 */
extern int
ccl_bittable_window_union_with_roll (ccl_bittable *bt_dst, int dst, 
				     const ccl_bittable *bt_src, int src, 
				     int w);

/*!
 * \brief Checks if the set represented by \a bt1 is included in \a bt2
 * \param bt1 the first table of bits
 * \param bt2 the second table of bits
 * \pre ccl_bittable_get_size (bt1) == ccl_bittable_get_size (bt2)
 * \return a non-null value if all bits set to 1 in \a bt1 are also set to 1 
 *         in \a bt2
 */
extern int
ccl_bittable_is_included_in (const ccl_bittable *bt1, const ccl_bittable *bt2);

/*!
 * \brief Display the content of the table bt to the stream \a log
 * \param log the stream
 * \param bt the table of bits
 * \pre bt != NULL
 */
extern void
ccl_bittable_log (ccl_log_type log, const ccl_bittable *bt);

END_C_DECLS

#endif /* ! __CCL_BITTABLE_H__ */
