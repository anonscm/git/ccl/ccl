/*
 * ccl-poset.h -- A generic Partially Ordered Set
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_POSET_H__
# define __CCL_POSET_H__

# include <ccl/ccl-list.h>

BEGIN_C_DECLS

typedef struct ccl_poset_st ccl_poset;

extern ccl_poset *
ccl_poset_create (int accept_redundant_edges, ccl_duplicate_func *dup, 
		  ccl_compare_func *cmp, ccl_delete_proc *del);

extern ccl_poset *
ccl_poset_add_reference (ccl_poset *poset);

extern void
ccl_poset_del_reference (ccl_poset *poset);

extern int
ccl_poset_get_set_size (ccl_poset *poset);

extern void
ccl_poset_add (ccl_poset *poset, void *ev);

extern int
ccl_poset_has (ccl_poset *poset, void *ev);

extern int
ccl_poset_has_path (ccl_poset *poset, void *ev1, void *ev2);

extern void*
ccl_poset_get_object (ccl_poset *poset, void *obj);

extern int
ccl_poset_add_pair (ccl_poset *poset,  void *greater, void *least);

extern int
ccl_poset_add_poset (ccl_poset *poset, ccl_poset *other);

extern int
ccl_poset_add_lt (ccl_poset *poset, ccl_poset *poset1, ccl_poset *poset2);

extern void
ccl_poset_reverse (ccl_poset *poset);

extern ccl_list *
ccl_poset_get_greater_objects (ccl_poset *poset, void *ev);

extern ccl_pointer_iterator *
ccl_poset_get_objects (ccl_poset *poset);

extern ccl_list *
ccl_poset_get_top_to_down_objects (ccl_poset *poset);

extern ccl_list *
ccl_poset_get_down_to_top_objects (ccl_poset *poset);

extern int
ccl_poset_is_empty (ccl_poset *poset);

extern void
ccl_poset_log (ccl_log_type log, ccl_poset *poset, 
	       void (*logproc)(ccl_log_type log, void *obj, void *data),
	       void *data);

extern void
ccl_poset_log_as_dot (ccl_log_type log, ccl_poset *poset, 
		      void (*logproc)(ccl_log_type log, void *obj, void *data),
		      void *data);


extern ccl_poset *
ccl_poset_dup (ccl_poset *poset);

END_C_DECLS

#endif /* ! __CCL_POSET_H__ */
