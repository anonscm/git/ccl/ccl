/*
 * ccl-protos.h Frequently used prototypes of generic functions
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-protos.h
 * \brief Frequently used prototypes of generic functions
 */
#ifndef __CCL_PROTOS_H__
# define __CCL_PROTOS_H__

# include <ccl/ccl-common.h>

BEGIN_C_DECLS

/*!
 * \brief Prototype of functions used to compute a hashed value
 * from an abstract object pointed by \a ptr
 */
typedef unsigned int ccl_hash_func (const void *ptr);

/*!
 * \brief Special pointer to a \a ccl_hash_func indicating that a \e default
 * hashing function should be used.
 */
# define CCL_DEFAULT_HASH_FUNC ((ccl_hash_func *) NULL)

/*!
 * \brief Prototype of procedures used to release the resources allocated
 * to the object pointed by \a ptr
 */
typedef void ccl_delete_proc (void *ptr);

/*!
 * \brief Special pointer to a \a ccl_delete_proc indicating that no deletion
 * has to be used.
 */
# define CCL_NO_DELETE_PROC ((ccl_delete_proc *) NULL)

/*!
 * \brief Prototype of functions used to check the equality of two objects
 * pointed respectively by \a ptr1 and \a ptr2. 
 * \return a non null value if the object are equal.
 */
typedef int ccl_equals_func (const void *ptr1, const void *ptr2);

/*!
 * \brief Special pointer to a \a ccl_equals_func indicating that a \e default
 * equality test should be used.
 */
# define CCL_DEFAULT_EQUALS_FUNC ((ccl_equals_func *) NULL)

/*!
 * \brief Prototype of functions used to order two objects pointed respectively
 * by \a ptr1 and \a ptr2. 
 * \return 
 *  \li a negative value if *ptr1 < *ptr2
 *  \li a null value if *ptr1 == *ptr2
 *  \li a positive value if *ptr1 > *ptr2
 */
typedef int ccl_compare_func (const void *ptr1, const void *ptr2);

/*!
 * \brief Special pointer to a \a ccl_compare_func indicating that a \e default
 * comparison function should be used.
 */
# define CCL_DEFAULT_COMPARE_FUNC ((ccl_compare_func *) NULL)

/*!
 * \brief Prototype of functions used to duplicate an object pointed by \a ptr.
 */
typedef void *ccl_duplicate_func (void *ptr);

/*!
 * \brief Special pointer to a \a ccl_duplcaite_func indicating that a 
 * \e default duplication function should be used.
 */
# define CCL_DEFAULT_DUPLICATE_FUNC ((ccl_duplicate_func *) NULL)

typedef char *ccl_to_string_func (const void *label);

END_C_DECLS

#endif /* ! __CCL_PROTOS_H__ */
