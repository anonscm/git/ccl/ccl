/*
 * ccl-init.h -- CCL initializer and finalizer
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-log-p.h"
#include "ccl-pool-p.h"
#include "ccl-string-p.h"
#include "ccl-init.h"

static int init_counter = 0;

			/* --------------- */
void
ccl_init (void)
{
  if( init_counter == 0 )
    {
      ccl_log_init ();
      ccl_pool_init ();
      ccl_string_init ();
    }

  init_counter++;
}

			/* --------------- */

void
ccl_terminate(void)
{
  --init_counter;
  if (init_counter == 0)
    {
      ccl_string_terminate ();
      ccl_pool_terminate ();
      ccl_log_terminate ();
    }
}
