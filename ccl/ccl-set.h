/*
 * ccl-set.h -- A simple set implementation
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2014 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef CCL_SET_H
# define CCL_SET_H

# include <ccl/ccl-common.h>
# include <ccl/ccl-iterator.h>
# include <ccl/ccl-hash.h>

BEGIN_C_DECLS

typedef ccl_hash ccl_set;

extern ccl_set *
ccl_set_create (void);

extern ccl_set *
ccl_set_create_for_objects (ccl_hash_func *hash,
			    ccl_compare_func *cmp,
			    ccl_delete_proc *del);

extern void
ccl_set_delete (ccl_set *set);

extern int
ccl_set_has (const ccl_set *set, void *obj);
	     
extern void
ccl_set_add (ccl_set *set, void *obj);

extern void
ccl_set_remove (ccl_set *set, void *obj);
	     
extern ccl_pointer_iterator *
ccl_set_get_elements (const ccl_set *set);

extern int
ccl_set_is_empty (const ccl_set *set);

extern int
ccl_set_get_size (const ccl_set *set);

extern ccl_set *
ccl_set_dup (const ccl_set *set);

extern void
ccl_set_merge (ccl_set *set, const ccl_set *other);

extern ccl_set * 
ccl_set_union (const ccl_set *s1, const ccl_set *s2);

extern ccl_set * 
ccl_set_intersect (const ccl_set *s1, const ccl_set *s2);


END_C_DECLS

#endif /* ! CCL_SET_H */
