/*
 * ccl-assert.h -- Macros allowing to set assertions into the code
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-assert.h
 * \brief Macros allowing to set assertions into the code
 * 
 * This is a set of macros that raise an exception when a condition is not
 * satisfied. This macros are enable if the macro-constant 
 * CCL_ENABLE_ASSERTIONS is defined.
 */
#ifndef __CCL_ASSERT_H__
# define __CCL_ASSERT_H__

# ifdef HAVE_CONFIG_H
#  include <config.h>
# endif

# include <ccl/ccl-common.h>
# include <ccl/ccl-exception.h>

BEGIN_C_DECLS

/*!
 * \brief Exception raised when an assertion is violated.
 */
CCL_DECLARE_EXCEPTION (assertion, internal_error);

/*!
 * \brief Exception raised when an assumed unreachable branch of the code is 
 * accessed.
 */
CCL_DECLARE_EXCEPTION (unreachable_code_error, internal_error);

/*!
 * \brief Checks if the assertion \a _cond_ is satisfied
 */
# define ccl_assert(_cond_) \
CCL_CHECK_CONDITION ("assertion", _cond_, __FILE__, __LINE__)

/*!
 * \brief Checks if the pre-condition \a _cond_ is satisfied
 */
# define ccl_pre(_cond_) \
CCL_CHECK_CONDITION ("pre-condition", _cond_, __FILE__, __LINE__)

/*!
 * \brief Checks if the post-condition \a _cond_ is satisfied
 */
# define ccl_post(_cond_) \
CCL_CHECK_CONDITION ("post-condition", _cond_, __FILE__, __LINE__)

# if CCL_ENABLE_ASSERTIONS

#  define CCL_CHECK_CONDITION(_t, _c, _f, _l) \
  ccl_check_condition (_c, _f, _l, "failed " _t " " #_c)

#  define ccl_unreachable() \
  ccl_throw (unreachable_code_error, "unreachable code access")

# else /* ! CCL_ENABLE_ASSERTIONS */
#  define CCL_CHECK_CONDITION(_t,_c,_f,_l) CCL_NOP ()

#  define ccl_unreachable() CCL_NOP ()
  
#endif /* ! CCL_ENABLE_ASSERTIONS */

/*!
 * \brief Shortcut to write the implication of \a _a by \a _b
 */
# define ccl_imply(_a, _b) ((!(_a)) || (_b))

extern void
ccl_check_condition (int cond, const char *file, int line, const char *msg);

END_C_DECLS

#endif /* ! __CCL_ASSERT_H__ */
