/*
 * ccl-tree.h -- Generic not ordered tree structure.
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-tree.h
 * \brief Generic not ordered tree structure.
 *
 * This module implements a generic tree whose nodes are labelled with objects
 * pointed by \c void pointers. 
 */
#ifndef __CCL_TREE_H__
# define __CCL_TREE_H__

# include <ccl/ccl-protos.h>
# include <ccl/ccl-list.h>

BEGIN_C_DECLS

/*!
 * \brief Structure storing a node of a generic tree
 */
typedef struct ccl_tree_st ccl_tree;

/*!
 * \brief Structure storing a node of a generic tree
 */
struct ccl_tree_st
{
  int refcount;
  /*!
   * \brief The object labelling the node
   */
  void *object;

  /*!
   * \brief The procedure call to free resources allocated to the \a object
   */
  ccl_delete_proc *del;

  /*!
   * \brief A pointer to the first sibling of the node
   */
  ccl_tree *next;

  /*!
   * \brief A pointer to the first child of the node
   */
  ccl_tree *childs;
  ccl_tree *parent;
};

/*!
 * \brief Create a leaf node label with the object \a obj. 
 * \param obj the object that labels the node
 * \param del the deletion procedure applyed to \a obj when the node is deleted
 *        or the label replaced.
 * \return a new leaf node
 */
extern ccl_tree *
ccl_tree_create (void *obj, ccl_delete_proc *del);

/*!
 * \brief Increments the counter of references to the tree \a t
 * \param t the tree
 * \pre t != NULL
 * \return \a t
 */
extern ccl_tree *
ccl_tree_add_reference (ccl_tree *t);

/*!
 * \brief Suppresses one reference to \a t. When no more reference exists the
 *        node is deleted.
 * \param t the tree
 * \pre t != NULL
 * \pre t->refcount > 0
 */
extern void
ccl_tree_del_reference (ccl_tree *t);

/*!
 * \brief Returns the depth of the tree \a t.
 *
 * The depth of \a t is the number of nodes in the maximal path from the root 
 * \a t to one of its leaves.
 * 
 * \param t the tree
 * \pre t != NULL
 * \return the depth of the tree \a t
 */
extern int
ccl_tree_get_depth (const ccl_tree *t);

/*!
 * \brief Checks if the node \a t is a leaf.
 * \param t the tree
 * \pre t != NULL
 * \return a non-null value if \a t is not a leaf.
 */
extern int
ccl_tree_is_leaf (const ccl_tree *t);

/*!
 * \brief Returns the number of siblings of \a t.
 *
 * The function counts only siblings accessible from \a t not those placed
 * before \a t in the linked list of sons.
 *
 * \param t the tree
 * \pre t != NULL
 * \return the number of siblings of \a t.
 */
extern int
ccl_tree_get_nb_siblings (const ccl_tree *t);

/*!
 * \brief Returns the number of childs of \a t.
 * \param t the tree
 * \pre t != NULL
 * \return the number of childs of \a t; 0 if \a t is a leaf.
 */
extern int
ccl_tree_get_nb_childs (const ccl_tree *t);

/*!
 * \brief Returns the \a index th child of \a t
 * \param t the tree
 * \param index the index of the child
 * \pre 0 <= index && index < ccl_tree_get_nb_childs (t)
 * \return child of \a t at the position \a index
 */
extern ccl_tree *
ccl_tree_get_child (ccl_tree *t, int index);

/*!
 * \brief Add a new \a child to the tree \a t
 * The \a child is added at the end of the liust of sons of \a t. A new 
 * reference is added to \a child.
 * \param t the tree
 * \param child the new child
 * \pre t != NULL
 * \pre child != NULL
 * \return child of \a t at the position \a index
 */
extern void
ccl_tree_add_child (ccl_tree *t, ccl_tree *child);

/*!
 * \brief Changes the label of \a t with the object \a obj.
 * \param t the tree
 * \param obj the new object
 * \param del the deletion procedure applied to \a obj
 * \pre t != NULL
 */
extern void
ccl_tree_set_label (ccl_tree *t, void *obj, ccl_delete_proc *del);

extern ccl_list *
ccl_tree_get_path (ccl_tree *from, ccl_tree *to);

extern ccl_tree *
ccl_tree_least_common_ancestor (ccl_tree *root, ccl_tree *v1, ccl_tree *v2);

END_C_DECLS

#endif /* ! __CCL_TREE_H__ */
