/*
 * ccl-iterator.c -- A generic iterator interface
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-iterator.h"

struct pointer_filter_iterator 
{
  ccl_pointer_iterator super;
  ccl_pointer_iterator *base;
  int (*accept) (void *obj, void *data);
  void *data;
  ccl_delete_proc *del;
  void *next_element;
};

			/* --------------- */

struct concat_iterator 
{
  ccl_pointer_iterator super;
  ccl_pointer_iterator **pi;
  ccl_pointer_iterator *iterators[1];
};

			/* --------------- */

static int
s_pointer_filter_iterator_has_more_elements (const ccl_pointer_iterator *i)
{
  struct pointer_filter_iterator *pfi = (struct pointer_filter_iterator *) i;

  return (pfi->next_element != NULL);
}

			/* --------------- */

static void *
s_pointer_filter_iterator_next_element (ccl_pointer_iterator *i)
{
  struct pointer_filter_iterator *pfi = (struct pointer_filter_iterator *) i;
  void *result = pfi->next_element;

  pfi->next_element = NULL;
  while (ccl_iterator_has_more_elements (pfi->base))
    {
      void *obj = ccl_iterator_next_element (pfi->base);
      if (pfi->accept (obj, pfi->data))
	{
	  pfi->next_element = obj;
	  break;
	}
      else if (pfi->del != NULL)
	{
	  pfi->del (obj);
	}
    }

  return result;
}

			/* --------------- */

static void 
s_pointer_filter_iterator_delete_iterator (ccl_pointer_iterator *i)
{
  struct pointer_filter_iterator *pfi = (struct pointer_filter_iterator *) i;

  ccl_iterator_delete (pfi->base);
  ccl_delete (pfi);
}

			/* --------------- */

ccl_pointer_iterator *
ccl_iterator_crt_filter (ccl_pointer_iterator *base, 
			 int (*accept) (void *obj, void *data), void *data,
			 ccl_delete_proc *del)
{
  struct pointer_filter_iterator *result = 
    ccl_new (struct pointer_filter_iterator);

  result->super.has_more_elements = s_pointer_filter_iterator_has_more_elements;
  result->super.next_element = s_pointer_filter_iterator_next_element;
  result->super.delete_iterator = s_pointer_filter_iterator_delete_iterator;
  result->base = base;
  result->accept = accept;
  result->data = data;
  result->del = del;
  result->next_element = NULL;

  s_pointer_filter_iterator_next_element ((ccl_pointer_iterator *) result);

  return (ccl_pointer_iterator *) result;
}

			/* --------------- */

static int
s_concat_iterator_has_more_elements (const ccl_pointer_iterator *i)
{
  struct concat_iterator *ci = (struct concat_iterator *) i;
  ccl_pointer_iterator *current = *(ci->pi);

  return (current != NULL && ccl_iterator_has_more_elements (current));
}

			/* --------------- */

static void *
s_concat_iterator_next_element (ccl_pointer_iterator *i)
{
  struct concat_iterator *ci = (struct concat_iterator *) i;   
  void *result = ccl_iterator_next_element (*ci->pi);

  if (! ccl_iterator_has_more_elements (*ci->pi))
    {
      do 
	{	
	  ci->pi++;
	}
      while (*ci->pi && ! ccl_iterator_has_more_elements (*ci->pi));
    }

  return result;
}

			/* --------------- */

static void 
s_concat_iterator_delete_iterator (ccl_pointer_iterator *i)
{
  struct concat_iterator *ci = (struct concat_iterator *) i;
  ccl_pointer_iterator **pi;

  for (pi = ci->iterators; *pi; pi++)
    ccl_iterator_delete (*pi);
  ccl_delete (ci);
}

			/* --------------- */

ccl_pointer_iterator *
ccl_iterator_crt_concat (ccl_pointer_iterator *i1, ccl_pointer_iterator *i2)
{
  ccl_pointer_iterator *t[2];

  t[0] = i1;
  t[1] = i2;

  return ccl_iterator_crt_sequence (2, t);
}

			/* --------------- */

ccl_pointer_iterator *
ccl_iterator_crt_sequence (int nb_iterators, ccl_pointer_iterator **iterators)
{
  int i;
  struct concat_iterator *result = 
    ccl_malloc (sizeof (struct concat_iterator) + 
		nb_iterators * sizeof (ccl_pointer_iterator *));

  ccl_pre (nb_iterators > 1);

  result->super.has_more_elements = s_concat_iterator_has_more_elements;
  result->super.next_element = s_concat_iterator_next_element;
  result->super.delete_iterator = s_concat_iterator_delete_iterator;
  result->iterators[nb_iterators] = NULL;
  result->pi = &(result->iterators[nb_iterators]);
  for (i = 0; i < nb_iterators; i++)
    {
      result->iterators[i] = iterators[i];
      if (*(result->pi) == NULL && 
	  ccl_iterator_has_more_elements (result->iterators[i]))
	result->pi = &(result->iterators[i]);
    }

  return (ccl_pointer_iterator *) result;  
}
