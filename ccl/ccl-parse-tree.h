/*
 * ccl-parse-tree.h -- A generic parse tree
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-parse-tree.h
 * \brief A generic parse tree
 * 
 */
#ifndef __CCL_PARSE_TREE_H__
# define __CCL_PARSE_TREE_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-string.h>

BEGIN_C_DECLS

/*!
 * \brief Possible values that can decorate a node.
 */
typedef union ccl_parse_tree_value_union ccl_parse_tree_value;

/*!
 * \brief Possible values that can decorate a node.
 */
union ccl_parse_tree_value_union {
  /*!
   * \brief An integer value 
   */
  int int_value;   

  /*!
   * \brief A floating point value 
   */     
  double flt_value;

  /*!
   * \brief An allocated string that must be freed when the tree is deleted.
   */
  char *string_value;

  /*!
   * \brief A unique string (not deletable) 
   */
  ccl_ustring id_value;
};

/*!
 * \brief Enum that indicates the kind of data that decorates a node.
 */
enum ccl_parse_tree_value_type_enum {
  /*!
   * \brief An integer value stored in the \ref ccl_parse_tree_value::int_value
   * "int_value" field of a \ref ccl_parse_tree_value.
   */
  CCL_PARSE_TREE_INT,    

  /*! 
   * \brief A floating-point value stored in the \ref 
   * ccl_parse_tree_value::flt_value "flt_value" field of a 
   * \ref ccl_parse_tree_value.
   */
  CCL_PARSE_TREE_FLOAT, 
  /*!
   * \brief A string value stored in the \ref 
   * ccl_parse_tree_value::string_value "string_value" field of a
   * \ref ccl_parse_tree_value.
   */ 
  CCL_PARSE_TREE_STRING,
  /*! \brief An indentifier value stored in the \ref
   * ccl_parse_tree_value::id_value "id_value" field of a 
   * \ref ccl_parse_tree_value.
   */
  CCL_PARSE_TREE_IDENT,  

  /*!
   * \brief No data is attached to the node. 
   */
  CCL_PARSE_TREE_EMPTY   
};

/*!
 * \brief Enum that indicates the kind of data that decorates a node.
 */
typedef enum ccl_parse_tree_value_type_enum ccl_parse_tree_value_type;

/*!
 * \brief Type of a parse-tree node 
 */
typedef struct ccl_parse_tree_st ccl_parse_tree;

/*!
 * \brief Structure encoding of a parse-tree node 
 */
struct ccl_parse_tree_st {
  /*!
   * \brief An integer indicating the kind of the node e.g. the name of the 
   * non-terminal that generates the sub-tree.
   */
  int node_type; 

 /*!
  * \brief A human-readable string of the \ref ccl_parse_tree::node_type 
  * "node_type" field. Mainly for debugging purposes.
  */
  const char *node_type_string;

  /*!
   * \brief Field indicating what type of data is stored into the \ref 
   * ccl_parse_tree::value "value" field.
   */
  ccl_parse_tree_value_type value_type; 

  /*!
   * \brief A value that decorates this. This value is taken from the union
   * \ref ccl_parse_tree_value according to the \ref 
   * ccl_parse_tree::value_type "value_type" field.
   */
  ccl_parse_tree_value value;

  /*!
   * \brief Line of the "file" where this tree starts.
   */
  int line;

  /*!
   * \brief Name of the "file" from which this tree has been read. This field 
   * can indicates something else than a file.
   */
  const char *filename;

  /*!
   * \brief Pointer to the first child of the node (NULL for leaves).
   */
  ccl_parse_tree *child;

  /*!
   * \brief Pointer to the next sibling of the node in the sibling list of the
   * parent node.
   */
  ccl_parse_tree *next;

  /*!
   * \brief Pointer to the next node the global storage list. This list is
   * used to collect all nodes created during the parsing of the file. It is
   * then used to clean up the nodes in case of a jump on error.
   */
  ccl_parse_tree *next_in_container;
};

/*!
 * \brief Creates a new node.
 * \param type identifier of the kind of node (field \ref 
 *        ccl_parse_tree::node_type "node_type")
 * \param type_string a human readable string describing \a type (field \ref
 *        ccl_parse_tree::node_type_string "node_type_string")
 * \param vtype kind of value stored in this node (field \ref 
 *        ccl_parse_tree::value_type "value_type")
 * \param line start line of the sub-tree (field \ref 
 *        ccl_parse_tree::line "line")
 * \param filename name of the file from which comes the node (field \ref 
 *        ccl_parse_tree::filename "filename")
 * \param child pointer to the first child node (field \ref 
 *        ccl_parse_tree::child "child")
 * \param next pointer to the first sibling (field \ref 
 *        ccl_parse_tree::next "next")
 * \param container pointer to the next node in the list of created nodes 
 *        (field \ref ccl_parse_tree::next_in_container "next_in_container")
 * \return a new node.
 */
extern ccl_parse_tree *
ccl_parse_tree_create (int type, const char *type_string, 
		       ccl_parse_tree_value_type vtype, int line, 
		       const char *filename, ccl_parse_tree *child, 
		       ccl_parse_tree *next, ccl_parse_tree *container);

/*!
 * \brief Returns the number of siblings of the node \a t.
 * \param t the node to explore
 * \return the number of siblings of the node \a t
 */
extern int
ccl_parse_tree_count_siblings (ccl_parse_tree *t);

/*!
 * \brief Deletes the node \a t.
 * 
 * The function releases the resources allocated to the node \a t. Sub-trees
 * and siblings are not deleted. Attached \ref ccl_parse_tree::value "values"
 * are deleted according to their \ref ccl_parse_tree::value_type "value_type".
 *
 * \param t the node to delete
 * \pre t != NULL
 */
extern void
ccl_parse_tree_delete_node (ccl_parse_tree *t);

/*!
 * \brief Deletes the tree \a t and its siblings.
 * 
 * The function deletes the tree pointed by \a t and deletes the siblings of 
 * \a t following the \ref ccl_parse_tree::next "next" chaining.
 *
 * \param t the tree to delete
 * \pre t != NULL
 */
extern void
ccl_parse_tree_delete_tree (ccl_parse_tree *t);

/*!
 * \brief Deletes the nodes in the list \a t.
 * 
 * The function deletes the list starting at \a t following the chaining made 
 * with the \ref ccl_parse_tree::next_in_container "next_in_container" field.
 *
 * \param t the tree
 */
extern void
ccl_parse_tree_delete_container (ccl_parse_tree *t);

/*!
 * \brief Reverses the list of siblings starting at the node \a t. 
 * 
 * The function reverses the list starting a \a t following the chaining made 
 * with the \ref ccl_parse_tree::next "next" field.
 *
 * \param t the tree
 */
extern ccl_parse_tree *
ccl_parse_tree_reverse_siblings (ccl_parse_tree *t);

/*!
 * \brief Duplicate the tree \a t.
 * 
 * The function duplicates the tree \a t and its sub-tree. Each 
 * \ref ccl_parse_tree::value "value" field is duplicated according to its kind
 * of value. The \ref ccl_parse_tree::filename "filename" is also duplicated 
 * but as a \ref ccl_ustring.
 *
 * If \a cont is not NULL then each newly created node is chained with \a *cont
 * using the \ref ccl_parse_tree::next_in_container "next_in_container" field 
 * of \a t.
 *
 * \param t the tree to duplicate
 * \param cont the address of a container list
 * \pre t != NULL
 * \return the root of the copy of \a t
 */
extern ccl_parse_tree *
ccl_parse_tree_duplicate (ccl_parse_tree *t, ccl_parse_tree **cont);

END_C_DECLS

#endif /* ! __CCL_PARSE_TREE_H__ */

