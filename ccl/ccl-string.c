/*
 * ccl-string.c -- String manipulation module
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-hash.h"
#include "ccl-string-p.h"

/*
 *
 */
# define BUFFER_INIT_SIZE 1000

static ccl_hash *UNIQUE_STRINGS;

			/* --------------- */

void
ccl_string_init (void)
{
  UNIQUE_STRINGS = 
    ccl_hash_create (ccl_string_hash, ccl_string_compare, ccl_free, NULL);
}

			/* --------------- */

void
ccl_string_terminate (void)
{
  ccl_hash_delete (UNIQUE_STRINGS);
  UNIQUE_STRINGS = NULL;
}

			/* --------------- */

unsigned int
ccl_string_hash (const void *key)
{
  const char *s;
  unsigned int result = 0;

  ccl_pre (key != NULL);
  
  for (s = (const char *) key; *s; s++)
    result = result * 19 + (unsigned int)*s;

  return result;
}

			/* --------------- */

int
ccl_string_equals (const void *str1, const void *str2)
{
  ccl_pre (str1 != NULL);
  ccl_pre (str2 != NULL);

  return (str1 == str2) || (strcmp (str1, str2) == 0);
}

			/* --------------- */

char *
ccl_string_dup (const char *s)
{
  int len;
  char *result;

  ccl_pre (s != NULL);

  len = strlen (s);
  result = (char *) ccl_calloc (sizeof (char), len + 1);
  strcpy (result, s);

  return result;
}

			/* --------------- */

char *
ccl_string_format_new (const char *fmt, ...)
{
  char *result;
  va_list pa;

  va_start (pa, fmt);
  result = ccl_string_format_new_va (fmt, pa);
  va_end (pa);

  return result;
}

			/* --------------- */

char *
ccl_string_format_new_va (const char *fmt, va_list pa)
{
  size_t sz = 0;
  char *result = NULL;
    
  ccl_string_format_va (&result, &sz, fmt, pa);

  return result;
}

			/* --------------- */

void
ccl_string_format (char **dst, size_t *size, const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa, fmt);
  ccl_string_format_va (dst, size, fmt, pa);
  va_end (pa);
}

			/* --------------- */

static void
s_set_buffer_size (char **pbuf, size_t *psize, size_t size)
{
  if (size == 0)
    {
      if (*pbuf != NULL)
	{
	  ccl_delete (*pbuf);
	  *pbuf = NULL;
	  *psize = 0;
	}
    }
  else if (*psize < size)
    {
      if (*pbuf == NULL)
	*pbuf = ccl_new_array (char, size);
      else 
	{
	  char *tmp = ccl_new_array (char, size);
	  ccl_memcpy (tmp, *pbuf, *psize * sizeof (char));
	  ccl_delete (*pbuf);
	  *pbuf = tmp;
	}
      *psize = size;
    }
}

			/* --------------- */

void
ccl_string_format_va (char **dst, size_t *psize, const char *fmt, va_list pa)
{
  size_t aux = 0;

  ccl_pre (dst != NULL);
  ccl_pre (fmt != NULL);

  if (psize == NULL)
    psize = &aux;

  s_set_buffer_size (dst, psize, BUFFER_INIT_SIZE);

  for (;;)
    {
      va_list cpa;
      int ret;

      va_copy (cpa, pa);
      ret = vsnprintf (*dst, *psize, fmt, cpa);
      va_end (cpa);

      ccl_assert (ret >= 0);

      if (((size_t) ret) < *psize)
	break;
      s_set_buffer_size (dst, psize, *psize * 2);
    }
}

			/* --------------- */

void
ccl_string_format_append (char **dst, const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa, fmt);
  ccl_string_format_append_va (dst, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ccl_string_format_append_va (char **dst, const char *fmt, va_list pa)
{
  char *tmp = ccl_string_format_new_va (fmt, pa);

  if (*dst == NULL)
    {
      *dst = tmp;
    }
  else
    {
      int len_dst = strlen (*dst);
      int len_tmp = strlen (tmp);
      char *res = ccl_new_array (char, len_dst + len_tmp + 1);
      strncpy (res, *dst, len_dst);
      strncpy (res + len_dst, tmp, len_tmp);
      ccl_delete (*dst);
      ccl_delete (tmp);
      *dst = res;
    }
}

			/* --------------- */

# define IS_DIGIT(c) (((c)=='0') || ((c)=='1') || ((c)=='2') || ((c)=='3') || \
		      ((c)=='4') || ((c)=='5') || ((c)=='6') || ((c)=='7') || \
		      ((c)=='8') || ((c)=='9'))

int
ccl_string_parse_int (const char *s)
{
  int result;

  if (s == NULL)
    {
      result = 0;
    }
  else
    {
      result = 0;

      if (*s != '0')
	{
	  int sign = 1;

	  if (*s == '-')
	    {
	      sign = -1;
	      s++;
	    }

	  while (*s && IS_DIGIT (*s)) 
	    {
	      result = result * 10 + ((*s) - '0');	  
	      s++;
	    }
	  
	  result *= sign;
	}
    }

  return result;
}

			/* --------------- */

int
ccl_string_parse_boolean (const char *s)
{
  int result;
  
  if (s == NULL) 
    {
      result = 0;
    }
  else if( ccl_string_equals (s, "true") ||
	   ccl_string_equals (s, "TRUE") ||
	   ccl_string_equals (s, "enabled")  ||
	   ccl_string_equals (s, "ENABLED") ||
	   ccl_string_equals (s, "1") )
    {
      result = 1;
    }
  else
    {
      result = 0;
    }

  return result;
}

			/* --------------- */

ccl_ustring
ccl_string_make_unique (const char *s)
{
  ccl_ustring result = NULL;

  ccl_pre (s != NULL);

  if (ccl_hash_has (UNIQUE_STRINGS, s))
    result = ccl_hash_get_with_key (UNIQUE_STRINGS, s);
  else
    {
      result = ccl_string_dup (s);
      ccl_hash_find (UNIQUE_STRINGS, result);
      ccl_hash_insert (UNIQUE_STRINGS, result);
    }

  return result;
}


			/* --------------- */

ccl_ustring
ccl_string_make_unique_from_int (int i)
{
  char tmp[100];

  sprintf (tmp, "%d", i);

  return ccl_string_make_unique (tmp);
}

			/* --------------- */

const char *
ccl_string_has_prefix (const char *pref, const char *arg)
{
  int len;
  const char *result;

  ccl_pre (pref != NULL);
  ccl_pre (arg != NULL);
  
  len = strlen (pref);
  if (strncmp (arg, pref, len) == 0)
    result = arg + len;
  else
    result = NULL;
  return result;
}
