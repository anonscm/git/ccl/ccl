/*
 * ccl-bittable.c -- Implementation of vectors of bits
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-bittable.h"

/**
 *
 */
struct ccl_bittable_st {
  int nb_bits;
  int table_size;
  int first;
  uint32_t *table;
};

/**
 *
 */
typedef struct bittable_iterator_st bittable_iterator;
struct bittable_iterator_st {
  ccl_int_iterator super;
  ccl_bittable*table;
  int current;
};

			/* --------------- */

/**
 *
 */
# define one ((uint32_t) 1)

/**
 *
 */
# define bits_to_uint32(_s_) (((_s_) & 0x1F)?(((_s_) >> 5) + 1):((_s_) >> 5))

/**
 *
 */
# define index_of(_i) ((_i) >> 5)

/**
 *
 */
# define offset_of(_i) ((_i) & 0x1F)

/**
 *
 */
static int
s_bittable_iterator_has_more_elements (const ccl_int_iterator *i);

/**
 *
 */
static int
s_bittable_iterator_next_element (ccl_int_iterator *i);

/**
 *
 */
static void
s_bittable_iterator_delete_iterator (ccl_int_iterator *i);

/**
 *
 */
static int
s_get_first (const ccl_bittable *bt);

/**
 *
 */
static int
s_read_bits (const ccl_bittable *bt, uint32_t *dst_w, int src, int w);

/**
 *
 */
static int
s_write_bits (ccl_bittable *bt, int dst, uint32_t *src_w, int w, int *pchg);

/**
 *
 */
static ccl_int_iterator BITTABLE_ITERATOR = {
  s_bittable_iterator_has_more_elements,
  s_bittable_iterator_next_element,
  s_bittable_iterator_delete_iterator
};
	
			/* --------------- */


ccl_bittable *
ccl_bittable_create (int size)
{
  ccl_bittable *result = ccl_new (ccl_bittable);

  ccl_pre (size >= 0);

  result->nb_bits = size;
  result->table_size = bits_to_uint32 (size);
  result->table = ccl_new_array (uint32_t, result->table_size);
  result->first = size;

  return result;
}

			/* --------------- */

void
ccl_bittable_delete (ccl_bittable *bt)
{
  ccl_pre (bt != NULL);

  ccl_delete (bt->table);
  ccl_delete (bt);
}

			/* --------------- */

int
ccl_bittable_get_size (const ccl_bittable *bt)
{
  ccl_pre (bt != NULL);

  return bt->nb_bits;
}

			/* --------------- */

int
ccl_bittable_has (const ccl_bittable *bt, int i)
{
  int offset = offset_of (i);
  int index = index_of (i);

  ccl_pre (bt != NULL);
  ccl_pre (index >= 0);

  if (index >= bt->table_size || i >= bt->nb_bits)
    return 0;

  return ((bt->table[index] & (one << offset)) != 0);
}

			/* --------------- */

void
ccl_bittable_set (ccl_bittable *bt, int i)
{
  int offset = offset_of (i);
  int index = index_of (i);

  ccl_pre (bt != NULL);
  ccl_pre (i >= 0);

  if (index >= bt->table_size)
    {
      uint32_t *new_table = ccl_new_array (uint32_t, index + 1);
      ccl_memcpy (new_table, bt->table, sizeof (uint32_t) * bt->table_size);
      bt->table_size = index + 1;
      ccl_delete (bt->table);
      bt->table = new_table;
    }

  if (i >= bt->nb_bits) 
    {
      if (bt->first == bt->nb_bits)
	bt->first = i + 1;
      bt->nb_bits = i + 1;
    }

  bt->table[index] |= (one << offset);
  if (i < bt->first)
    bt->first = i;
}

			/* --------------- */

void
ccl_bittable_unset (ccl_bittable *bt, int i)
{
  int offset = offset_of (i);
  int  index = index_of (i);

  ccl_pre (bt != NULL);
  ccl_pre (i >= 0);

  if (index >= bt->table_size)
    {
      uint32_t *new_table = ccl_new_array (uint32_t, index + 1);
      ccl_memcpy (new_table, bt->table, sizeof (uint32_t) * bt->table_size);
      bt->table_size = index + 1;
      ccl_delete (bt->table);
      bt->table = new_table;
    }

  if (i >= bt->nb_bits)
    {
      if (bt->first == bt->nb_bits)
	bt->first = i + 1;
      bt->nb_bits = i + 1;
    }

  bt->table[index] &= ~(one << offset);
  if (i == bt->first)
    {
      int next = ccl_bittable_get_next (bt, i);
      if (next < 0) 
	bt->first = bt->nb_bits;
      else
	bt->first = next;

      ccl_assert (bt->first > i);
    }
}

			/* --------------- */

int
ccl_bittable_get_first (ccl_bittable *bt)
{
  ccl_pre (bt != NULL);

  if (bt->first == bt->nb_bits)
    return -1;

  return bt->first;
}

			/* --------------- */

int
ccl_bittable_get_next (ccl_bittable *bt, int prev)
{
  uint32_t w;
  int  index, offset, result = -1;

  ccl_pre (0 <= prev && prev < ccl_bittable_get_size (bt));

  index = index_of (prev);
  offset = offset_of (prev);
  w = bt->table[index] >> offset;

  for (offset++, w >>= 1; offset < 32; offset++, w >>= 1 )
    {
      if ((w & ((uint32_t) 1)) != 0)
	{
	  result = (index << 5) + offset; 
	  break;
	}
    }

  if (result == -1)
    {
      for (index++; index < bt->table_size; index++)
	{
	  w = bt->table[index];
	  if (w != 0)
	    break;
	}

      if (index < bt->table_size) 
	{
	  index <<= 5;
	  for (offset = 0; offset < 32; offset++, w >>= 1 )
	    {
	      if ( (w & ((uint32_t) 1)) != 0 )
		{
		  result = index+offset; 
		  break;
		}
	    }
	}
    }

  if (result >= bt->nb_bits)
    result = -1;

  return result;
}

			/* --------------- */

void
ccl_bittable_clear (ccl_bittable *bt)
{
  ccl_pre (bt != NULL);

  ccl_memset (bt->table, 0, sizeof (uint32_t) * bt->table_size);
  bt->first = bt->nb_bits;
}


			/* --------------- */

unsigned int
ccl_bittable_hash (const ccl_bittable *bt)
{
  int i;
  unsigned int result;

  ccl_pre (bt != NULL);
  result = bt->nb_bits;
  for (i = 0; i < bt->table_size; i++)
    result = (result << 3) + bt->table[i];

  return result;
}

			/* --------------- */

int
ccl_bittable_equals (const ccl_bittable *bt1, const ccl_bittable *bt2)
{
  int i;

  ccl_pre (bt1 != NULL);
  ccl_pre (bt2 != NULL);

  if (bt1->nb_bits != bt2->nb_bits && bt1->first != bt2->first)
    return 0;

  ccl_assert (bt1->table_size == bt2->table_size);

  for (i = 0; i < bt1->table_size; i++)
    if (bt1->table[i] != bt2->table[i])
      return 0;

  return 1;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_dup (const ccl_bittable *bt)
{
  ccl_bittable *result;

  ccl_pre (bt != NULL);

  result = ccl_new (struct ccl_bittable_st);
  result->nb_bits = bt->nb_bits;
  result->table_size = bt->table_size;
  result->table = ccl_new_array (uint32_t, result->table_size);
  result->first = bt->first;
  ccl_memcpy (result->table, bt->table, sizeof (int) * bt->table_size);

  return result;
}


			/* --------------- */

int
ccl_bittable_get_nb_one (ccl_bittable *bt)
{
  int i;
  int r = 0;

  for (i = ccl_bittable_get_first (bt); i >= 0; 
       i = ccl_bittable_get_next (bt, i))
    r++;

  return r;
}


			/* --------------- */

ccl_int_iterator *
ccl_bittable_get_ones (ccl_bittable *bt)
{
  bittable_iterator *result = ccl_new (bittable_iterator);

  result->super = BITTABLE_ITERATOR;
  result->table = bt;
  result->current = ccl_bittable_get_first (bt);
  
  return (ccl_int_iterator *) result;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_resize (ccl_bittable *bt, int newsize)
{
  int minsize;
  ccl_bittable *result = ccl_bittable_create (newsize);

  ccl_pre (bt != NULL);

  minsize = bt->table_size;
  if (result->table_size < minsize)
    minsize = result->table_size;

  ccl_memcpy (result->table, bt->table, minsize * sizeof (uint32_t));

  if (bt->first == bt->nb_bits || bt->first >= result->nb_bits)
    result->first = result->nb_bits;
  else 
    result->first = bt->first;

  return result;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_union (const ccl_bittable *bt1, const ccl_bittable *bt2)
{
  int i;
  ccl_bittable *bt;
  
  ccl_pre (ccl_bittable_get_size(bt1) == ccl_bittable_get_size(bt2));

  bt = ccl_bittable_create (bt2->nb_bits);
  for (i = 0; i < bt->table_size; i++)
    bt->table[i] = (bt1->table[i] | bt2->table[i]);
  bt->first = s_get_first (bt);

  return bt;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_nary_union (ccl_bittable **bts, int nb_bts)
{
  int b;
  ccl_bittable *result;
    
  ccl_pre (nb_bts > 0);
  ccl_pre (bts != 0);  

  result = ccl_bittable_dup (bts[0]);
  for (b = 1; b < nb_bts; b++)
    {
      int i;
      ccl_assert (result->nb_bits == bts[b]->nb_bits);

      for (i = 0; i < result->table_size; i++)
	result->table[i] |= bts[b]->table[i];
    }
  result->first = s_get_first (result);

  return result;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_intersection (const ccl_bittable *bt1, const ccl_bittable *bt2)
{
  int i;
  ccl_bittable *bt;
  
  ccl_pre (ccl_bittable_get_size(bt1) == ccl_bittable_get_size(bt2));

  bt = ccl_bittable_create (bt2->nb_bits);
  for (i = 0; i < bt->table_size; i++)
    bt->table[i] = (bt1->table[i] & bt2->table[i]);
  bt->first = s_get_first (bt);

  return bt;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_sub (const ccl_bittable *bt1, const ccl_bittable *bt2)
{
  int i;
  ccl_bittable *bt;
  
  ccl_pre (ccl_bittable_get_size(bt1) == ccl_bittable_get_size(bt2));

  bt = ccl_bittable_create (bt2->nb_bits);
  for (i = 0; i < bt->table_size; i++)
    bt->table[i] = (bt1->table[i] & ~bt2->table[i]);
  bt->first = s_get_first (bt);

  return bt;
}

			/* --------------- */

ccl_bittable *
ccl_bittable_complement (const ccl_bittable *bt)
{
  int i;
  ccl_bittable *rbt;
  
  ccl_pre (bt != NULL);  

  rbt = ccl_bittable_create (bt->nb_bits);
  for (i = 0; i < bt->table_size; i++)
    rbt->table[i] = ~ bt->table[i];

  if (i > 0 && (bt->nb_bits & 0x1F) != 0)
    rbt->table[i-1] &= ~(0xFFFFFFFF << (bt->nb_bits & 0x1F));

  rbt->first = s_get_first (rbt);

  return rbt;
}

			/* --------------- */

void
ccl_bittable_window_union (ccl_bittable *bt_dst, int dst, 
			   const ccl_bittable *bt_src, int src, int w)
{
  int d = dst;
  int chg = 0;

  ccl_pre (bt_dst != bt_src || dst + w - 1 < src || src + w - 1 < dst);
  ccl_pre (src + w - 1 < ccl_bittable_get_size (bt_src));
  ccl_pre (dst + w - 1 < ccl_bittable_get_size (bt_dst));

  while (w > 0)
    {
      uint32_t r = 0;
      int nb_readed = s_read_bits (bt_src, &r, src, w);
      int to_write = nb_readed;

      src += nb_readed;

      do 
	{
	  int nb_written = s_write_bits (bt_dst, dst, &r, to_write, &chg);
	  to_write -= nb_written;
	  dst += nb_written;
	} 
      while (to_write > 0);

      w -= nb_readed;
    }
  
  if (d <= bt_dst->first)
    bt_dst->first = s_get_first (bt_dst);
}

			/* --------------- */

int
ccl_bittable_window_union_with_roll (ccl_bittable *bt_dst, int dst, 
				     const ccl_bittable *bt_src, int src, 
				     int w)
{
  int chg, d, c2, c1;

  ccl_pre (bt_dst != bt_src || dst + w - 1 < src || src + w - 1 < dst);
  ccl_pre (src + w - 1 < ccl_bittable_get_size (bt_src));
  ccl_pre (dst + w - 1 < ccl_bittable_get_size (bt_dst));

  chg = 0;
  d = dst;
  c1 = (bt_src->table[index_of (src + w - 1)] >> offset_of (src + w - 1));
  c1 &= 0x1;

  while (w > 0)
    {
      uint32_t r = 0;
      int nb_readed = s_read_bits (bt_src, &r, src, w);
      int to_write = nb_readed;

      ccl_assert (nb_readed > 0);

      src += nb_readed;
      c2 = (r >> (nb_readed - 1)) & 0x1;
      r = (r << 1) | c1;
      c1 = c2;

      do 
	{
	  int nb_written = s_write_bits (bt_dst, dst, &r, to_write, &chg);
	  to_write -= nb_written;
	  dst += nb_written;
	} 
      while (to_write > 0);

      w -= nb_readed;
    }
  
  if (d <= bt_dst->first)
    bt_dst->first = s_get_first (bt_dst);

  return chg;
}

			/* --------------- */

int
ccl_bittable_is_included_in (const ccl_bittable *bt1, const ccl_bittable *bt2)
{
  int i;
  int mask;

  ccl_pre (ccl_bittable_get_size (bt1) == ccl_bittable_get_size (bt2));

  for (i = 0; i < bt1->table_size-1; i++)
    {
      if ((~bt1->table[i]|bt2->table[i]) != 0xFFFFFFFF)
	return 0;
    }

  mask = ~(0xFFFFFFFF << (bt1->nb_bits % 32));

  return (~(bt1->table[i] & mask)|(bt2->table[i] & mask)) == 0xFFFFFFFF;
}

			/* --------------- */

void
ccl_bittable_log (ccl_log_type log, const ccl_bittable *bt)
{
  int i, max;

  ccl_pre (bt != NULL);
  max = ccl_bittable_get_size (bt);
  for (i = 0; i < max; i++)
    {
      const char *s = ccl_bittable_has (bt, i) ? "1" : "0";
      ccl_log (log, "%s", s);
    }
}

			/* --------------- */

static int
s_bittable_iterator_has_more_elements (const ccl_int_iterator *i)
{
  bittable_iterator *bii = (bittable_iterator *) i;

  ccl_pre (bii != NULL);

  return bii->current >= 0;
}

			/* --------------- */

static int
s_bittable_iterator_next_element (ccl_int_iterator *i)
{
  int result;
  bittable_iterator *bii = (bittable_iterator *) i;

  ccl_pre (bii != NULL);

  result = bii->current;
  bii->current = ccl_bittable_get_next (bii->table,result);

  return result;
}

			/* --------------- */

static void
s_bittable_iterator_delete_iterator (ccl_int_iterator *i)
{
  ccl_pre (i != NULL);

  ccl_delete (i);
}

			/* --------------- */

static int
s_get_first (const ccl_bittable *bt)
{
  int i;

  for (i = 0; i < bt->table_size; i++)
    {
      if (bt->table[i] != 0)
	{
	  int result = 32 * i;
	  uint32_t w = bt->table[i];	  
	  
	  while ((w & ((uint32_t) 0x1)) == 0)
	    {
	      w >>= 1;
	      result++;
	    }

	  ccl_assert (ccl_bittable_has (bt, result));

	  return result;
	}
    }

  return bt->nb_bits;
}

			/* --------------- */

static int
s_read_bits (const ccl_bittable *bt, uint32_t *dst_w, int src, int w)
{
  int offset = offset_of (src);
  int nb_readed = 32 - offset;

  if (nb_readed > w)
    nb_readed = w;

  *dst_w = (bt->table[index_of (src)] >> offset);

  if (nb_readed < 32)
    *dst_w &= ~(0xFFFFFFFF << nb_readed);

  return nb_readed;
}

			/* --------------- */

static int
s_write_bits (ccl_bittable *bt, int dst, uint32_t *src_w, int w, int *pchg)
{
  int offset = offset_of (dst);
  int nb_written = 32 - offset;
  uint32_t mask = 0xFFFFFFFF;
  uint32_t val;
  uint32_t oldval;

  if (nb_written > w)
    nb_written = w;

  if (nb_written < 32) 
    mask = ~(0xFFFFFFFF << nb_written);
  val = ((*src_w) & mask) << offset;
  oldval = bt->table[index_of (dst)] & (mask << offset);

  if ((oldval|val) != oldval)
    *pchg = 1;

  bt->table[index_of (dst)] |= val;
  
  *src_w >>= nb_written;

  return nb_written;
}

			/* --------------- */

