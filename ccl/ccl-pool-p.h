/*
 * ccl-pool-p.h -- Private API of ccl_pool's
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_POOL_P_H__
# define __CCL_POOL_P_H__

# include "ccl-pool.h"

extern void
ccl_pool_init (void);

extern void
ccl_pool_terminate (void);

#endif /* ! __CCL_POOL_P_H__ */
