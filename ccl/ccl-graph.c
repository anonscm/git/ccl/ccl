/*
 * ccl-graph.c -- Generic graph
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-exception.h"
#include "ccl-memory.h"
#include "ccl-list.h"
#include "ccl-graph.h"

#define EDGE_IN  0
#define EDGE_OUT 1

struct ccl_graph_st {
  int refcount;
  ccl_hash *vertices;
  ccl_hash *edges;
  ccl_vertex_data_methods *vertex_methods;
  ccl_edge_data_methods *edge_methods;
};

struct ccl_vertex_st {
  int flags;
  void *data;
  ccl_hash *succ;
  ccl_hash *pred;
  ccl_edge *edges[2];
};

struct ccl_edge_st {
  ccl_edge *next[2];
  void *data;
  ccl_vertex *src;
  ccl_vertex *tgt;
};

struct edge_iterator {
  ccl_edge_iterator super;
  int incidence_list;
  ccl_vertex *v;
  ccl_edge *current;
};

struct vertex_filter_iterator {
  ccl_vertex_iterator super;
  ccl_vertex_iterator *vi;
  int (*accept) (ccl_vertex *v, void *data);
  void *data;
  ccl_vertex *next_element;
};

typedef ccl_pointer_iterator *get_iterator_func (void *container);
typedef void map_proc (void *obj, void *data);

			/* --------------- */

static void
s_vertex_delete (ccl_vertex *v);

static void
s_edge_delete (ccl_edge *e);

static ccl_edge **
s_look_for_edge (ccl_edge **list, ccl_edge *e, int ilist);

static int
s_edge_list_size (ccl_edge *list, int ilist, int *p_nb_loops);

static void
s_map (void *container, get_iterator_func *get_i, map_proc *map, void *data);

static ccl_edge_iterator *
s_crt_edge_iterator (int incidence_list, ccl_vertex *v, ccl_edge *current);

static void
s_cleanup_flags (ccl_graph *G);

static ccl_vertex_iterator *
s_vertex_filter_iterator_create (ccl_vertex_iterator *vi,
				 int (*accept) (ccl_vertex *v, void *data),
				 void *data);

static ccl_vertex_data_methods DUMMY_VERTEX_METHODS = { NULL, NULL, NULL };
static ccl_edge_data_methods DUMMY_EDGE_METHODS = { NULL, NULL, NULL };

			/* --------------- */

ccl_graph *
ccl_graph_create (ccl_vertex_data_methods *vertex_methods,
		  ccl_edge_data_methods *edge_methods)
{
  ccl_graph *result = ccl_new (ccl_graph);

  ccl_pre (vertex_methods != NULL);
  ccl_pre (edge_methods != NULL);

  result->refcount = 1;
  result->vertices = 
    ccl_hash_create_methods_struct (vertex_methods, 
				    (ccl_delete_proc *) s_vertex_delete); 
  
  result->edges = ccl_hash_create (NULL, NULL, NULL, 
				   (ccl_delete_proc *) s_edge_delete);
  result->vertex_methods = vertex_methods;
  result->edge_methods = edge_methods;

  return result;
}

			/* --------------- */

ccl_graph *
ccl_graph_add_reference (ccl_graph *G)
{
  ccl_pre (G != NULL);

  G->refcount++;

  return G;
}

			/* --------------- */

void
ccl_graph_del_reference (ccl_graph *G)
{
  ccl_pre (G != NULL);
  ccl_pre (G->refcount > 0);
  
  G->refcount--;
  if (G->refcount == 0)
    {
      ccl_pointer_iterator *ei = ccl_hash_get_keys (G->edges);
      ccl_hash_delete (G->vertices);
      while (ccl_iterator_has_more_elements (ei))
	{	  
	  ccl_edge *e = ccl_iterator_next_element (ei);
	  if (G->edge_methods->del != NULL)
	    G->edge_methods->del (e->data);
	}
      ccl_iterator_delete (ei);
      ccl_hash_delete (G->edges);
      ccl_delete (G);
    }
}

			/* --------------- */

int
ccl_graph_get_number_of_vertices (ccl_graph *G)
{
  ccl_pre (G != NULL);

  return ccl_hash_get_size (G->vertices);
}

			/* --------------- */

void
ccl_graph_map_vertices (ccl_graph *G, ccl_vertex_map_proc *map, void *data)
{  
  ccl_pre (G != NULL);
  ccl_pre (map != NULL);

  s_map (G, (get_iterator_func *) ccl_graph_get_vertices, (map_proc *) map, 
	 data);
}

			/* --------------- */

ccl_vertex_iterator *
ccl_graph_get_vertices (ccl_graph *G)
{
  ccl_pre (G != NULL);

  return (ccl_vertex_iterator *) ccl_hash_get_elements (G->vertices);
}

			/* --------------- */

int
ccl_graph_get_number_of_edges (ccl_graph *G)
{
  ccl_pre (G != NULL);

  return ccl_hash_get_size (G->edges);
}

			/* --------------- */

void
ccl_graph_map_edges (ccl_graph *G, ccl_edge_map_proc *map, void *data)
{
  ccl_pre (G != NULL);
  ccl_pre (map != NULL);

  s_map (G, (get_iterator_func *) ccl_graph_get_edges, (map_proc *) map, data);
}

			/* --------------- */

ccl_edge_iterator *
ccl_graph_get_edges (ccl_graph *G)
{
  ccl_pre (G != NULL);

  return (ccl_edge_iterator *) ccl_hash_get_keys (G->edges);
}


			/* --------------- */

struct log_data
{
  ccl_log_type log;
  void (*dot_vertex_data)(ccl_log_type log, ccl_vertex *v, void *cbdata);
  void *v_cbdata;
  void (*dot_edge_data)(ccl_log_type log, ccl_edge *e, void *cbdata);
  void *e_cbdata;
};

			/* --------------- */

static void
s_log_vertex (ccl_vertex *v, void *data)
{
  struct log_data *d = data;

  ccl_log (d->log, "N%p", v);
  if (d->dot_vertex_data)
    {
      ccl_log (d->log, " [");
      d->dot_vertex_data (d->log, v, d->v_cbdata);
      ccl_log (d->log, "]");
    }
  ccl_log (d->log, ";\n");
}

			/* --------------- */

static void
s_log_edge (ccl_edge *e, void *data)
{
  struct log_data *d = data;

  ccl_log (d->log, "N%p -> N%p", e->src, e->tgt);
  if (d->dot_edge_data)
    {
      ccl_log (d->log, " [");
      d->dot_edge_data (d->log, e, d->e_cbdata);
      ccl_log (d->log, "]");
    }
  ccl_log (d->log, ";\n");
}

			/* --------------- */

void
ccl_graph_log_as_dot (ccl_log_type log, ccl_graph *G,
		      void (*graph_attributes) (ccl_log_type log,
						ccl_graph *G,
						void *cbdata),
		      void *graph_cb_data,
		      void (*dot_vertex_data)(ccl_log_type log, 
					      ccl_vertex *v,
					      void *cbdata),
		      void *vertex_cb_data,
		      void (*dot_edge_data)(ccl_log_type log, 
					    ccl_edge *e,
					    void *cbdata),
		      void *edge_cb_data)
{
  struct log_data data;

  ccl_log (log, "digraph \"G@%p\" {\n", G);

  if (graph_attributes)
    graph_attributes (log, G, graph_cb_data);

  data.log = log;

  data.dot_vertex_data = dot_vertex_data;
  data.v_cbdata = vertex_cb_data;
  data.dot_edge_data = dot_edge_data;
  data.e_cbdata = edge_cb_data;

  ccl_graph_map_vertices (G, s_log_vertex, &data);
  ccl_graph_map_edges (G, s_log_edge, &data);

  ccl_log (log, "}\n");
}

			/* --------------- */

struct dupdata 
{
  ccl_graph *result;
  ccl_hash *vmap;
  ccl_duplicate_func *v_dup;
  ccl_duplicate_func *e_dup;
};

			/* --------------- */

static void
s_duplicate_vertex (ccl_vertex *v, void *data)
{
  struct dupdata *ddata = data;
  void *vdata = ddata->v_dup ? ddata->v_dup (v->data) : v->data;
  ccl_vertex *nv = ccl_graph_add_vertex (ddata->result, vdata);

  ccl_assert (!ccl_hash_find (ddata->vmap, v));

  ccl_hash_find (ddata->vmap, v);
  ccl_hash_insert (ddata->vmap, nv);
}

			/* --------------- */

static void
s_duplicate_edge (ccl_edge *e, void *data)
{
  struct dupdata *ddata = data;
  void *edata = ddata->e_dup ? ddata->e_dup (e->data) : e->data;
  ccl_vertex *nsrc;
  ccl_vertex *ntgt;

  ccl_assert (ccl_hash_find (ddata->vmap, e->src));
  ccl_hash_find (ddata->vmap, e->src);
  nsrc = ccl_hash_get (ddata->vmap);

  ccl_assert (ccl_hash_find (ddata->vmap, e->tgt));
  ccl_hash_find (ddata->vmap, e->tgt);
  ntgt = ccl_hash_get (ddata->vmap);

  ccl_graph_add_edge (ddata->result, nsrc, ntgt, edata);
}

			/* --------------- */

ccl_graph *
ccl_graph_dup (ccl_graph *G, ccl_duplicate_func *dup_vertex_data,
	       ccl_duplicate_func *dup_edge_data)
{
  struct dupdata data;

  data.vmap = ccl_hash_create (NULL, NULL, NULL, NULL);  
  data.result = ccl_graph_create (G->vertex_methods, G->edge_methods);
  data.v_dup = dup_vertex_data;
  data.e_dup = dup_edge_data;
  ccl_graph_map_vertices (G, &s_duplicate_vertex, &data);
  ccl_graph_map_edges (G, &s_duplicate_edge, &data);
  ccl_hash_delete (data.vmap);

  return data.result;
}

			/* --------------- */

ccl_vertex *
ccl_graph_find_or_add_vertex (ccl_graph *G, void *data)
{
  ccl_vertex *result = ccl_graph_get_vertex (G, data);

  if (result == NULL)
    result = ccl_graph_add_vertex (G, data);

  return result;
}

			/* --------------- */

ccl_vertex *
ccl_graph_add_vertex (ccl_graph *G, void *data)
{
  ccl_vertex *result;

  ccl_pre (G != NULL);
  ccl_pre (ccl_graph_get_vertex (G, data) == NULL);
  
  result = ccl_new (ccl_vertex);
  result->data = data;
  result->succ = ccl_hash_create (NULL, NULL, NULL, NULL);
  result->pred = ccl_hash_create (NULL, NULL, NULL, NULL);
  result->edges[EDGE_IN] = NULL;
  result->edges[EDGE_OUT] = NULL;
  ccl_hash_find (G->vertices, data);
  ccl_hash_insert (G->vertices, result);

  return result;
}

			/* --------------- */

void
ccl_graph_remove_vertex (ccl_graph *G, ccl_vertex *v)
{
  int i;

  ccl_pre (G != NULL);
  ccl_pre (v != NULL);
  ccl_pre (ccl_graph_get_vertex (G, v->data) == v);

  for (i = 0; i < 2; i++)
    {
      while (v->edges[i] != NULL)
	ccl_graph_remove_edge (G, v->edges[i]);
    }
  ccl_hash_find (G->vertices, v->data);
  ccl_hash_remove (G->vertices);
}

			/* --------------- */

ccl_vertex *
ccl_graph_get_vertex (const ccl_graph *G, void *data)
{
  ccl_vertex *result = NULL;

  ccl_pre (G != NULL);

  if (ccl_hash_find (G->vertices, data))
    result = ccl_hash_get (G->vertices);

  return result;
}

			/* --------------- */

void *
ccl_vertex_get_data (ccl_vertex *v)
{
  ccl_pre (v != NULL);

  return v->data;
}

			/* --------------- */

void
ccl_vertex_map_edges (ccl_vertex *v, ccl_edge_map_proc *map, void *data)
{
  ccl_pre (v != NULL); 
  ccl_pre (map != NULL); 

  s_map (v, (get_iterator_func *) ccl_vertex_get_edges, (map_proc *) map, data);
}

			/* --------------- */

ccl_edge_iterator *
ccl_vertex_get_edges (ccl_vertex *v)
{
  ccl_pointer_iterator *i1 = (ccl_pointer_iterator *)
    ccl_vertex_get_in_edges (v);
  ccl_pointer_iterator *i2 = (ccl_pointer_iterator *)
    ccl_vertex_get_in_edges (v);
  ccl_edge_iterator *result = (ccl_edge_iterator *)
    ccl_iterator_crt_concat (i1, i2);
  return result;
}

			/* --------------- */

int
ccl_vertex_get_degree (ccl_vertex *v)
{
  int nb_loops = 0;
  int result = (s_edge_list_size (v->edges[EDGE_OUT], EDGE_OUT, &nb_loops) +
		s_edge_list_size (v->edges[EDGE_IN], EDGE_IN, NULL));

  ccl_assert (result >= 2 * nb_loops);
  result -= nb_loops / 2;

  return result;
}

			/* --------------- */

void
ccl_vertex_map_in_edges (ccl_vertex *v, ccl_edge_map_proc *map, void *data)
{
  ccl_pre (v != NULL); 
  ccl_pre (map != NULL); 

  s_map (v, (get_iterator_func *) ccl_vertex_get_in_edges, (map_proc *) map,
	 data);
}

			/* --------------- */

ccl_edge_iterator *
ccl_vertex_get_in_edges (ccl_vertex *v)
{
  ccl_pre (v != NULL);

  return s_crt_edge_iterator (EDGE_IN, NULL, v->edges[EDGE_IN]);
}

			/* --------------- */

int
ccl_vertex_get_in_degree (const ccl_vertex *v)
{
  return s_edge_list_size (v->edges[EDGE_IN], EDGE_IN, NULL);
}

			/* --------------- */

void
ccl_vertex_map_out_edges (ccl_vertex *v, ccl_edge_map_proc *map, void *data)
{
  ccl_pre (v != NULL); 
  ccl_pre (map != NULL); 

  s_map (v, (get_iterator_func *) ccl_vertex_get_out_edges, (map_proc *) map,
	 data);
}

			/* --------------- */

ccl_edge_iterator *
ccl_vertex_get_out_edges (ccl_vertex *v)
{
  ccl_pre (v != NULL);

  return s_crt_edge_iterator (EDGE_OUT, NULL, v->edges[EDGE_OUT]);
}

			/* --------------- */

int
ccl_vertex_get_out_degree (const ccl_vertex *v)
{
  return s_edge_list_size (v->edges[EDGE_OUT], EDGE_OUT, NULL);
}

			/* --------------- */

int
ccl_vertex_has_successor (ccl_vertex *v, ccl_vertex *w)
{
  ccl_pre (v != NULL); 
  ccl_pre (w != NULL); 
  
  return ccl_hash_find (v->succ, w);
}

			/* --------------- */

ccl_vertex_iterator *
ccl_vertex_get_successors (ccl_vertex *v)
{
  ccl_pre (v != NULL); 
  
  return (ccl_vertex_iterator *) ccl_hash_get_keys (v->succ);
}

			/* --------------- */

int
ccl_vertex_has_predecessor (ccl_vertex *v, ccl_vertex *w)
{
  return ccl_vertex_has_successor (w, v);
}

			/* --------------- */

ccl_vertex_iterator *
ccl_vertex_get_predecessors (ccl_vertex *v)
{
  ccl_pre (v != NULL); 
  
  return (ccl_vertex_iterator *) ccl_hash_get_keys (v->pred);
}

			/* --------------- */

ccl_edge *
ccl_graph_add_edge (ccl_graph *G, ccl_vertex *src, ccl_vertex *tgt, void *data)
{
  intptr_t c;
  ccl_edge *result;

  ccl_pre (G != NULL);
  ccl_pre (ccl_graph_get_vertex (G, src->data) == src);
  ccl_pre (ccl_graph_get_vertex (G, tgt->data) == tgt);

  result = ccl_new (ccl_edge);
  result->src = src;
  result->tgt = tgt;
  result->data = data;
  
  result->next[EDGE_IN] = tgt->edges[EDGE_IN];
  tgt->edges[EDGE_IN] = result;

  result->next[EDGE_OUT] = src->edges[EDGE_OUT];
  src->edges[EDGE_OUT] = result;

  ccl_pre (! ccl_hash_find (G->edges, result));
  ccl_hash_find (G->edges, result);
  ccl_hash_insert (G->edges, result);

  
  if (ccl_hash_find (src->succ, tgt))
    c = (intptr_t) ccl_hash_get (src->succ);
  else
    c = 0;
  c++;
  ccl_hash_insert (src->succ, (void *) c);

  if (ccl_hash_find (tgt->pred, src))
    c = (intptr_t) ccl_hash_get (tgt->pred);
  else
    c = 0;
  c++;
  ccl_hash_insert (tgt->pred, (void *) c);

  return result;
}

			/* --------------- */

ccl_edge *
ccl_graph_add_data_edge (ccl_graph *G, void *sdata, void *ddata, void *data)
{
  ccl_vertex *src = ccl_graph_find_or_add_vertex (G, sdata);
  ccl_vertex *tgt = ccl_graph_find_or_add_vertex (G, ddata);

  return ccl_graph_add_edge (G, src, tgt, data);
}

			/* --------------- */

void
ccl_graph_remove_edge (ccl_graph *G, ccl_edge *e)
{
  ccl_edge **p_e;
  ccl_pre (G != NULL);
  ccl_pre (e != NULL);

  p_e = s_look_for_edge (&e->src->edges[EDGE_OUT], e, EDGE_OUT);
  ccl_assert (*p_e != NULL && *p_e == e);
  *p_e = e->next[EDGE_OUT];

  p_e = s_look_for_edge (&e->tgt->edges[EDGE_IN], e, EDGE_IN);
  ccl_assert (*p_e != NULL && *p_e == e);
  *p_e = e->next[EDGE_IN];

  ccl_assert (ccl_hash_find (G->edges, e));

  if (G->edge_methods->del != NULL)
    G->edge_methods->del (e->data);
  
  if (ccl_hash_find (e->src->succ, e->tgt))
    {
      intptr_t c = (intptr_t) ccl_hash_get (e->src->succ);
      if (--c == 0)
	ccl_hash_remove (e->src->succ);
      else
	ccl_hash_insert (e->src->succ, (void *) c);
    }

  if (ccl_hash_find (e->tgt->pred, e->src))
    {
      intptr_t c = (intptr_t) ccl_hash_get (e->tgt->pred);
      if (--c == 0)
	ccl_hash_remove (e->tgt->pred);
      else
	ccl_hash_insert (e->tgt->pred, (void *) c);
    }

  ccl_hash_find (G->edges, e);
  ccl_hash_remove (G->edges);
}

			/* --------------- */

void *
ccl_edge_get_data (const ccl_edge *e)
{
  ccl_pre (e != NULL);

  return e->data;
}


ccl_vertex *
ccl_edge_get_src (const ccl_edge *e)
{
  ccl_pre (e != NULL);

  return e->src;
}

			/* --------------- */

ccl_vertex *
ccl_edge_get_tgt (const ccl_edge *e)
{
  ccl_pre (e != NULL);

  return e->tgt;
}

			/* --------------- */

#define FL_VISITED 0x1

struct dfsdata
{  
  void (*visit_edges) (ccl_vertex *v, struct dfsdata *ddata);
  ccl_vertex_search_func *search;
  void *cbdata;
};

			/* --------------- */

static void
s_dfs (ccl_vertex *v, void *data);

			/* --------------- */

static void
s_dfs_visit_edges_forward (ccl_vertex *v, struct dfsdata *ddata)
{
  ccl_edge *e;

  for (e = v->edges[EDGE_OUT]; e; e = e->next[EDGE_OUT])
    {
      if (e->tgt->flags != 0)
	continue;

      s_dfs (e->tgt, ddata);
    }
}

			/* --------------- */

static void
s_dfs_visit_edges_backward (ccl_vertex *v, struct dfsdata *ddata)
{
  ccl_edge *e;

  for (e = v->edges[EDGE_IN]; e; e = e->next[EDGE_IN])
    {
      if (e->src->flags != 0)
	continue;

      s_dfs (e->src, ddata);
    }
}

			/* --------------- */

static void
s_dfs (ccl_vertex *v, void *data)
{
  if (v->flags == 0)
    {
      struct dfsdata *dd = data;

      v->flags |= FL_VISITED;
      if (dd->search (v, dd->cbdata))
	ccl_throw_no_msg (exception);
      dd->visit_edges (v, data);
    }
}

			/* --------------- */

void
ccl_graph_dfs (ccl_graph *G, ccl_vertex *root, int backward, 
	       ccl_vertex_search_func *search, void *cbdata)
{
  struct dfsdata data;

  s_cleanup_flags (G);

  if (backward)
    data.visit_edges = s_dfs_visit_edges_backward;
  else 
    data.visit_edges = s_dfs_visit_edges_forward;
  data.search = search;
  data.cbdata = cbdata;

  ccl_try (exception)
    {
      if (root == NULL)
	ccl_graph_map_vertices (G, s_dfs, &data);
      else
	s_dfs (root, &data);
    }
  ccl_no_catch;
}

			/* --------------- */


struct bfsdata
{  
  void (*visit_edges) (ccl_vertex *v, struct bfsdata *ddata);
  ccl_list *todo;
  ccl_vertex_search_func *search;
  void *cbdata;
};


static void
s_bfs_visit_edges_forward (ccl_vertex *v, struct bfsdata *ddata)
{
  ccl_edge *e;

  for (e = v->edges[EDGE_OUT]; e; e = e->next[EDGE_OUT])
    {
      if (e->tgt->flags != 0)
	continue;
      e->tgt->flags |= FL_VISITED;
      ccl_list_add (ddata->todo, e->tgt);
    }
}

			/* --------------- */

static void
s_bfs_visit_edges_backward (ccl_vertex *v, struct bfsdata *ddata)
{
  ccl_edge *e;

  for (e = v->edges[EDGE_IN]; e; e = e->next[EDGE_IN])
    {
      if (e->src->flags != 0)
	continue;

      ccl_list_add (ddata->todo, e->src);
    }
}

			/* --------------- */

static void
s_bfs (ccl_vertex *v, void *data)
{
  struct bfsdata *ddata = data;

  if (v->flags != 0)
    return;

  ccl_list_add (ddata->todo, v);

  while (!ccl_list_is_empty (ddata->todo))
    {
      ccl_vertex *v = ccl_list_take_first (ddata->todo);

      if (ddata->search (v, ddata->cbdata))
	ccl_throw_no_msg (exception);

      v->flags |= FL_VISITED;
      ddata->visit_edges (v, ddata);
    }

  ccl_list_clear (ddata->todo, NULL);
}

			/* --------------- */

void
ccl_graph_bfs (ccl_graph *G, ccl_vertex *root, int backward, 
	       ccl_vertex_search_func *search, void *cbdata)
{
  struct bfsdata data;

  data.todo = ccl_list_create ();
  data.search = search;
  data.cbdata = cbdata;
  if (backward)
    data.visit_edges = s_bfs_visit_edges_backward;
  else
    data.visit_edges = s_bfs_visit_edges_forward;

  ccl_try (exception)
    {
      s_cleanup_flags (G);

      if (root == NULL)
	ccl_graph_map_vertices (G, s_bfs, &data);
      else
	s_bfs (root, &data);

    }
  ccl_no_catch;

  ccl_list_delete (data.todo);
}
			/* --------------- */

static void
s_reverse_edges (ccl_vertex *v, void *dummy)
{
  int i;
  ccl_edge *e;

  for (i = 0; i < 2; i++)
    for (e = v->edges[i]; e; e = e->next[i])
      {
	ccl_vertex *aux = e->src;
	e->src = e->tgt;
	e->tgt = aux;
      }
  
  e = v->edges[0];
  v->edges[0] = v->edges[1];
  v->edges[1] = e;
}

			/* --------------- */

void
ccl_graph_reverse_edges (ccl_graph *G)
{
  ccl_graph_map_vertices (G, s_reverse_edges, NULL);
}

			/* --------------- */

static int
s_accept_roots (ccl_vertex *v, void *dummy)
{
  return ccl_vertex_get_in_degree (v) == 0;
}

			/* --------------- */

ccl_vertex_iterator *
ccl_graph_get_roots (ccl_graph *G)
{
  ccl_vertex_iterator *vertices = ccl_graph_get_vertices (G);

  return s_vertex_filter_iterator_create (vertices, s_accept_roots, NULL);
}

			/* --------------- */

static int
s_accept_leaves (ccl_vertex *v, void *dummy)
{
  return ccl_vertex_get_out_degree (v) == 0;
}

			/* --------------- */

ccl_vertex_iterator *
ccl_graph_get_leaves (ccl_graph *G)
{
  ccl_vertex_iterator *vertices = ccl_graph_get_vertices (G);

  return s_vertex_filter_iterator_create (vertices, s_accept_leaves, NULL);
}

			/* --------------- */

struct scc_data
{
  int pref;
  int comp;
  int in_stack;
  int ret;
};

static void
s_scc (ccl_graph *G, ccl_hash *vdata, ccl_list *stack, ccl_vertex *x, 
       int *p_n, int *p_k, 
       void (*attach)(ccl_graph *G, void *data, int comp, void *cbdata),
       void *cbdata)
{
  int m;
  struct scc_data *data;
  ccl_edge_iterator *ei;

  if (!ccl_hash_find (vdata, x))
    {
      data = ccl_new (struct scc_data);
      data->pref = 0;
      data->comp = 0;
      data->in_stack = 0;
      data->ret = 0;
      ccl_hash_insert (vdata, data);
    }
  else
    {
      data = ccl_hash_get (vdata);
    }

  ccl_list_put_first (stack, x);
  data->in_stack = 1;
  
  *p_n = *p_n + 1; 
  m = data->pref = *p_n;

  ei = ccl_vertex_get_out_edges (x);
  while (ccl_iterator_has_more_elements (ei))
    {
      ccl_edge *e = ccl_iterator_next_element (ei);
      ccl_vertex *y = ccl_edge_get_tgt (e);
      struct scc_data *ydata = NULL;

      if (! ccl_hash_find (vdata, y))
	{
	  s_scc (G, vdata, stack, y, p_n, p_k, attach, cbdata);
	  ccl_hash_find (vdata, y);
	  ydata = ccl_hash_get (vdata);
	  if (ydata->ret < m)
	    m = ydata->ret;
	}
      else
	{
	  ydata = ccl_hash_get (vdata);
	  if (ydata->in_stack && ydata->pref < m)
	    m = ydata->pref;
	}
     }
  ccl_iterator_delete (ei);

  data->ret = m;

  if (data->ret == data->pref)
    {
      ccl_vertex *y;
      struct scc_data *ydata = NULL;
      *p_k = *p_k + 1;
      do 
	{
	  y = ccl_list_take_first (stack);
	  ccl_hash_find (vdata, y);
	  ydata = ccl_hash_get (vdata);
	  ydata->in_stack = 0;
	  ydata->comp = *p_k;
	  attach (G, ccl_vertex_get_data (y), *p_k, cbdata);
	}
      while (y != x);
    }
}

			/* --------------- */

int
ccl_graph_compute_scc (ccl_graph *G, 
		       void (*attach)(ccl_graph *G, void *data, int comp, 
				      void *cbdata),
		       void *cbdata)
{
  int n = 0;
  int nb_comps = 0;
  ccl_hash *data = ccl_hash_create (NULL, NULL, NULL, ccl_free);
  ccl_vertex_iterator *vi = ccl_graph_get_vertices (G);
  ccl_list *stack = ccl_list_create ();

  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *v = ccl_iterator_next_element (vi);
      if (!ccl_hash_find (data, v))
	s_scc (G, data, stack, v, &n, &nb_comps, attach, cbdata);
    }
  ccl_iterator_delete (vi);

  ccl_assert (ccl_list_is_empty (stack));
  ccl_list_delete (stack);  
  ccl_hash_delete (data);

  return nb_comps;
}

			/* --------------- */

static void
s_vertex_delete (ccl_vertex *v)
{
  ccl_pre (v != NULL);
  ccl_hash_delete (v->succ);
  ccl_hash_delete (v->pred);
  ccl_delete (v);
}

			/* --------------- */

static void
s_edge_delete (ccl_edge *e)
{
  ccl_pre (e != NULL);

  ccl_delete (e);
}

			/* --------------- */

static ccl_edge **
s_look_for_edge (ccl_edge **list, ccl_edge *e, int ilist)
{
  for (; *list && *list != e; list = &((*list)->next[ilist]))
    CCL_NOP ();

  return list;
}

			/* --------------- */

static int
s_edge_list_size (ccl_edge *e, int ilist, int *p_nb_loops)
{

  int result = 0;

  if (p_nb_loops)
    {
      int loops = 0;
      for (; e != NULL; e = e->next[ilist])
	{
	  if (e->src == e->tgt)
	    loops++;
	  result++;
	}      
      *p_nb_loops = loops;
    }
  else
    {
      for (; e != NULL; e = e->next[ilist])
	result++;
    }

  return result;
}

			/* --------------- */

static void
s_map (void *container, get_iterator_func *get_i, map_proc *map, void *data)
{
  ccl_pointer_iterator *i = get_i (container);

  while (ccl_iterator_has_more_elements (i))
    {
      void *obj = ccl_iterator_next_element (i);
      map (obj, data);      
    }
  ccl_iterator_delete (i);
}

			/* --------------- */

static int
s_edge_iterator_has_more_elements (const ccl_edge_iterator *i)
{
  const struct edge_iterator *ei = (const struct edge_iterator *) i;

  return ei->current != NULL;
}

			/* --------------- */

static ccl_edge *
s_edge_iterator_next_element (ccl_edge_iterator *i)
{
  ccl_edge *result;
  ccl_edge *next;
  struct edge_iterator *ei = (struct edge_iterator *) i;

  result = ei->current;
  next = result->next[ei->incidence_list];

  if (next == NULL && ei->v != NULL)
    {
      ei->incidence_list++;
      next = ei->v->edges[ei->incidence_list];
      ei->v = NULL;
    }
  ei->current = next;

  return result;
}

			/* --------------- */

static void 
s_edge_iterator_delete (ccl_edge_iterator *i)
{
  ccl_delete (i);
}

			/* --------------- */

static ccl_edge_iterator *
s_crt_edge_iterator (int incidence_list, ccl_vertex *v, ccl_edge *current)
{
  struct edge_iterator *result = ccl_new (struct edge_iterator);

  result->super.has_more_elements = s_edge_iterator_has_more_elements; 
  result->super.next_element = s_edge_iterator_next_element; 
  result->super.delete_iterator = s_edge_iterator_delete; 
  result->incidence_list = incidence_list;
  result->v = v;
  result->current = current;

  return (ccl_edge_iterator *) result;
}

			/* --------------- */


static void
s_cleanup_vertex_flag (ccl_vertex *v, void *data)
{
  v->flags = 0;
}

			/* --------------- */

static void
s_cleanup_flags (ccl_graph *G)
{
  ccl_graph_map_vertices (G, s_cleanup_vertex_flag, NULL);
}

			/* --------------- */

static int
s_vertex_filter_iterator_has_more_elements (const ccl_vertex_iterator *i)
{
  const struct vertex_filter_iterator *vfi = 
    (const struct vertex_filter_iterator *) i;

  return (vfi->next_element != NULL);
}

			/* --------------- */

static ccl_vertex *
s_vertex_filter_iterator_next_element (ccl_vertex_iterator *i)
{
  struct vertex_filter_iterator *vfi = (struct vertex_filter_iterator *) i;
  ccl_vertex *result = vfi->next_element;

  vfi->next_element = NULL;
  while (ccl_iterator_has_more_elements (vfi->vi))
    {
      ccl_vertex *v = ccl_iterator_next_element (vfi->vi);
      if (vfi->accept (v, vfi->data))
	{
	  vfi->next_element = v;
	  break;
	}
    }

  return result;
}

			/* --------------- */

static void 
s_vertex_filter_iterator_delete_iterator (ccl_vertex_iterator *i)
{
  struct vertex_filter_iterator *vfi = (struct vertex_filter_iterator *) i;

  ccl_iterator_delete (vfi->vi);
  ccl_delete (vfi);
}

			/* --------------- */

static ccl_vertex_iterator *
s_vertex_filter_iterator_create (ccl_vertex_iterator *vi,
				 int (*accept) (ccl_vertex *v, void *data),
				 void *data)
{
  struct vertex_filter_iterator *vfi = ccl_new (struct vertex_filter_iterator);
  
  vfi->super.has_more_elements = s_vertex_filter_iterator_has_more_elements;
  vfi->super.next_element = s_vertex_filter_iterator_next_element;  
  vfi->super.delete_iterator = s_vertex_filter_iterator_delete_iterator;
  
  vfi->vi = vi;
  vfi->accept = accept;
  vfi->data = data;
  vfi->next_element = NULL;
  s_vertex_filter_iterator_next_element ((ccl_vertex_iterator *) vfi);

  return (ccl_vertex_iterator *) vfi;
}

			/* --------------- */

static void 
s_topological_order_rec (ccl_vertex *v, ccl_list *result)
{
  ccl_edge *e;

  if (v->flags != 0)
    return;
    
  v->flags = 1;
  for (e = v->edges[EDGE_OUT]; e; e = e->next[EDGE_OUT])
    s_topological_order_rec (e->tgt, result);

  ccl_list_put_first (result, v);
}

			/* --------------- */

ccl_list *
ccl_graph_topological_order (ccl_graph *G)
{
  ccl_list *result = ccl_list_create ();
  ccl_vertex_iterator *roots = ccl_graph_get_roots (G);

  s_cleanup_flags (G);

  while (ccl_iterator_has_more_elements (roots))
    {
      ccl_vertex *r = ccl_iterator_next_element (roots);
      s_topological_order_rec (r, result);
    }
  ccl_iterator_delete (roots);

  return result;
}

			/* --------------- */

struct search_vertex
{
  int found;
  ccl_vertex *v;
};

static int 
s_search_vertex (ccl_vertex *v, void *data)
{
  struct search_vertex *sv = data;

  if (v == sv->v)
    sv->found = 1;
  return sv->found;
}

int 
ccl_graph_is_reachable_from (ccl_graph *G, ccl_vertex *v, ccl_vertex *w)
{
  struct search_vertex sv = { 0, w };
  ccl_graph_bfs (G, v, 0, s_search_vertex, &sv);
  return sv.found;
}

struct scc_graph_data
{
  ccl_graph *result;
  ccl_hash *vertex2sccvertex;
};

static void
s_attach_to_scc (ccl_graph *G, void *data, int comp, void *cbdata)
{
  
  struct scc_graph_data *sgd = cbdata;
  ccl_vertex *sccvertex =
    ccl_graph_find_or_add_vertex (sgd->result, (void *) (intptr_t) comp);
  ccl_vertex *v = ccl_graph_get_vertex (G, data);
  
  ccl_assert (v != NULL);
  ccl_assert (!ccl_hash_find (sgd->vertex2sccvertex, v));
  ccl_hash_find (sgd->vertex2sccvertex, v);
  ccl_hash_insert (sgd->vertex2sccvertex, sccvertex);
}

ccl_graph * 
ccl_graph_compute_scc_graph (ccl_graph *G, ccl_hash **p_v2comp)
{
  struct scc_graph_data sgd;
  ccl_hash_entry_iterator *ei;

  sgd.result = ccl_graph_create (&DUMMY_VERTEX_METHODS, &DUMMY_EDGE_METHODS);
  sgd.vertex2sccvertex = ccl_hash_create (NULL, NULL, NULL, NULL);
  ccl_graph_compute_scc (G, s_attach_to_scc, &sgd);
  
  ccl_assert (ccl_hash_get_size (sgd.vertex2sccvertex) ==
	      ccl_graph_get_number_of_vertices (G));

  ei = ccl_hash_get_entries (sgd.vertex2sccvertex);
  while (ccl_iterator_has_more_elements (ei))
    {
      ccl_hash_entry e = ccl_iterator_next_element (ei);
      ccl_vertex_iterator *si = ccl_vertex_get_successors (e.key);
      while (ccl_iterator_has_more_elements (si))
	{
	  ccl_vertex *s = ccl_iterator_next_element (si);
	  ccl_vertex *sccs = ccl_hash_get_with_key (sgd.vertex2sccvertex, s);

	  if (sccs != e.object && ! ccl_vertex_has_successor (e.object, sccs))
	    ccl_graph_add_edge (sgd.result, e.object, sccs, NULL);
	}
      ccl_iterator_delete (si);
    }
  ccl_iterator_delete (ei);
  
  if (p_v2comp != NULL)
    *p_v2comp = sgd.vertex2sccvertex;
  else
    ccl_hash_delete (sgd.vertex2sccvertex);
  
  return sgd.result;
}

struct module_info
{
  int start;
  int end;
  int min;
  int max;
  int last;
};

static void
s_compute_graph_dates (ccl_vertex *v, ccl_hash *dates, int *counter)
{
  struct module_info *md;
  
  if (ccl_hash_find (dates, v))
    {      
      md = ccl_hash_get (dates);
      ccl_assert (md->end >= 0);
      md->last = *counter;
      return; 
    }

  md = ccl_new (struct module_info);
  ccl_hash_insert (dates, md);
  
  md->start = *counter;
  md->min = md->max = -1;
  if (ccl_vertex_get_out_degree (v) == 0)
    md->end = md->last = *counter;
  else
    {
      ccl_vertex_iterator *vi = ccl_vertex_get_successors (v);
      while (ccl_iterator_has_more_elements (vi))
	{
	  ccl_vertex *s = ccl_iterator_next_element (vi);
	  (*counter)++;
	  s_compute_graph_dates (s, dates, counter);
	}
      ccl_iterator_delete (vi);
      (*counter)++;
      md->end = md->last = *counter;
    }
}

static struct module_info *
s_compute_graph_modules (ccl_vertex *v, ccl_hash *dates, ccl_list *result,
			 int with_leaves)
{
  struct module_info *md;
  ccl_vertex_iterator *vi;
  int is_leave;
  
  ccl_pre (ccl_hash_find (dates, v));
  ccl_hash_find (dates, v);

  md = ccl_hash_get (dates);

  if (md->min >= 0)
    return md;
  
  ccl_assert (md->last >= md->end);
  md->min = md->start;
  md->max = md->last;

  vi = ccl_vertex_get_successors (v);
  is_leave = ! ccl_iterator_has_more_elements (vi);
  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *s = ccl_iterator_next_element (vi);
      struct module_info *mdc = s_compute_graph_modules (s, dates, result,
							 with_leaves);
      ccl_assert (mdc != NULL);

      if (mdc->min < md->min)
	md->min = mdc->min;
      if (mdc->max > md->max)
	md->max = mdc->max;
    }
  ccl_iterator_delete (vi);

  if (md->start <= md->min && md->max <= md->end)
    {
      if (!is_leave || with_leaves)
	ccl_list_add (result, v);
    }

  return md;
}

ccl_list *
ccl_graph_compute_modules (ccl_graph *G, int with_leaves)
{
  int counter = 0;
  ccl_list *result = ccl_list_create ();
  ccl_hash *dates = ccl_hash_create (NULL, NULL, NULL, ccl_free);
  ccl_vertex_iterator *vi = ccl_graph_get_roots (G);

  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *r = ccl_iterator_next_element (vi);
      s_compute_graph_dates (r, dates, &counter);
    }
  ccl_iterator_delete (vi);

  vi = ccl_graph_get_roots (G);

  while (ccl_iterator_has_more_elements (vi))
    {
      ccl_vertex *r = ccl_iterator_next_element (vi);
      s_compute_graph_modules (r, dates, result, with_leaves);
    }
  ccl_iterator_delete (vi);
  ccl_hash_delete (dates);

  return result;
}

ccl_tree *
ccl_graph_compute_lsa_tree (ccl_graph *G, ccl_hash **p_v2t)
{
  ccl_tree *root = ccl_tree_create (NULL, NULL);
  ccl_list *L = ccl_graph_topological_order (G);
  ccl_hash *v2t = ccl_hash_create (NULL, NULL, NULL,
				   (ccl_delete_proc *) ccl_tree_del_reference);
  
  while (! ccl_list_is_empty (L))
    {
      ccl_vertex *v = ccl_list_take_first (L);
      ccl_tree *vt = ccl_tree_create (v, NULL);
      ccl_hash_find (v2t, v);
      ccl_hash_insert (v2t, vt);
      
      if (ccl_vertex_get_in_degree (v) == 0)
	ccl_tree_add_child (root, vt);
      else
	{
	  ccl_vertex_iterator *pi = ccl_vertex_get_predecessors (v);
	  ccl_vertex *lsca = ccl_iterator_next_element (pi);
	  ccl_tree *lscat = ccl_hash_get_with_key (v2t, lsca);
	  
	  while (ccl_iterator_has_more_elements (pi))
	    {
	      ccl_vertex *p = ccl_iterator_next_element (pi);
	      if (p != lsca)
		{
		  ccl_tree *pt = ccl_hash_get_with_key (v2t, p);
		  lscat = ccl_tree_least_common_ancestor (root, lscat, pt);
		  lsca = lscat->object;
		}
	    }
	  ccl_iterator_delete (pi);
	  ccl_tree_add_child (lscat, vt);
	}
    }
  ccl_list_delete (L);
  ccl_assert (ccl_hash_get_size (v2t) == ccl_graph_get_number_of_vertices (G));
  if (p_v2t != NULL)
    *p_v2t = v2t;
  else
    ccl_hash_delete (v2t);
  
  return root;
}
