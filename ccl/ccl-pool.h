/*
 * ccl-pool.h -- A by-packet allocator
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-pool.h
 * \brief A by-packet allocator
 */
#ifndef __CCL_POOL_H__
# define __CCL_POOL_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-log.h>

BEGIN_C_DECLS

/*!
 * \brief Abstract type of a Pool
 */
typedef struct ccl_pool_st ccl_pool;

/*!
 * \brief Creates a pool named by \a poolname that allocates packets of \a 
 * nb_elements objects each one having a size of \a object_size bytes.
 * 
 * \param poolname a string displayed with statistics about pools
 * \param object_size the size (in bytes) of objects stored in the pool
 * \param nb_elements the number of objects allocated by packet
 *
 * \pre object_size >= sizeof (void *)
 * \pre nb_elements > 0
 * \return a pool handling objects of size \a object_size
 */
extern ccl_pool *
ccl_pool_create (const char *poolname, size_t object_size, int nb_elements);

/*!
 * \brief Deletion of \a pool
 * 
 * The function releases all resources allocated to this \a pool. All packets
 * of objects are also freed thus references to objects are no longer valid
 * after this function.
 *
 * \param pool the pool to delete
 * \pre pool != NULL 
 */
extern void
ccl_pool_delete (ccl_pool *pool);

/*!
 * \brief Allocates a new object.
 *
 * The function looks for a free object in one of the already allocated packets.
 * If such object exists then its address is returned. Else, a new packet is
 * allocated and the address of a fresh object is returned.
 *
 * The area returned by the function is not filled with zero.
 *
 * \param pool the pool
 * \pre pool != NULL
 * \return a pointer to a new object
 */
extern void *
ccl_pool_alloc (ccl_pool *pool);

/*!
 * \brief Releases the block pointed by \a ptr
 * 
 * The function puts the block pointed by \a ptr into the list of free objects
 * of the pool \a pool. 
 *
 * \param pool the pool
 * \param ptr the address of an object to collect
 * \pre pool != NULL
 * \pre ptr != NULL 
 */
extern void
ccl_pool_release (ccl_pool *pool, void *ptr);

/*!
 * \brief Tries to collect completely free packets.
 * 
 * This function looks for packets whose all objects are free. The memory 
 * allocated to such packet is removed.
 *
 * \param pool the pool
 * \pre ptr != NULL 
 */
extern void
ccl_pool_collect (ccl_pool *pool);

/*!
 * \brief Display statictics about \a pool 
 * \param lt the stream on which statistics are displayed
 * \param pool the pool
 * \pre pool != NULL
 */
extern void
ccl_pool_display_info (ccl_log_type lt, ccl_pool *pool);

/*!
 * \brief Collect packets in all pools
 *
 * This function simply applies \ref ccl_pool_collect to all existing pools.
 */
extern void
ccl_pools_collect (void);

/*!
 * \brief Display statistics about all pools
 *
 * This function simply applies \ref ccl_pool_display_info to all existing 
 * pools.
 *
 * \param lt the stream on which statistics are displayed
 */
extern void
ccl_pools_display_info (ccl_log_type lt);

END_C_DECLS

#endif /* ! __CCL_POOL_H__ */
