/*
 * ccl-array.c -- A kind of template for dynamic arrays
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-array.h"

void
ccl_array_resize (int *poldsize, int elsize, void **data, int newsize, 
		  int trim)
{
  char *newdata;
  int oldsize = *poldsize;

  if (newsize < oldsize)
    {
      if (trim)
	{
	  oldsize = newsize;
	}
      else
	{
	  return;
	}
    }

  newdata = ccl_calloc (elsize, newsize);
  if (oldsize)
    ccl_memcpy (newdata, *data, oldsize * elsize);
  ccl_free (*data);
  *data = newdata;
  *poldsize = newsize;
}
