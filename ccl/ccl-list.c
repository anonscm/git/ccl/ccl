/*
 * ccl-list.c -- A generic list encoded with a simple chaining.
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-list.h"

struct list_iterator {
  ccl_pointer_iterator super;
  ccl_list *list;
  ccl_pair *p;
  ccl_duplicate_func *dup;
  ccl_delete_proc *del;
};

# define s_alloc_list()  ccl_new (ccl_list)
# define s_free_list(_l) ccl_delete ((_l))
# define s_alloc_pair()  ccl_new (ccl_pair)
# define s_free_pair(_p) ccl_delete ((_p))

			/* --------------- */

static int
s_default_cmp (const void *p1, const void *p2);

static int
s_default_cmp_with_data (const void *p1, const void *p2, const void *data);

static ccl_pair *
s_sort_rec (ccl_pair *p, ccl_compare_with_data_func *cmp, const void *data, 
	    ccl_pair *queue);

static ccl_pair **
s_get_last (ccl_list *l);

			/* --------------- */

ccl_list *
ccl_list_create (void)
{
  ccl_list *result = s_alloc_list ();

  result->size = 0;
  result->first = NULL;
  result->last = &result->first;

  return result;
}

			/* --------------- */

void
ccl_list_delete (ccl_list *l)
{
  ccl_list_clear_and_delete (l, NULL);
}

			/* --------------- */

void
ccl_list_clear (ccl_list *l, ccl_delete_proc *del)
{
  ccl_pair *p;
  ccl_pair *next;

  ccl_pre (l != NULL);

  if (del != NULL)
    {
      for (p = FIRST (l); p; p = next)
	{
	  next = CDR (p);
	  if (del != NULL)
	    del (CAR (p));
	  s_free_pair (p);
	}
    }
  else
    {
      for (p = FIRST (l); p; p = next)
	{
	  next = CDR (p);
	  s_free_pair (p);
	}
    }
  l->first = NULL;
  l->size = 0;
  l->last = &l->first;
}

			/* --------------- */

void
ccl_list_clear_and_delete (ccl_list *l, ccl_delete_proc *del)
{
  ccl_list_clear (l, del);
  s_free_list (l);
}

			/* --------------- */

void
ccl_list_add (ccl_list *l, void *object)
{
  ccl_pair *p = s_alloc_pair ();

  ccl_pre (l != NULL);

  CAR (p) = object;
  CDR (p) = NULL;
  *(l->last) = p;
  l->last = &(CDR (p));
  l->size++;
}

			/* --------------- */

int
ccl_list_has (const ccl_list *l, void *object)
{
  ccl_pair *p;

  ccl_pre (l != NULL);

  for (p = FIRST (l); p; p = CDR (p))
    {
      if (CAR (p) == object)
	return 1;
    }

  return 0;
}

			/* --------------- */

void
ccl_list_put_first (ccl_list *l, void *obj)
{
  ccl_pre (l != NULL);
  
  if (ccl_list_is_empty (l))
    ccl_list_add (l, obj);
  else
    {
      ccl_pair *first = s_alloc_pair ();

      first->car = obj;
      first->cdr = l->first;
      l->first = first;
      l->size++;
    }
}

			/* --------------- */

void *
ccl_list_take_first (ccl_list *l)
{
  ccl_pair *first;
  void *result;

  ccl_pre (!ccl_list_is_empty (l));

  first = FIRST (l);
  result = CAR (first);

  if (l->last == (&CDR (first)))
    l->last = &(FIRST (l));
  FIRST (l) = CDR (FIRST (l));
  l->size--;
  s_free_pair (first);

  return result;
}

			/* --------------- */

static int
s_cmp_with_data (const void *o1, const void *o2, const void *data)
{
  return ((ccl_compare_func *) data) (o1, o2);
}

			/* --------------- */

void
ccl_list_sort (ccl_list *l, ccl_compare_func *cmp)
{
  if (cmp == CCL_DEFAULT_COMPARE_WITH_DATA_FUNC)
    cmp = s_default_cmp;

  return ccl_list_sort_with_data (l, s_cmp_with_data, cmp);
}

			/* --------------- */


void
ccl_list_sort_with_data (ccl_list *l, ccl_compare_with_data_func *cmp, 
			 const void *data)
{
  ccl_pre (l != NULL);
  
  if (cmp == CCL_DEFAULT_COMPARE_WITH_DATA_FUNC)
    cmp = s_default_cmp_with_data;

  l->first = s_sort_rec (l->first, cmp, data, NULL);
  l->last = s_get_last (l);
}

			/* --------------- */

int
ccl_list_insert (ccl_list *l, void *obj, ccl_compare_func *cmp)
{
  int index;
  ccl_pair *p;
  ccl_pair **pp;

  if (l->size == 0)
    {
      ccl_list_add (l, obj);
      return 0;
    }

  l->size++;
  if (cmp == CCL_DEFAULT_COMPARE_FUNC)
    cmp = s_default_cmp;

  for (index = 0, pp = &(l->first); *pp; pp = &((*pp)->cdr), index++)
    {
      if (cmp (obj, (*pp)->car) < 0)
	break;
    }

  p = s_alloc_pair ();
  p->car = obj;
  p->cdr = *pp;
  *pp = p;

  for (pp = &(p->cdr); *pp; pp = &((*pp)->cdr))
    CCL_NOP ();
  l->last = pp;

  return index;
}

			/* --------------- */

void
ccl_list_insert_before (ccl_list *l, void *obj, void *inserted)
{
  ccl_pair *p;
  ccl_pair **pp;

  ccl_pre (l != NULL);

  for (pp = &(l->first); *pp; pp = &((*pp)->cdr))
    {
      if ((*pp)->car == obj)
	break;
    }

  p = s_alloc_pair ();
  p->car = inserted;
  p->cdr = *pp;

  if (*pp == NULL)
    l->last = &(p->cdr);

  l->size++;
  *pp = p;
}

			/* --------------- */

void
ccl_list_insert_after (ccl_list *l, void *obj, void *inserted)
{
  ccl_pair *p;

  ccl_pre (l != NULL);

  for (p = l->first; p; p = CDR (p))
    {
      if (p->car == obj)
	break;
    }

  if (p == NULL)
    ccl_list_add (l, inserted);
  else
    {
      ccl_pair *np = s_alloc_pair ();
      np->car = inserted;
      np->cdr = p->cdr;
      p->cdr = np;
      l->size++;
      if (np->cdr == NULL)
	l->last = &(np->cdr);
    }
}

			/* --------------- */

int
ccl_list_equals (const ccl_list *l1, const ccl_list *l2)
{
  ccl_pair *p1;
  ccl_pair *p2;

  if (ccl_list_get_size (l1) != ccl_list_get_size (l2))
    return 0;

  p1 = FIRST (l1);
  p2 = FIRST (l2);
  while (p1 != NULL && p2 != NULL)
    {
      if (CAR (p1) != CAR (p2))
	return 0;

      p1 = CDR (p1);
      p2 = CDR (p2);
    }

  return (p1 == NULL && p2 == NULL);
}

			/* --------------- */

ccl_list *
ccl_list_dup (const ccl_list *l)
{
  ccl_pair *p;
  ccl_list *result = ccl_list_create ();

  ccl_pre (l != NULL);

  for (p = FIRST (l); p; p = CDR (p))
    ccl_list_add (result, CAR (p));

  return result;
}

			/* --------------- */

ccl_list *
ccl_list_deep_dup (const ccl_list *l, ccl_duplicate_func *dup)
{
  ccl_pair *p;
  ccl_list *result;

  ccl_pre (l != NULL);

  if (dup == CCL_DEFAULT_DUPLICATE_FUNC)
    result = ccl_list_dup (l);
  else
    {
      result = ccl_list_create ();
      for (p = FIRST (l); p; p = CDR (p))
	ccl_list_add (result, dup (CAR (p)));
    }

  return result;
}

			/* --------------- */

void *
ccl_list_get_at (ccl_list *l, int index)
{
  ccl_pair *p;

  ccl_pre (0 <= index && index < ccl_list_get_size(l));

  p = FIRST (l);
  while (index--)
    p = CDR (p);
  ccl_post (p != NULL);

  return CAR (p);
}

			/* --------------- */

void
ccl_list_remove (ccl_list *l, void *ptr)
{
  ccl_pair **pp;

  ccl_pre (l != NULL);
  
  for (pp = &l->first; *pp; pp = &((*pp)->cdr))
    {
      if ((*pp)->car == ptr)
	break;
    }

  if (*pp != NULL)
    {
      ccl_pair *p = *pp;

      if (l->last == &(p->cdr))
	l->last = pp;
      *pp = p->cdr;
      s_free_pair (p);
      l->size--;
    }
}

			/* --------------- */

void
ccl_list_sub (ccl_list *l1, const ccl_list *l2)
{
  ccl_pair *p;

  ccl_pre (l1 != NULL);
  ccl_pre (l2 != NULL);

  for (p = FIRST (l2); p != NULL; p = CDR (p))
    ccl_list_remove (l1, CAR (p));
}

			/* --------------- */

void
ccl_list_append (ccl_list *l1, const ccl_list *l2)
{
  ccl_list_deep_append (l1, l2, NULL);
}

			/* --------------- */

void
ccl_list_deep_append (ccl_list *l1, const ccl_list *l2, 
		      ccl_duplicate_func *dup)
{
  ccl_pair *p;

  ccl_pre (l1 != NULL);
  ccl_pre (l2 != NULL);

  for (p = FIRST (l2); p != NULL; p = CDR (p))
    {
      void *ptr = dup ? dup (CAR (p)) : CAR (p);
      ccl_list_add (l1, ptr);
    }
}

			/* --------------- */

int
ccl_list_get_index (const ccl_list *l, void *ptr, ccl_compare_func *cmp)
{
  ccl_pair *p ;
  int result = 0;

  ccl_pre (l != NULL);

  p = FIRST (l);
  if (cmp == CCL_DEFAULT_COMPARE_FUNC)
    {
      for (; p != NULL; p = CDR (p), result++)
	{
	  if (CAR (p) == ptr)
	    return result;
	}
    }
  else
    {
      for (; p != NULL; p = CDR (p), result++)
	{
	  if (cmp (CAR (p), ptr) == 0)
	    return result;
	}
    }

  return -1;
}

			/* --------------- */

int
ccl_list_compare (const ccl_list *l1, const ccl_list *l2, 
		  ccl_compare_func *cmp)
{
  int result = 0;
  ccl_pair *p1;
  ccl_pair *p2;

  ccl_pre (l1 != NULL);
  ccl_pre (l2 != NULL);

  p1 = FIRST (l1);
  p2 = FIRST (l2);
  if (cmp == CCL_DEFAULT_COMPARE_FUNC)
    cmp = s_default_cmp;

  while (p1 != NULL && p2 != NULL)
    {
      result = cmp (CAR (p1), CAR (p2));
      if (result != 0)
	break;

      p1 = CDR (p1);
      p2 = CDR (p2);
    }

  if (p1 == NULL && p2 == NULL)
    result = 0;
  else if (p1 == NULL)
    result = -1;
  else if (p2 == NULL)
    result = 1;

  return result;
}

			/* --------------- */

unsigned int
ccl_list_hash (const ccl_list *l)
{
  return ccl_list_hash_all (l, CCL_DEFAULT_HASH_FUNC);
}

			/* --------------- */

unsigned int
ccl_list_hash_all (const ccl_list *l, ccl_hash_func *h)
{
  ccl_pair *p;
  unsigned int result = 0;

  ccl_pre (l != NULL);

  p = FIRST (l);

  if (h == CCL_DEFAULT_HASH_FUNC)
    {
      for (; p; p = CDR (p))
	{
	  if (CAR (p) != NULL)
	    result = 9 * result + ((uintptr_t) (CAR (p)));
	}
    }
  else
    {
      for (; p; p = CDR (p))
	result = 9 * result + h (CAR (p));
    }

  return result;
}

			/* --------------- */

void **
ccl_list_to_array (const ccl_list *l, int *p_size)
{
  ccl_pair *p;
  void **po;
  int sz = ccl_list_get_size (l);
  void **result = ccl_new_array (void *, sz);

  ccl_pre (l != NULL);

  for (p = FIRST (l), po = result; p; p = CDR (p), po++)
    *po = CAR (p);

  if (p_size != NULL)
    *p_size = sz;

  return result;
}

			/* --------------- */

static int
s_list_iterator_has_more_elements (const ccl_pointer_iterator *i)
{
  return ((struct list_iterator *)i)->p != NULL;
}

			/* --------------- */

static void *
s_list_iterator_next_element (ccl_pointer_iterator *i)
{
  struct list_iterator *li = (struct list_iterator *) i;
  void *result = CAR (li->p);

  li->p = CDR (li->p);
  if (li->dup != NULL)
    result = li->dup (result);

  return result;
}

			/* --------------- */

static void 
s_list_iterator_delete_iterator (ccl_pointer_iterator *i)
{
  struct list_iterator *li = (struct list_iterator *) i;
  
  ccl_list_clear_and_delete (li->list, li->del);
  ccl_delete (i);
}

			/* --------------- */

ccl_pointer_iterator *
ccl_list_get_iterator (ccl_list *l, ccl_duplicate_func *dup,
		       ccl_delete_proc *del)
{
  struct list_iterator *result = ccl_new (struct list_iterator);

  result->super.has_more_elements = s_list_iterator_has_more_elements; 
  result->super.next_element = s_list_iterator_next_element; 
  result->super.delete_iterator = s_list_iterator_delete_iterator; 
  result->list = ccl_list_deep_dup (l, dup);
  result->p = FIRST (l);

  return (ccl_pointer_iterator *) result;
}

			/* --------------- */

ccl_list *
ccl_list_create_from_iterator (ccl_pointer_iterator *i)
{
  ccl_list *result = ccl_list_create ();

  while (ccl_iterator_has_more_elements (i))
    {
      void *ptr = ccl_iterator_next_element (i);
      ccl_list_add (result, ptr);
    }
  return result;
}

			/* --------------- */

static int
s_default_cmp (const void *p1, const void *p2)
{
  if (p1 < p2)
    return -1;

  if (p1 == p2)
    return 0;

  return 1;
}

			/* --------------- */

static int
s_default_cmp_with_data (const void *p1, const void *p2, const void *dummy)
{
  return s_default_cmp (p1, p2);
}

			/* --------------- */

static ccl_pair *
s_sort_rec (ccl_pair *p, ccl_compare_with_data_func *cmp, const void *data, 
	    ccl_pair *queue)
{
  int c;
  ccl_pair *cdr;
  ccl_pair *pivot = p;
  ccl_pair *inf = NULL;
  ccl_pair *sup = NULL;

  if (p == NULL)
    return queue;

  for (p = CDR (p); p; p = cdr)
    {
      cdr = CDR (p);

      c = cmp (CAR (p), CAR (pivot), data);
      if (c < 0)
	{
	  CDR (p) = inf;
	  inf = p;
	}
      else
	{
	  CDR (p) = sup;
	  sup = p;
	}
    }

  CDR (pivot) = s_sort_rec (sup, cmp, data, queue);

  return s_sort_rec (inf, cmp, data, pivot);
}

			/* --------------- */

static ccl_pair **
s_get_last (ccl_list *l)
{
  ccl_pair **last;

  for (last = &l->first; *last; last = &(CDR (*last)))
    CCL_NOP ();

  return last;
}

			/* --------------- */

