/*
 * ccl-stack.h -- A generic stack
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-stack.h
 * \brief A generic stack implementation
 */
#ifndef __CCL_STACK_H__
# define __CCL_STACK_H__

# include <ccl/ccl-common.h>

BEGIN_C_DECLS

/*!
 * \brief Abstract type of a stack
 */
typedef struct ccl_stack_st ccl_stack;

/*!
 * \brief Creates a new empty stack
 * \return a new empty stack
 */
extern ccl_stack *
ccl_stack_create (void);

/*! 
 * \brief Deletion of the stack \a s
 * 
 * The resources allocated to this stack are released. Objects stacked on
 * \a s are not deleted.
 * 
 * \param s the stack to delete
 * \pre s != NULL
 */
extern void
ccl_stack_delete (ccl_stack *s);

/*!
 * \brief Returns the number of elements on the stack \a s
 * \param s the stack
 * \pre s != NULL
 * \return the number of elements on the stack \a s
 */
extern int
ccl_stack_get_size (const ccl_stack *s);

/*! 
 * \brief Checks is the stack \a s is empty
 * \param _s the stack
 * \pre _s != NULL
 * \return a non null value if \a s is empty 
 */
# define ccl_stack_is_empty(_s) (ccl_stack_get_size (_s) == 0)

/*!
 * \brief Returns the object at the top of the stack
 * 
 * If \a s is not an empty stack, the function returns the last stored object
 * i.e. the one on the top of the stack.
 *
 * \param s the stack
 * \pre ! ccl_stack_is_empty (s)
 * \return the object contained in the cell on the top of the stack \a s
 */
extern void *
ccl_stack_get_top (ccl_stack *s);

/*!
 * \brief Put the object \a obj on the stack \a s
 * 
 * The function put the object \a obj on the top of the stack. 
 *
 * \param s the stack
 * \param obj the object put on the stack
 * \pre s != NULL
 * \post ccl_stack_get_top (s) == obj
 */
extern void
ccl_stack_push (ccl_stack *s, void *obj);

/*
 * \brief Removes and returns the object at the top of the stack \a s
 * 
 * The function removes the cell at the top of the non-empty stack \a s and
 * returns the object stored into this cell.
 *
 * \param s the stack
 * \pre ! ccl_stack_is_empty(s)
 * \return the object contained in the cell on the top of the stack \a s
 */
extern void *
ccl_stack_pop (ccl_stack *s);

END_C_DECLS

#endif /* ! __CCL_STACK_H__ */
