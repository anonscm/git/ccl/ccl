/*
 * ccl-string-p.h -- Internal routines of the ccl-string module
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_STRING_P_H__
# define __CCL_STRING_P_H__

# include "ccl-string.h"

extern void
ccl_string_init (void);

extern void
ccl_string_terminate (void);

#endif /* ! __CCL_STRING_P_H__ */
