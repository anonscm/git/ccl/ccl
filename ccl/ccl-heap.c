/*
 * ccl-heap.c -- a generic binary heap
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-assert.h"
#include "ccl-memory.h"
#include "ccl-array.h"
#include "ccl-heap.h"

struct ccl_heap_st
{  
  ccl_compare_func *cmp;
  int nb_elements;
  int init_size;
  CCL_ARRAY (void *) heap;
};

			/* --------------- */

static int
s_default_cmp (const void *p1, const void *p2);

			/* --------------- */

ccl_heap *
ccl_heap_create (int size, ccl_compare_func *cmp)
{
  ccl_heap *result = ccl_new (ccl_heap);

  ccl_pre (size > 0);

  result->cmp = (cmp == NULL) ? s_default_cmp : cmp;
  result->nb_elements = 0;
  result->init_size = size;
  ccl_array_init_with_size (result->heap, size);

  return result;
}

			/* --------------- */

void
ccl_heap_delete (ccl_heap *h)
{
  ccl_heap_clear_and_delete (h, NULL);
}

			/* --------------- */

void
ccl_heap_clear (ccl_heap *h, ccl_delete_proc *del)
{
  ccl_pre (h != NULL);

  if (del != NULL)
    {
      int i;

      for (i = 0; i < h->heap.size; i++)
	del (h->heap.data[i]);
    }

  ccl_array_trim (h->heap, h->init_size);
}


			/* --------------- */

void
ccl_heap_clear_and_delete (ccl_heap *h, ccl_delete_proc *del)
{
  ccl_heap_clear (h, del);
  ccl_array_delete (h->heap);
  ccl_delete (h);
}

			/* --------------- */

int
ccl_heap_get_size (const ccl_heap *h)
{
  ccl_pre (h != NULL);

  return h->nb_elements;
}

			/* --------------- */

void *
ccl_heap_get_first (const ccl_heap *h)
{
  ccl_pre (! ccl_heap_is_empty (h));

  return h->heap.data[0];
}

			/* --------------- */

#define heap_element(h, i) ((h)->heap.data[(i)-1])
#define left_child(i) ((i) << 1)
#define right_child(i) (((i) << 1) + 1)
#define parent(i) ((i) >> 1)


			/* --------------- */

static void
s_check_structure (ccl_heap *h)
{
#ifdef CCL_ENABLE_ASSERTIONS
  int i;

  for (i = 1; i <= h->nb_elements; i++)
    {
      int l = left_child (i);
      int r = right_child (i);

      if (l <= h->nb_elements)
	ccl_assert (h->cmp (heap_element (h, i), heap_element (h,l)) <= 0);
      if (r <= h->nb_elements)
	ccl_assert (h->cmp (heap_element (h, i), heap_element (h,r)) <= 0);
    }
#endif
}

			/* --------------- */

void *
ccl_heap_take_first (ccl_heap *h)
{
  int i;
  void *result = ccl_heap_get_first (h);
  
  heap_element (h, 1) = heap_element (h, h->nb_elements);
  h->nb_elements--;

  i = 1;

  while (i <= h->nb_elements)
    {
      int l = left_child (i);
      int r = right_child (i);
      int minindex;

      if (r > h->nb_elements)
	{
	  if (l > h->nb_elements)
	    break;
	  else
	    minindex = l;
	}
      else if (h->cmp (heap_element (h, l), heap_element (h, r)) <= 0)
	minindex = l;
      else 
	minindex = r;

      if (h->cmp (heap_element (h, i), heap_element (h, minindex)) > 0)
	{
	  void *tmp = heap_element (h, i);
	  heap_element (h, i) = heap_element (h, minindex);
	  heap_element (h, minindex) = tmp;
	  i = minindex;
	}
      else
	break;
    }    

  s_check_structure (h);

  return result;
}

			/* --------------- */

void
ccl_heap_add (ccl_heap *h, void *obj)
{
  int i;

  ccl_pre (h != NULL);  

  h->nb_elements++;
  ccl_array_ensure_size (h->heap, h->nb_elements);
  heap_element (h, h->nb_elements) = obj;

  i = h->nb_elements;
  while (i > 1 && 
	 h->cmp (heap_element (h, parent (i)), heap_element (h, i)) > 0)
    {
      void *tmp = heap_element(h, i);
      heap_element (h, i) = heap_element (h, parent (i));
      heap_element (h, parent (i)) = tmp;
      i = parent (i);
    }

  s_check_structure (h);
}

			/* --------------- */

int
ccl_heap_has (const ccl_heap *h, void *obj)
{
  int i;

  ccl_pre (h != NULL);  

  for (i = 0; i < h->heap.size; i++)
    if (h->cmp (h->heap.data[i], obj) == 0)
      return 1;

  return 0;
}

			/* --------------- */

void
ccl_heap_log (ccl_log_type log, ccl_heap *h, 
	      void (*logproc) (ccl_log_type log, void *obj, void *data),
	      void *data)
{
  int i;

  ccl_log (log, "[");
  for (i = 0; i < h->nb_elements; i++)
    {
      logproc (log, h->heap.data[i], data);
      if (i < h->nb_elements - 1)
	ccl_log (log, " ");
    }
  ccl_log (log, "]\n");
}

			/* --------------- */

static int
s_default_cmp (const void *p1, const void *p2)
{
  if (p1 < p2 ) return -1;
  if (p1 == p2) return 0;
  return 1;
}

