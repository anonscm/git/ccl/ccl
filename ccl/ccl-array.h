/*
 * ccl-array.h -- A kind of template for dynamic arrays
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-array.h
 * \brief A kind of template for dynamic arrays
 */
#ifndef __CCL_ARRAY_H__
# define __CCL_ARRAY_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-memory.h>

BEGIN_C_DECLS

/*! 
 * \brief Macro to define an array of objects of type \a _t_
 */
# define CCL_ARRAY(_t_) \
  struct {		\
    int size;		\
    _t_ *data; \
  }

/*!
 * \brief Initialize the array \a _a_ with no cell.
 */
# define ccl_array_init(_a_)					\
  do {								\
    (_a_).size = 0;						\
    (_a_).data = ccl_calloc (sizeof ((_a_).data[0]), 1);	\
  } while(0)

/*!
 * \brief Initialize the array \a _a_ with \a _sz_ cells
 */
# define ccl_array_init_with_size(_a_,_sz_)			\
  do {								\
    (_a_).size = (_sz_);					\
    (_a_).data = ccl_calloc (sizeof ((_a_).data[0]), (_sz_));	\
  } while(0)

/*!
 * \brief Adds the element \a _el_ in a new cell added at the end of the 
 * array \a _a_
 */
# define ccl_array_add(_a_,_el_)			\
  do {							\
    ccl_array_ensure_size (_a_, (_a_).size + 1);	\
    (_a_).data[(_a_).size-1] = _el_;			\
  } while(0)

/*!
 * \brief Deletes the memory allocated for \a _a_. Objects stored into the 
 * array are not deleted.
 */
# define ccl_array_delete(_a_) \
  do { ccl_free ((_a_).data); } while (0)

/*!
 * \brief Resize the array \a _a_ up to \a _sz_ cells if necessary i.e. if the
 * current number of cells is less than \a _sz_
 */
# define ccl_array_ensure_size(_a_,_sz_) \
  ccl_array_resize(&(_a_).size, sizeof ((_a_).data[0]), \
		   (void **) &((_a_).data),		\
		   _sz_, 0)
/*! 
 * \brief Add a new cell to the array \a _a_
 */
# define ccl_array_ensure_size_plus_one(_a_) \
  ccl_array_ensure_size (_a_, (_a_).size + 1)

/*!
 * \brief Resize the array to the given size.
 */
# define ccl_array_trim(_a_,_sz_) \
  ccl_array_resize (&(_a_).size, sizeof ((_a_).data[0]), \
		    (void **) &((_a_).data), \
		   _sz_, 1)

/*!
 * \brief Resize the array to its actual size.
 */
# define ccl_array_trim_to_size(_a_) ccl_array_trim (_a_, (_a_).size)

extern void 
ccl_array_resize (int *poldsz, int elsz, void **data, int newsz, int trim);

END_C_DECLS

#endif /* ! __CCL_ARRAY_H__ */
