/*
 * ccl-graph.h -- Generic graph 
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#ifndef __CCL_GRAPH_H__
# define __CCL_GRAPH_H__

# include <ccl/ccl-log.h>
# include <ccl/ccl-hash.h>
# include <ccl/ccl-list.h>
# include <ccl/ccl-tree.h>

BEGIN_C_DECLS

typedef struct ccl_graph_st ccl_graph;

typedef struct ccl_vertex_st ccl_vertex;
typedef ccl_hash_methods ccl_vertex_data_methods;
typedef void ccl_vertex_map_proc (ccl_vertex *v, void *data);
typedef int ccl_vertex_search_func (ccl_vertex *v, void *data);
CCL_ITERATOR_TYPEDEF (ccl_vertex_iterator, ccl_vertex *);

typedef struct ccl_edge_st ccl_edge;
typedef ccl_hash_methods ccl_edge_data_methods;
typedef void ccl_edge_map_proc (ccl_edge *v, void *data);
CCL_ITERATOR_TYPEDEF (ccl_edge_iterator, ccl_edge *);

			/* --------------- */

extern ccl_graph *
ccl_graph_create (ccl_vertex_data_methods *vertex_methods,
		  ccl_edge_data_methods *edge_methods);

extern ccl_graph *
ccl_graph_add_reference (ccl_graph *G);

extern void
ccl_graph_del_reference (ccl_graph *G);

extern int
ccl_graph_get_number_of_vertices (ccl_graph *G);

extern void
ccl_graph_map_vertices (ccl_graph *G, ccl_vertex_map_proc *map, void *data);

extern ccl_vertex_iterator *
ccl_graph_get_vertices (ccl_graph *G);

extern int
ccl_graph_get_number_of_edges (ccl_graph *G);

extern void
ccl_graph_map_edges (ccl_graph *G, ccl_edge_map_proc *map, void *data);

extern ccl_edge_iterator *
ccl_graph_get_edges (ccl_graph *G);

extern void
ccl_graph_log_as_dot (ccl_log_type log, ccl_graph *G,
		      void (*graph_attributes) (ccl_log_type log,
						ccl_graph *G,
						void *cbdata),
		      void *graph_cb_data,
		      void (*dot_vertex_data)(ccl_log_type log, 
					      ccl_vertex *v,
					      void *cbdata),
		      void *vertex_cb_data,
		      void (*dot_edge_data)(ccl_log_type log, 
					    ccl_edge *e, 
					    void *cbdata),
		      void *edge_cb_data);

extern ccl_graph *
ccl_graph_dup (ccl_graph *G, 
	       ccl_duplicate_func *dup_vertex_data,
	       ccl_duplicate_func *dup_edge_data);

			/* --------------- */

extern ccl_vertex *
ccl_graph_find_or_add_vertex (ccl_graph *G, void *data);

extern ccl_vertex *
ccl_graph_add_vertex (ccl_graph *G, void *data);

extern void
ccl_graph_remove_vertex (ccl_graph *G, ccl_vertex *v);

extern ccl_vertex *
ccl_graph_get_vertex (const ccl_graph *G, void *data);

extern void *
ccl_vertex_get_data (ccl_vertex *v);

extern void
ccl_vertex_map_edges (ccl_vertex *v, ccl_edge_map_proc *map, void *data);

extern ccl_edge_iterator *
ccl_vertex_get_edges (ccl_vertex *v);

extern int
ccl_vertex_get_degree (ccl_vertex *v);

extern void
ccl_vertex_map_in_edges (ccl_vertex *v, ccl_edge_map_proc *map, void *data);

extern ccl_edge_iterator *
ccl_vertex_get_in_edges (ccl_vertex *v);

extern int
ccl_vertex_get_in_degree (const ccl_vertex *v);

extern void
ccl_vertex_map_out_edges (ccl_vertex *v, ccl_edge_map_proc *map, void *data);

extern ccl_edge_iterator *
ccl_vertex_get_out_edges (ccl_vertex *v);

extern int
ccl_vertex_get_out_degree (const ccl_vertex *v);

extern int
ccl_vertex_has_successor (ccl_vertex *v, ccl_vertex *w);

extern ccl_vertex_iterator *
ccl_vertex_get_successors (ccl_vertex *v);

extern int
ccl_vertex_has_predecessor (ccl_vertex *v, ccl_vertex *w);

extern ccl_vertex_iterator *
ccl_vertex_get_predecessors (ccl_vertex *v);


			/* --------------- */

extern ccl_edge *
ccl_graph_add_edge (ccl_graph *G, ccl_vertex *src, ccl_vertex *dst, void *data);

extern ccl_edge *
ccl_graph_add_data_edge (ccl_graph *G, void *sdata, void *ddata, void *data);

extern void
ccl_graph_remove_edge (ccl_graph *G, ccl_edge *e);

extern void *
ccl_edge_get_data (const ccl_edge *e);

extern ccl_vertex *
ccl_edge_get_src (const ccl_edge *e);

extern ccl_vertex *
ccl_edge_get_tgt (const ccl_edge *e);

			/* --------------- */

extern void
ccl_graph_dfs (ccl_graph *G, ccl_vertex *root, int backward, 
	       ccl_vertex_search_func *map, void *cbdata);

extern void
ccl_graph_bfs (ccl_graph *G, ccl_vertex *root, int backward, 
	       ccl_vertex_search_func *search, void *cbdata);

extern void
ccl_graph_reverse_edges (ccl_graph *G);

extern ccl_vertex_iterator *
ccl_graph_get_roots (ccl_graph *G);

extern ccl_vertex_iterator *
ccl_graph_get_leaves (ccl_graph *G);

extern int
ccl_graph_compute_scc (ccl_graph *G, 
		       void (*attach)(ccl_graph *G, void *data, int comp, 
				      void *cbdata),
		       void *cbdata);

extern ccl_list *
ccl_graph_topological_order (ccl_graph *G);

extern int 
ccl_graph_is_reachable_from (ccl_graph *G, ccl_vertex *from, ccl_vertex *to);

extern ccl_graph * 
ccl_graph_compute_scc_graph (ccl_graph *G, ccl_hash **p_v2comp);

extern ccl_list *
ccl_graph_compute_modules (ccl_graph *G, int with_leaves);

/*!
 * Compute the LSA-tree of the DAG G.
 * Table p_v2t maps vertices of G to nodes of the result.
 * 
 * See: J. Fisher and D.H. Huson. New common ancestor problems in trees and 
 * directed acyclic graphs. In Information Processing Letters 110 (2010).
 */
extern ccl_tree *
ccl_graph_compute_lsa_tree (ccl_graph *G, ccl_hash **p_v2t);

END_C_DECLS

#endif /* ! __CCL_GRAPH_H__ */
