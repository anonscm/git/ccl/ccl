/*
 * ccl-string.h -- String manipulation module
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-string.h
 * \brief Module gathering routines frequently used for the manipulation
 * of string.
 */
#ifndef __CCL_STRING_H__
# define __CCL_STRING_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-memory.h>

BEGIN_C_DECLS

/*!
 * \brief Alias type for strings that are stored in an internal dictionary and
 * then can be used as identifiers and can be compared by pointers.
 */
typedef char *ccl_ustring;

/*!
 * \brief Hashes the string pointed by \a ptr
 *
 * \param ptr the string to hash
 * ccl_pre ptr != NULL
 * \return a hashed value of \a ptr.
 */
extern unsigned int
ccl_string_hash (const void *ptr);

/*!
 * \brief Deletion function for a string
 */
# define ccl_string_delete ccl_free

/*!
 * \brief Equality checking for strings.
 * 
 * This function checks if \a str1 and \a str2 are equal. This function can
 * be used where a pointer to a \ref ccl_equals_func is required.
 *
 * \param str1 first string arg
 * \param str2 second string arg
 * \pre str1 != NULL
 * \pre str2 != NULL
 *
 * \return a non-null value if \a str1 and \a str2 are equal.
 */
extern int
ccl_string_equals (const void *str1, const void *str2);

/*!
 * \brief String comparison
 *
 * This function compares \a str1 and \a str2. This function returns a value
 * less than, equal to or greater than zero if \a str1 is, respectively, 
 * less than, equal to or greater than \a str2. This function can
 * be used where a pointer to a \ref ccl_compare_func is required.
 *
 * \param str1 first string arg
 * \param str2 second string arg
 * \pre str1 != NULL && 
 * \pre str2 != NULL
 *
 * \retval < 0 if \a str1 < \a str2
 * \retval == 0 if \a str1 == \a str2
 * \retval > 0 if \a str1 > \a str2
 */
# define ccl_string_compare ((ccl_compare_func *) strcmp)

/*!
 * \brief String comparison
 *
 * Alias for \a strcmp 
 */
# define ccl_string_cmp strcmp

/*!
 * \brief Duplicates the string \a s.
 * \param s the string to duplicate
 * \pre s != NULL
 * \return a copy of \a s
 */
extern char *
ccl_string_dup (const char *s);

/*!
 * \brief Allocates a formatted string.
 * 
 * This function allocates a new string and fills it using formatting 
 * informations contained in \a fmt. The syntax of \a fmt is the one of
 * \e printf-like functions.
 *
 * \param fmt the formatting string
 * \pre fmt != NULL
 *
 * return a formatted string.
 */
extern char *
ccl_string_format_new (const char *fmt, ...);

/*!
 * \brief Allocates a formatted string.
 * 
 * This function makes the same work than \ref ccl_string_format_new but 
 * variable arguments are replaced by a \a va_list; thus it can be called by
 * funtions that use variable arguments (i.e. \c ...).
 *
 * \param fmt the formatting string
 * \param pa the list of arguments
 * \pre fmt != NULL
 * return a formatted string.
 */
extern char *
ccl_string_format_new_va (const char *fmt, va_list pa);

/*!
 * \brief Allocates a formatted string.
 *
 * This function creates a formatted string using \a fmt specifications in the
 * same way than \ref ccl_string_format_new. The resulting string is stored in 
 * the buffer pointed by \a dst. The function assumes that \a *size bytes can be
 * stored into \a *dst; if \a size is NULL the buffer size is assumed to be 0.
 * If *dst has a size inferior to the number of bytes required to store the 
 * formatted string then the buffer is resized and *size is changed according to
 * the new size. \a *dst can be NULL.
 * 
 * \param dst a pointer to the string that should receive the result. 
 * \param size a pointer to an integer that should receive the new size of 
 * the buffer pointed by *dst. This parameter can be NULL.
 * \param fmt the formatting string
 *
 * \pre dst != NULL
 * \pre fmt != NULL
 */
extern void
ccl_string_format (char **dst, size_t *size, const char *fmt, ...);

/*!
 * \brief Allocates a formatted string.
 *
 * This function behaves like \ref ccl_string_format but variable arguments
 * are replaced by a \e va_list.
 * \param dst a pointer to the string that should receive the result. 
 * \param size a pointer to an integer that should receive the new size of 
 * the buffer pointed by *dst. This parameter can be NULL.
 * \param fmt the formatting string
 * \param pa the list of arguments
 * \pre dst != NULL
 * \pre fmt != NULL 
 */
extern void
ccl_string_format_va (char **dst, size_t *size, const char *fmt, va_list pa);

/*!
 * \brief Appends a formatted string to \a *dst
 *
 * This function creates a formatted string using \a fmt specifications in the
 * same way than \ref ccl_string_format_new. The resulting string is appended 
 * to the string pointed by \a dst. \a *dst can be NULL in which case it is
 * considered as an empty string.
 *
 * \param dst a pointer to the string that should receive the result. 
 * \param fmt the formatting string
 *
 * \pre fmt != NULL
 */
extern void
ccl_string_format_append (char **dst, const char *fmt, ...);

/*!
 * \brief Appends a formatted string to \a *dst
 *
 * This function behaves like \ref ccl_string_format_append but variable 
 * arguments are replaced by a \e va_list.
 *
 * \param dst a pointer to the string that should receive the result. 
 * \param fmt the formatting string
 * \param pa the list of arguments
 * \pre fmt != NULL
 */
extern void
ccl_string_format_append_va (char **dst, const char *fmt, va_list pa);

/*!
 * \brief Interprets \a s as an integer value.
 * 
 * The function parses the longest prefix of \a s that matches the regexp 
 * of a signed integer: 0|-([0-9]+). 
 * 
 * If \a s is NULL then the function returns 0.
 *
 * \param s the string to parse
 * \return the integer encoded by \a s 
 */
extern int
ccl_string_parse_int (const char *s);

/*!
 * \brief Interprets \a s as a Boolean value.
 *
 * If \a s is equal to one of the following strings \a true, \a enabled, \a 
 * TRUE, \a ENABLED or \a 1, the function returns a non-null value.
 * If \a s is NULL then the function returns 0.
 *
 * \param s the string to parse
 * \return a non-null value if \a s describe a Boolean value equivalent to \a 
 * true.
 */
extern int
ccl_string_parse_boolean (const char *s);

/*!
 * \brief Creates a unique string from the string \a s
 * 
 * A \e unique string is just a string stored in an internal dictionary. All
 * unique string can then be compared by addresses instead of the 
 * lexicographical order (e.g. using strcmp).
 *
 * The function manages an internal dictionary. If the string \a s exists in 
 * that dictionary the function returns the pointer to the stored string; else,
 * a copy of \a s is stored in the dictionary and returned.
 *
 * \param s the string made unique
 * \pre s!= NULL
 * \return a unique string from the string \a s
 */
extern ccl_ustring
ccl_string_make_unique (const char *s);

/*!
 * \brief Creates a unique string from an integer \a i
 * \param i the integer transformed into a unique string
 * \return a unique string from the integer \a i
 */
extern ccl_ustring
ccl_string_make_unique_from_int (int i);

extern const char *
ccl_string_has_prefix (const char *pref, const char *arg);

END_C_DECLS

#endif /* ! __CCL_STRING_H__ */
