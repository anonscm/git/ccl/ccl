/*
 * ccl-memory.h -- Memory allocators
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-memory.h
 * \brief Memory Allocators
 * 
 */
#ifndef __CCL_MEMORY_H__
# define __CCL_MEMORY_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-exception.h>

BEGIN_C_DECLS

typedef void ccl_memory_exhaustion_handler (void *data);

/*!
 * \brief Exception raised on free memory exhaustion.
 */
CCL_DECLARE_EXCEPTION (memory_exhausted_exception, runtime_exception);

extern void
ccl_memory_add_exhaustion_handler (ccl_memory_exhaustion_handler *hdl,
				   void *data);

extern void
ccl_memory_del_exhaustion_handler (ccl_memory_exhaustion_handler *hdl,
				   void *data);

/*!
 * \brief Allocates a new uninitialized bloc of \a size bytes.
 * \param size the number of bytes to allocate
 */
extern void *
ccl_malloc (size_t size);

/*!
 * \brief Releases the memory block at the address \a ptr
 * \param ptr the address of the block to free
 * \pre ptr != NULL
 */
extern void
ccl_free (void *ptr);

/*!
 * \brief Changes the size of the block at the address \a ptr.
 * The data pointed by \a ptr is copied in the new block. Additional 
 * bytes are not initiliazed.
 * \param ptr the address of the block to resize
 * \param size the new size 
 * \pre ptr != NULL
 * \return the address of the resized block.
 */
extern void *
ccl_realloc (void *ptr, size_t size);

/*!
 * \brief Allocates an array with \a nb_el cells, each cell requiring \a el_size
 * bytes.
 * The memory allocated is filled with zeros.
 * \param nb_el the number of elements allocated
 * \param el_size the size of each element
 * \return the address of the array
 */
extern void *
ccl_calloc (size_t nb_el, size_t el_size); 

/*!
 * \brief Allocates an array of \a _sz_ elements of type \a _type_.
 */
# define ccl_new_array(_type_,_sz_) \
  ((_type_ *)ccl_calloc (_sz_, sizeof(_type_)))

/*!
 * \brief Allocates a new block large enough to receive an object of type 
 * \a _type_
 */
# define ccl_new(_type_) ccl_new_array (_type_, 1)

/*!
 * \brief Releases the memory pointed by \a ptr.
 */
# define ccl_delete(_ptr_) ccl_free (_ptr_)

/*!
 * \brief Releases the memory pointed by \a ptr if not NULL.
 */
# define ccl_zdelete(_del,_ptr_)		\
  do						\
    {						\
      if ((_ptr_) != NULL)			\
	_del (_ptr_);				\
    }						\
  while(0)

/*!
 * \brief Fill the memory block at address \a _p with \a _size 0-bytes.
 */
# define ccl_memzero(_p,_size) ccl_memset (_p, 0, _size)

/*!
 * \brief Fill the memory block at address \a _p with \a _size bytes equal to
 * \a _c.
 */
# define ccl_memset(_p,_c,_size) memset (_p, _c, _size)

/*!
 * \brief Copy \a _size bytes from the address \a _src to the address \a _dst.
 */
# define ccl_memcpy(_dst,_src,_size) memcpy (_dst, _src, _size)

/*!
 * \brief Lexicographically compates memory blocks at addresses \a _p1 and 
 * \a _p2 for at most \a _size bytes.
 */
# define ccl_memcmp(_p1,_p2,_size) memcmp (_p1, _p2, _size)

END_C_DECLS

#endif /* ! __CCL_MEMORY_H__ */
