/*
 * ccl-log.c -- Message displayer 
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file
 * \brief
 * 
 */
#include "ccl-time.h"
#include "ccl-memory.h"
#include "ccl-assert.h"
#include "ccl-string.h"
#include "ccl-log-p.h"

# define LOG_BUFFER_INIT_SIZE 1000

typedef struct timed_block_st timed_block;
struct timed_block_st 
{
  timed_block *next;
  uint32_t timer;
  char *msg;
};

typedef struct log_listener_st log_listener;
struct log_listener_st 
{
  ccl_log_proc *proc;
  void *data;
  log_listener *next;
};

			/* --------------- */

static void
s_log_string (ccl_log_type type, const char *str);

			/* --------------- */

static int debug_tab_level;
static int debug_max_level;
static int debug_new_line;
static char *log_buffer = NULL;
static size_t log_buffer_size = 0;
static log_listener *listeners;
static log_listener *redirection[CCL_LOG_NB_TYPES];
int ccl_debug_is_on;
static timed_block *blocks = NULL;
static timed_block *free_blocks = NULL;

			/* --------------- */

void
ccl_log_init(void)
{
  int i;

  debug_tab_level = 0;
  debug_max_level = 0;
  debug_new_line = 1;
  ccl_debug_is_on = 0;
  log_buffer = NULL;
  log_buffer_size = 0;
  listeners = NULL;

  for (i = 0; i < CCL_LOG_NB_TYPES; i++)
    redirection[i] = NULL;
}

			/* --------------- */

void
ccl_log_terminate (void)
{
  timed_block *b;

  while (listeners != NULL)
    {
      log_listener *next = listeners->next;
      ccl_delete (listeners);
      listeners = next;
    }

  if (log_buffer != NULL)
    {
      ccl_delete (log_buffer);
      log_buffer = NULL;
    }

  for (; blocks != NULL; blocks = b)
    {
      b = blocks->next;
      ccl_delete (blocks->msg);
      ccl_delete (blocks);
    }

  for (blocks = free_blocks; blocks != NULL; blocks = b)
    {
      b = blocks->next;
      ccl_delete (blocks);
    }
}

			/* --------------- */

int
ccl_log_has_listener (void)
{
  return listeners != NULL;
}

			/* --------------- */

void
ccl_log_add_listener (ccl_log_proc *proc, void *data)
{
  log_listener *l;
  
  l = ccl_new (log_listener);
  l->proc = proc;
  l->data = data;
  l->next = listeners;
  listeners = l;
}

			/* --------------- */

void
ccl_log_remove_listener (ccl_log_proc *proc)
{
  log_listener **plist;
  log_listener *to_remove;

  for (plist = &listeners; *plist; plist = &((*plist)->next))
    {
      if ((*plist)->proc == proc)
	break;
    }

  ccl_pre ("unknown listener removal" && 
	   (*plist != NULL));

  to_remove =  *plist;
  *plist = to_remove->next;
  ccl_delete (to_remove);
}

			/* --------------- */

void
ccl_log_push_redirection (ccl_log_type type, ccl_log_proc *proc, void *data)
{
  log_listener *ll = ccl_new (log_listener);

  ll->proc = proc;
  ll->data = data;
  ll->next = redirection[type];
  redirection[type] = ll;
}

			/* --------------- */

void
ccl_log_pop_redirection (ccl_log_type type)
{
  log_listener *ll = redirection[type];

  ccl_pre (redirection[type] != NULL);

  redirection[type] = ll->next;
  ccl_delete (ll);
}

			/* --------------- */

void
ccl_log (ccl_log_type type, const char *fmt, ...)
{
  va_list  pa;
    
  va_start (pa, fmt);
  ccl_log_va (type, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ccl_log_va (ccl_log_type type, const char *fmt, va_list pa)
{
  if (type == CCL_LOG_DEBUG && !ccl_debug_is_on)
    return;
    
  ccl_string_format_va (&log_buffer, &log_buffer_size, fmt, pa);

  if (type == CCL_LOG_DEBUG)
    {
      char *s;

      if (debug_max_level >= 0 && debug_tab_level >= debug_max_level)
	{
	  for (s = log_buffer; *s && *s != '\n'; s++)
	    CCL_NOP ();
	  debug_new_line = (*s == '\n');
	  return;
	}

      if (debug_new_line)
	{
	  int i;
	  
	  for (i = 0; i < debug_tab_level; i++)
	    s_log_string (type, " ");
	}

      for (s = log_buffer; *s && *s != '\n'; s++)
	/* empty */;
      debug_new_line = (*s == '\n');
    }

  s_log_string (type, log_buffer);
}


			/* --------------- */

void
ccl_display (const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa, fmt);
  ccl_log_va (CCL_LOG_DISPLAY, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ccl_warning (const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa, fmt);
  ccl_log_va (CCL_LOG_WARNING, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ccl_panic (const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa,fmt);
  ccl_log_va (CCL_LOG_PANIC, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ccl_error (const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa, fmt);
  ccl_log_va (CCL_LOG_ERROR, fmt, pa);
  va_end (pa);
}


			/* --------------- */

void
ccl_debug (const char *fmt, ...)
{
  va_list pa;
  
  va_start (pa,fmt);  
  ccl_log_va (CCL_LOG_DEBUG, fmt, pa);
  va_end (pa);
}

			/* --------------- */

void
ccl_debug_max_level (int maxlevel)
{
  debug_max_level = maxlevel;
  ccl_debug_is_on = (debug_max_level != 0);
}

			/* --------------- */

void
ccl_debug_push_tab_level (void)
{
  debug_tab_level++;
}

			/* --------------- */

void
ccl_debug_pop_tab_level (void)
{
  debug_tab_level--;
  ccl_assert (debug_tab_level >= 0);
}

			/* --------------- */

void
ccl_debug_start_timed_block (const char *fmt, ...)
{
  va_list pa;
  timed_block *nbl = free_blocks;
  if (nbl == NULL)
    nbl = ccl_new (timed_block);
  else
    free_blocks = nbl->next;

  nbl->next = blocks;
  blocks = nbl;
  va_start (pa, fmt);  
  nbl->msg = ccl_string_format_new_va (fmt, pa);
  va_end (pa);
  ccl_log (CCL_LOG_DEBUG, "%s\n", nbl->msg);
  ccl_debug_push_tab_level ();
  nbl->timer = ccl_time_get_in_milliseconds ();
}

			/* --------------- */

void
ccl_debug_end_timed_block (void)
{
  timed_block *b = blocks;  
  uint32_t elapsed_time;

  ccl_debug_pop_tab_level ();
  elapsed_time = ccl_time_get_in_milliseconds () - b->timer;

  ccl_debug ("%s done within %.0f ms\n", b->msg, (float) elapsed_time);
  ccl_delete (b->msg);

  blocks = b->next;
  b->next = free_blocks;
  free_blocks = b;
}

			/* --------------- */

static void
s_output_to_string (ccl_log_type type, const char  *msg, void *data)
{
  ccl_string_format_append (data, msg);
}

			/* --------------- */

void
ccl_log_redirect_to_string (ccl_log_type type, char **dst)
{
  ccl_log_push_redirection (type, s_output_to_string, dst);
}

			/* --------------- */
static void
s_output_to_FILE (ccl_log_type type, const char  *msg, void *data)
{
  fprintf (data, "%s", msg);
  fflush (data);
}


			/* --------------- */

void
ccl_log_redirect_to_FILE (ccl_log_type type, FILE *output)
{
  ccl_log_push_redirection (type, s_output_to_FILE, output);
}

			/* --------------- */

static void
s_log_string (ccl_log_type type, const char *str)
{
  if (redirection[type] != NULL)
    {
      redirection[type]->proc (type, str, redirection[type]->data);
    }
  else
    {
      log_listener *l;
      
      for (l = listeners; l; l = l->next)
	l->proc (type, str, l->data);
    }
}


