/*
 * ccl-config-table.h -- Configuration table
 * 
 * This file is a part of the C Common Library (CCL) project. 
 * 
 * Copyright (C) 2010 CNRS UMR 5800 & Université Bordeaux I (see AUTHORS file).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the AltaRica Public License that comes with this
 * package.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 */

/*!
 * \file ccl/ccl-config-table.h
 * \brief Implementation of some kind of "preferences" table.
 * 
 * This module implements hierarchical associative tables. Each \ref 
 * ccl_config_table stores couple of strings (identifier, value). A table
 * may inherits content from another which means that is an identifier is not
 * found locally in a table, the search is transfered to the parent table 
 * (until the identifier is found or no more ancestor exists).
 *
 * 
 */
#ifndef __CCL_CONFIG_TABLE_H__
# define __CCL_CONFIG_TABLE_H__

# include <ccl/ccl-common.h>
# include <ccl/ccl-list.h>

BEGIN_C_DECLS

/*!
 * \brief Abstract type for "preferences" table.
 */
typedef struct ccl_config_table_st ccl_config_table;

/*! 
 * \brief Builds an empty table.
 */
# define ccl_config_table_create() ccl_config_table_inherits (NULL)

/*!
 * \brief Adds one reference to \a conf.
 *
 * The function increments the counter of references of \a conf.
 * \param conf the configuration table
 * \pre conf != NULL
 * \return conf
 */
extern ccl_config_table *
ccl_config_table_add_reference (ccl_config_table *conf);

/*!
 * \brief Removes one reference to \a conf.
 *
 * If the counter of references of \a conf falls to zero then the resources
 * allocated to \a conf are released.
 * \param conf the configuration table
 * \pre conf != NULL
 */
extern void
ccl_config_table_del_reference (ccl_config_table *conf);

/*! 
 * \brief Builds a locally empty table but inherits content from \a parent
 * \param parent the configuration table used as parent
 * \pre parent != NULL
 * \return a new empty table
 */
extern ccl_config_table *
ccl_config_table_inherits (ccl_config_table *parent);

/*! 
 * \brief Checks if the content of the table has changed.
 *
 * The table maintains a flag indicating if the value of one entry has been 
 * changed since the creation of the table or its \ref ccl_config_table_load 
 * "loading". New entries are no considered here.
 *
 * \param conf the configuration table
 * \pre conf != NULL
 * \return a non-null value if the content of \a conf or those of one of its 
 * parents has changed.
 */
extern int
ccl_config_table_is_changed (ccl_config_table *conf);

/*!
 * \brief Adds the couple (\a name, \a value) into \a conf.
 * 
 * If the entry \a name exists in the table then the old value is replaced.
 *
 * \param conf the configuration table
 * \param name the option name
 * \param value the value of the \a option
 * \pre conf != NULL
 * \pre name != NULL
 * \pre value != NULL
 */
extern void
ccl_config_table_set (ccl_config_table *conf, const char *name, 
		      const char *value);

/*! 
 * \brief Checks if the table \a conf contains an entry for the option \a name.
 * \param conf the configuration table
 * \param name the name of the configuration option
 * \pre conf != NULL
 * \pre name != NULL
 * \return a non-null value if \a conf contains an entry for the option \a name.
 */
extern int
ccl_config_table_has (ccl_config_table *conf, const char * name);

/*!
 * \brief Add the couple (\a name, \a value) if the entry does not exist.
 *
 * If the entry \a name does not exist in \a conf, then the entry is created
 * with the couple (name, value); else, the existing value is not modified.
 * \param conf the configuration table
 * \param name the name of the option to assign
 * \param value the value assigned to the option 
 * \pre conf != NULL
 * \pre name != NULL
 * \pre value != NULL
 * \return the value of the option \a name
 */
extern const char *
ccl_config_table_set_if_null (ccl_config_table *conf, const char *name, 
			      const char *value);

/*!
 * \brief Adds all (local) preferences of \a others to \a conf.
 *
 * \param conf the configuration table
 * \param others another configuration table
 * \pre conf != NULL
 * \pre others != NULL
 */
extern void
ccl_config_table_add (ccl_config_table *conf, ccl_config_table *others);

/*! 
 * \brief Returns the value for the configuration option \a name.
 * 
 * If the option is not found then a message is sent on \ref CCL_LOG_WARNING.
 *
 * \param conf the configuration table
 * \param name the name of the option
 * \pre conf != NULL
 * \pre name != NULL
 * \return the value for the configuration option \a name or NULL if the
 *         option is not set.
 */
extern const char *
ccl_config_table_get (ccl_config_table *conf, const char * name);

/*!
 * \brief Returns the value for the configuration option \a name but the
 *        value is interpreted as an integer.
 * \param conf the configuration table
 * \param name the name of the option
 * \pre conf != NULL
 * \pre name != NULL
 * \return the value for the configuration option \a name but the
 *         value is interpreted as an integer.
 */
extern int
ccl_config_table_get_integer (ccl_config_table *conf, const char * name);

/*!
 * \brief Returns the value for the configuration option \a name but the
 *        value is interpreted as a Boolean value.
 * \param conf the configuration table
 * \param name the name of the option
 * \pre conf != NULL
 * \pre name != NULL
 * \return the value for the configuration option \a name but the value is 
 *         interpreted as a Boolean value.
 */
extern int
ccl_config_table_get_boolean (ccl_config_table *conf, const char * name);

/*! 
 * \brief Returns the list of valued options.
 * 
 * The function collects in \a conf and its parents the name of options
 * that have been set.
 *
 * \param conf the configuration table
 * \pre conf != NULL
 * \return a newly allocated list of strings. Strings contained into the list
 *         must not be deleted.
 */
extern ccl_list *
ccl_config_table_get_names (ccl_config_table *conf);

/*!
 * \brief Writes the content of \a conf onto the \a output stream.
 *
 * \param conf the configuration table
 * \param output the output stream
 * \pre conf != NULL
 * \pre output != NULL
 */
extern void
ccl_config_table_save (ccl_config_table *conf, FILE *output);

/*!
 * \brief Reads configuration options from the \a input stream and adds them
 * into the \a conf table.
 *
 * \a input must be a stream containing data previously saved with the 
 * \ref ccl_config_table_save function.
 *
 * \param conf the configuration table
 * \param input the input stream
 * \pre conf != NULL
 * \pre input != NULL
 */
extern void
ccl_config_table_load (ccl_config_table *conf, FILE *input);

/*!
 * \brief Display on the CCL_LOG_DISPLAY stream the entries of \a conf
 *
 * \param conf the configuration table
 * \pre conf != NULL
 */
extern void
ccl_config_table_display (ccl_config_table *conf);

END_C_DECLS

#endif /* ! __CCL_CONFIG_TABLE_H__ */
